class InterUser {
  final String id;
  final String name;
  final String uid;
  final username;
  final birthdate;
  final picture;
  final phoneNumber;
  final String? email;
  final String? address;
  final String? roleId;
  final bool? active;
  final String? packageId;
  final String? country;
  final String? state;
  final String? city;
  final String? neighborhoodId;

  InterUser(
      {required this.id,
      required this.name,
      required this.uid,
      this.username,
      this.birthdate,
      this.picture,
      this.phoneNumber,
      this.email,
      this.address,
      this.roleId,
      this.active,
      this.packageId,
      this.state,
      this.city,
      this.country,
      this.neighborhoodId});

  factory InterUser.fromJson(Map<String, dynamic> json) {
    return InterUser(
      id: json['id'].toString(),
      name: json['name'].toString(),
      uid: json['uid'].toString(),
      username: json['username'],
      birthdate: json['birthdate'],
      picture: json['picture'],
      phoneNumber: json['phone_number'],
      email: json['email'].toString(),
      address: json['address'].toString(),
      roleId: json['role_id'].toString(),
      active: json['active'] as bool,
      packageId: json['package_id'].toString(),
      country: json['country'] as String,
      state: json['state'] as String,
      city: json['city'] as String,
      neighborhoodId: json['neighborhood_id'] ?? null,
    );
  }
}
