import 'interuser.dart';

class Post {
  final String id;
  final String title;
  final String? userId;
  final InterUser? user;
  final int? type;
  int? status;
  final String? categoryId;
  final String image;
  final String? shortDescription;
  final String content;
  final String? date;
  final String? location;
  final String? creadtedAt;
  final String? updatedAt;
  final String? groupId;

  Post(
      {required this.id,
      required this.title,
      this.userId,
      this.user,
      this.type,
      this.status,
      this.categoryId,
      required this.image,
      this.shortDescription,
      required this.content,
      this.date,
      this.location,
      this.creadtedAt,
      this.updatedAt,
      this.groupId});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json['id'],
      title: json['title'],
      userId: json['user_id'],
      groupId: json['group_id'],
      user: InterUser.fromJson(json['user']),
      type: json['type'],
      status: json['status'],
      categoryId: json['category_id'],
      image: json['image'],
      shortDescription: json['short_description'],
      content: json['content'],
      date: json['date'],
      location: json['location'],
      creadtedAt: json['creadted_at'],
      updatedAt: json['updated_at'],
    );
  }

  factory Post.fromJsonNoUser(Map<String, dynamic> json) {
    return Post(
      id: json['id'],
      title: json['title'],
      userId: json['user_id'],
      groupId: json['group_id'],
      type: json['type'],
      status: json['status'],
      categoryId: json['category_id'],
      image: json['image'],
      shortDescription: json['short_description'],
      content: json['content'],
      date: json['date'],
      location: json['location'],
      creadtedAt: json['creadted_at'],
      updatedAt: json['updated_at'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'userId': userId,
        'type': type,
        'status': status,
        'categoryId': categoryId,
        'image': image,
        'shortDescription': shortDescription,
        'content': content,
        'date': date,
        'location': location,
      };
}
