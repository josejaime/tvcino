class Alert {
  final String? id;
  final String? name;
  final String? userId;
  final String? content;
  final int? type;
  final int? riskRate;
  final String? expirationTime;
  final bool? active;

  Alert({
    this.id,
    this.name,
    this.userId,
    this.content,
    this.type,
    this.riskRate,
    this.expirationTime,
    this.active,
  });

  factory Alert.fromJson(Map<String, dynamic> json) {
    return Alert(
      id: json['id'] as String,
      name: json['name'] as String,
      userId: json['userId'] as String,
      content: json['content'] as String,
      type: json['type'] as int,
      riskRate: json['riskRate'] as int,
      expirationTime: json['expirationTime'] as String,
      active: json['active'] as bool,
    );
  }
}
