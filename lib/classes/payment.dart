import 'package:tvcino/classes/interuser.dart';

class Payment {
  final String id;
  final String feeId;
  final InterUser user;
  final double payment;
  final double change;
  final String createdAt;

  Payment(
      {required this.id,
      required this.feeId,
      required this.user,
      required this.payment,
      required this.change,
      required this.createdAt});

  factory Payment.fromJson(Map<String, dynamic> json) {
    return Payment(
      id: json['id'] as String,
      feeId: json['fee_id'] as String,
      user: InterUser.fromJson(json['user']),
      payment: json['payment'] as double,
      change: json['change'] as double,
      createdAt: json['created_at'] as String,
    );
  }
}
