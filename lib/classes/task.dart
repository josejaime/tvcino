import 'package:tvcino/classes/interuser.dart';

class Task {
  final String id;
  final String? name;
  final InterUser? user;
  final String? content;
  final int priority;
  final InterUser? createdBy;
  final String? limitDate;
  bool active;

  Task({
    required this.id,
    this.name,
    this.user,
    this.content,
    required this.priority,
    this.createdBy,
    this.limitDate,
    required this.active,
  });

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      id: json['id'] as String,
      name: json['name'] as String,
      user: InterUser.fromJson(json['user']),
      content: json['content'] as String,
      priority: json['priority'] as int,
      createdBy: InterUser.fromJson(json['created_by']),
      limitDate: json['limit_date'] as String,
      active: json['active'] as bool,
    );
  }
}
