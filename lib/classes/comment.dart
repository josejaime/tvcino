import 'package:tvcino/classes/interuser.dart';

class Comment {
  final String id;
  final String postId;
  final String? userId;
  final String content;
  final String createdAt;
  final String updatedAt;
  final InterUser user;

  Comment(
      {required this.id,
      required this.postId,
      this.userId,
      required this.content,
      required this.createdAt,
      required this.updatedAt,
      required this.user});

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
      id: json['id'],
      postId: json['post_id'],
      userId: json['user_id'],
      user: InterUser.fromJson(json['user']),
      content: json['content'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}
