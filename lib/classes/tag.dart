class Tag {
  final String? id;
  final String? name;
  final String? description;
  final String? icon;

  Tag({this.id, this.name, this.description, this.icon});

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      icon: json['icon'],
    );
  }
}
