class Group {
  final String id;
  final String name;
  final String image;
  final String shortDescription;
  final String description;
  final String authorId;
  final String mainNeighborhoodId;
  final bool active;
  final int totalMembers;

  Group(
      {required this.id,
      required this.name,
      required this.image,
      required this.shortDescription,
      required this.description,
      required this.authorId,
      required this.mainNeighborhoodId,
      required this.active,
      required this.totalMembers});

  factory Group.fromJson(Map<String, dynamic> json) {
    return Group(
      id: json['id'] as String,
      name: json['name'] as String,
      image: json['image'] as String,
      shortDescription: json['short_description'] as String,
      description: json['description'] as String,
      authorId: json['author_id'] as String,
      mainNeighborhoodId: json['main_neighborhood_id'] as String,
      active: json['active'] as bool,
      totalMembers: json['total_members'] as int,
    );
  }
}
