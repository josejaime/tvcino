import 'package:tvcino/classes/interuser.dart';

class Fee {
  final String id;
  final String name;
  final double total;
  final String neighborhoodId;
  final String createdAt;
  final bool active;
  final InterUser createdBy;

  Fee({
    required this.id,
    required this.name,
    required this.total,
    required this.neighborhoodId,
    required this.createdAt,
    required this.active,
    required this.createdBy,
  });

  factory Fee.fromJson(Map<String, dynamic> json) {
    return Fee(
      id: json['id'] as String,
      name: json['name'] as String,
      total: json['total'] as double,
      neighborhoodId: json['neighborhood_id'] as String,
      createdAt: json['created_at'] as String,
      active: json['active'] as bool,
      createdBy: InterUser.fromJson(json['created_by']),
    );
  }
}
