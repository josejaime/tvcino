class Rol {
  final String? id;
  final String? name;

  Rol({
    this.id,
    this.name,
  });

  factory Rol.fromJson(Map<String, dynamic> json) {
    return Rol(
      id: json['id'] as String,
      name: json['name'] as String,
    );
  }
}
