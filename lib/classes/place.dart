import 'dart:convert';

///
/// Class representation of a place for tuvcino
/// for easier way to manage jsons
///

class Place {
  final String id;
  final String neighborhoodId;
  final String userId;
  final String name;
  final String city;
  final String state;
  final String location;
  final double? raiting;
  final int votes;
  //final String tags;
  final List<Tag> tags;
  final String description;
  final String phone;
  final String image;
  final String facebook;
  final String instagram;
  final String whatsapp;
  final bool active;

  Place(
      {required this.id,
      required this.neighborhoodId,
      required this.userId,
      required this.name,
      required this.city,
      required this.state,
      required this.location,
      this.raiting,
      required this.votes,
      required this.tags,
      required this.description,
      required this.phone,
      required this.image,
      required this.active,
      required this.facebook,
      required this.instagram,
      required this.whatsapp});

  factory Place.fromJson(Map<String, dynamic> json) {
    //List<Tag> listTags = List<Tag>.from(json['tags']);
    List innerTags = json['tags'] as List;
    List<Tag> listTags = innerTags.map((e) => Tag.fromJson(e)).toList();

    return Place(
      id: json['id'] as String,
      neighborhoodId: json['neighborhood_id'] as String,
      userId: json['user_id'] as String,
      name: json['name'] as String,
      city: json['city'] as String,
      state: json['state'] as String,
      location: json['location'] as String,
      raiting: json['raiting'],
      votes: json['votes'] as int,
      tags: listTags,
      description: json['description'] as String,
      phone: json['phone'] as String,
      image: json['image'] as String,
      active: json['active'] as bool,
      facebook: json['facebook'] as String,
      instagram: json['instagram'] as String,
      whatsapp: json['whatsapp'] as String,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'neighborhood_id': neighborhoodId,
        'userId': userId,
        'name': name,
        'location': location,
        'raiting': raiting,
        'votes': votes,
        //'tags': tags,
        'description': description,
        'phone': phone,
        'image': image,
        'active': active,
        'facebook': facebook,
        'instagram': instagram,
        'whatsapp': whatsapp,
      };
}

// Functions to adresss with out making an object
List<Place> parsePlaces(String responseBody) {
  // make the big json
  final parsed = jsonDecode(responseBody).cast < Map<String, dynamic>();

  // return a list of places
  return parsed.map<Place>((json) => Place.fromJson(json)).toList();
}

class Tag {
  final String? id;
  final String? name;

  Tag({this.id, this.name});

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(
      id: json['id'],
      name: json['name'],
    );
  }

  //factory Tag.fromJson();
}
