class Neighbordhood {
  final String id;
  final String name;
  final String country;
  final String state;
  final String city;
  final bool active;
  final String userId;
  final String zipcode;

  Neighbordhood({
    required this.id,
    required this.name,
    required this.country,
    required this.state,
    required this.city,
    required this.active,
    required this.userId,
    required this.zipcode,
  });

  factory Neighbordhood.fromJson(Map<String, dynamic> json) {
    return Neighbordhood(
      id: json['id'].toString(),
      name: json['name'].toString(),
      country: json['country'].toString(),
      state: json['state'].toString(),
      city: json['city'].toString(),
      active: json['active'] as bool,
      userId: json['user_id'].toString(),
      zipcode: json['zipcode'].toString(),
    );
  }
}
