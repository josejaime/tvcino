import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/comment.dart';
import 'package:tvcino/classes/posts.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:tvcino/pages/posts/update_post.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class EventPage extends StatefulWidget {
  final String tile;
  final String id;
  final String image;
  final int? type;

  EventPage(
      {required this.id, required this.tile, required this.image, this.type});

  @override
  _EventPageState createState() => _EventPageState();
}

class _EventPageState extends State<EventPage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  late String _userId;

  /// ==============================
  /// DIO GET THE POSTS
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = false;
  bool _error = false;
  String _errorMessage = "";
  bool _loadingComments = false;
  bool _errorPostComment = false;

  /// ==============================
  /// The Posts and Comments
  /// ==============================
  late Post _post;
  String finalDate = "";
  List<Comment> comments = List<Comment>.empty(growable: true);
  TextEditingController _commentController = TextEditingController();
  bool _errorText = false;
  bool _favorite = false;

  /// ==============================
  /// The Posts and Comments
  /// ==============================
  late SharedPreferences preferences;

  List<String> choices = <String>[
    "Atraco",
    "Aviso",
  ];

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    initializeDateFormatting();
    getPost();
    _getPreferences();
    super.initState();
  }

  _getPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print("TV Retrived data");
    print("TV User Name = ${preferences.getString('name')}");
    print("TV User userame = ${preferences.getString('username')}");
    print("TV User email  = ${preferences.getString('email')}");
    print("TV User UUID  = ${preferences.getString('uuid')}");
    print("TV User ID  = ${preferences.getString('userId')}");
    print("TV User birthdate  = ${preferences.getString('birthdate')}");
    print("TV User country  = ${preferences.getString('country')}");
    print("TV User state  = ${preferences.getString('state')}");
    print("TV User city  = ${preferences.getString('city')}");
    print("TV User address  = ${preferences.getString('address')}");

    setState(() {
      _userId = preferences.getString('userId') as String;
    });

    ///CONTROL
    print("TV - CONTROL");
    print("TV User Created  = ${preferences.getBool('userCreated')}");
    print("TV Neighborhood ID = ${preferences.getString('neighborhoodId')}");
    print("TV Neighborhood set? = ${preferences.getBool('neighborhoodSet')}");
  }

  /// ==============================
  /// Get posts from server
  /// ==============================
  getPost() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    print("ID- ${widget.id}");
    Response response =
        await dio.get('/posts/${widget.id}').catchError((onError) {
      print("TV - ERROR - ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response);
        print("DIO Trying to convert data");
        //List data = response.data['items'];
        setState(() {
          _post = Post.fromJson(response.data);
          _isLoading = false;
          _error = false;
          _message = "";
        });

        //
        DateFormat format = new DateFormat('dd-MM-yyy', 'es');
        DateTime something = format.parse(_post.date as String);
        finalDate = DateFormat.yMMMMd('es').format(something);

        print("Converted POST:");
        print(_post);
        isFavorite(_post);

        ///==============================
        /// Now Lets get the comments
        ///==============================
        bool safeCallComments = true;

        Response responseComments = await dio
            .get('/comments?post_id=${widget.id}')
            .catchError((onError) {
          print("TV - ERROR MAKING CALL TO COMMENTS ${onError.toString()}");
          safeCallComments = false;
        });

        if (safeCallComments) {
          List data = responseComments.data['items'];
          print(data);
          if (data.length > 0) {
            setState(() {
              for (int i = 0; i < data.length; i++) {
                print(data[i]['content']);
                comments.add(Comment.fromJson(data[i]));
              }
            });
          } else {
            /// There is not comments
            print("TV - MSG POST WITH NO COMMENTS");
          }
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  isFavorite(Post post) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List<String> favorites =
        preferences.getStringList('favoritePosts') as List<String>;

    setState(() {
      _favorite = favorites.contains(post.id);
    });
  }

  ///==============================
  /// Now Lets get the comments
  ///==============================
  Future<bool> getComments() async {
    bool safeCallComments = true;
    Dio dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    setState(() {
      _loadingComments = true;
    });

    Response responseComments =
        await dio.get('/comments?post_id=${widget.id}').catchError((onError) {
      print("TV - ERROR MAKING CALL TO COMMENTS ${onError.toString()}");
      safeCallComments = false;
    });

    if (safeCallComments) {
      List data = responseComments.data['items'];
      if (data.length > 0) {
        setState(() {
          comments.clear();
        });
        for (int i = 0; i < data.length; i++) {
          print(data[i]['content']);
          setState(() {
            comments.add(Comment.fromJson(data[i]));
          });
        }
        setState(() {
          _loadingComments = false;
        });
      } else {
        /// There is not comments
        print("TV - MSG POST WITH NO COMMENTS");
        setState(() {
          _loadingComments = false;
        });
      }
    }
    return false;
  }

  ///==============================
  /// Now Lets get the comments
  ///==============================
  Future<bool> postComment() async {
    preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    setState(() {
      _errorPostComment = false;
    });

    var url = CustomFunctions.url + '/comments/';

    try {
      String commentContent =
          _commentController.text.replaceAll(RegExp('"'), '\\"');
      String datos =
          '{"post_id": "${widget.id}", "user_id": "$userId", "content": "$commentContent"}';
      print("TV - DATA");
      print(datos);
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      http
          .post(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
        body: datos,
      )
          .then((response) {
        if (response.statusCode == 200) {
          print("Comment Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _commentController.clear();
            _errorPostComment = false;
          });

          print("TV - COMMENT CREATED ALL OK");
          getComments();
        } else {
          print(
              "TV - CREATE COMMENT something went wrong ${response.statusCode}");
          print("TV - something went wrong ${response.request}");
          print("TV - HTTP ERROR - Status Code ${response.body}");
          setState(() {
            _errorPostComment = true;
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _errorPostComment = true;
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _errorPostComment = true;
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _errorPostComment = true;
      });
      return false;
    }
    return false;
  }

  ///==============================
  /// Add the post as favorite
  ///==============================
  Future<bool> addAsFavorite() async {
    preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    setState(() {
      _errorPostComment = false;
    });

    String url = CustomFunctions.url;
    url = url + '/users/$userId/favorites/posts/${_post.id}';

    try {
      String commentContent =
          _commentController.text.replaceAll(RegExp('"'), '\\"');
      String datos =
          '{"post_id": "${widget.id}", "user_id": "$userId", "content": "$commentContent"}';
      print("TV - DATA");
      print(datos);
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      http
          .post(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
        body: datos,
      )
          .then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          //============================================/
          print("TV - POST ADDED AS FAVORITE!");
          SharedPreferences preferences = await SharedPreferences.getInstance();

          List<String> favorites =
              preferences.getStringList('favoritePosts') as List<String>;
          favorites.add(_post.id);
          await preferences.setStringList('favoritePosts', favorites);

          setState(() {
            _favorite = true;
          });
          showBottomMessage(Icons.done, '¡Marcado como favorito!');
        } else {
          print("TV - ADD as Favorte went wrong ${response.statusCode}");
          print("TV - something went wrong ${response.request}");
          print("TV - HTTP ERROR - Status Code ${response.body}");
          setState(() {
            _errorPostComment = true;
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _errorPostComment = true;
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _errorPostComment = true;
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _errorPostComment = true;
      });
      return false;
    }
    return false;
  }

  ///==============================
  /// remove the post from the
  /// favorites
  ///==============================
  Future<bool> removeFromFavorite() async {
    preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    setState(() {
      _errorPostComment = false;
    });

    String url = CustomFunctions.url;
    url = url + '/users/$userId/favorites/posts/${_post.id}';

    try {
      String datos = '{"post_id": "${widget.id}", "user_id": "$userId"}';
      print("TV - DATA");
      print(datos);
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      http.delete(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
      ).then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          //============================================/
          print("TV - POST REMOVED FROM FAVORITES!");
          SharedPreferences preferences = await SharedPreferences.getInstance();

          List<String> favorites =
              preferences.getStringList('favoritePosts') as List<String>;
          favorites.remove(_post.id);
          await preferences.setStringList('favoritePosts', favorites);

          setState(() {
            _favorite = false;
          });

          showBottomMessage(Icons.done, '¡Post removido de tus favoritos!');
        } else {
          print("TV - REMOVE as Favorte went wrong ${response.statusCode}");
          print("TV - something went wrong ${response.request}");
          print("TV - HTTP ERROR - Status Code ${response.body}");
          setState(() {
            _errorPostComment = true;
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _errorPostComment = true;
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _errorPostComment = true;
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _errorPostComment = true;
      });
      return false;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
        title: Image(
          image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
          height: 21.0,
        ),
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Theme.of(context).primaryColor,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height / 3,
                          decoration: BoxDecoration(
                            color:
                                Theme.of(context).accentColor.withOpacity(0.7),
                            // image: _post.image != null
                            //     ? DecorationImage(
                            //         image: _post.image != null
                            //             ? NetworkImage(_post.image)
                            //             : ExactAssetImage(
                            //                 'assets/images/tuvcino-directorio-3.jpg'),
                            //         fit: _post.image != null
                            //             ? BoxFit.cover
                            //             : BoxFit.contain,
                            //       )
                            //     : null,
                          ),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              Image(
                                fit: BoxFit.contain,
                                image: NetworkImage(_post.image, scale: 1),
                              ),
                              Positioned(
                                bottom: 0,
                                right: -15,
                                child: Column(
                                  children: [
                                    _post.user!.id == _userId
                                        ? RawMaterialButton(
                                            onPressed: () {
                                              //Navigator.push(context,MaterialPageRoute(builder: (context) => Page2())).then((value) { setState(() {});

                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        UpdatePost(
                                                      post: _post,
                                                    ),
                                                  )).then((value) {
                                                setState(() {
                                                  getPost();
                                                });
                                              });
                                            },
                                            shape: CircleBorder(),
                                            fillColor: Colors.white,
                                            //child: Icon( Icons.share , color: Theme.of(context).primaryColor,),
                                            child: Icon(Icons.settings,
                                                semanticLabel: 'Actualizar',
                                                color: Theme.of(context)
                                                    .primaryColor),
                                          )
                                        : Offstage(),
                                    /* RawMaterialButton(
                                      onPressed: () {
                                        print("Icon Share Button");
                                      },
                                      shape: CircleBorder(),
                                      fillColor: Colors.white,
                                      child: Icon(FontAwesome.comment_empty,
                                          color:
                                              Theme.of(context).primaryColor),
                                    ), */
                                    RawMaterialButton(
                                      onPressed: () {
                                        //print("Icon Share Button");
                                        if (!_favorite) {
                                          addAsFavorite();
                                        } else {
                                          removeFromFavorite();
                                        }
                                      },
                                      shape: CircleBorder(),
                                      fillColor: Colors.white,
                                      //child: Icon( Icons.thumb_up , color: Theme.of(context).primaryColor,),
                                      child: Icon(
                                          _favorite
                                              ? FontAwesome.heart
                                              : FontAwesome.heart_empty,
                                          color:
                                              Theme.of(context).primaryColor),
                                    ),
                                    RawMaterialButton(
                                      onPressed: () {
                                        _showDialogWithMessage(context,
                                            "¿Quieres reportar esta imagen?");
                                      },
                                      shape: CircleBorder(),
                                      fillColor: Colors.white,
                                      //child: Icon( Icons.thumb_up , color: Theme.of(context).primaryColor,),
                                      child: Icon(FontAwesome.block,
                                          color: Colors.red),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        /*====================================*/
                        /* Event title */
                        /*====================================*/
                        Container(
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              _post.title != null
                                  ? _post.title
                                  : 'Error al cargar...',
                              style: Theme.of(context).textTheme.headline1,
                              textScaleFactor: 0.7,
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        /*====================================*/
                        /* Event Chips */
                        /*====================================*/
                        /* Row(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 4.0),
                              child: Wrap(
                                spacing: 10.0,
                                children: [
                                  Chip(
                                    label: Text('Directorio'),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ), */
                        /*====================================*/
                        /* Event Place and Time */
                        /*====================================*/
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text("Por: ${_post.user!.name}"),
                          ),
                        ),
                        Divider(),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Row(
                            children: [
                              _post.date != null
                                  ? Expanded(
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8.0, 4.0, 8.0, 4.0),
                                            child: Icon(Icons.calendar_today,
                                                color: Theme.of(context)
                                                    .primaryColor),
                                          ),
                                          Text("Publicado: "),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                //_post.date
                                                finalDate,
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w600),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    )
                                  : Row(),
                              /* Expanded(
                                flex: 3,
                                child: _post.location != null &&
                                        _post.location != "null"
                                    ? Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8.0, 4.0, 8.0, 4.0),
                                            child: Icon(
                                              Icons.map,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                            ),
                                          ),
                                          Flexible(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  _post.location,
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    : Row(),
                              ), */
                            ],
                          ),
                        ),
                        /*====================================*/
                        /* Event Info */
                        /*====================================*/
                        Divider(),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Más Información',
                                style: Theme.of(context).textTheme.headline4,
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(_post.content),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: Form(
                            child: TextField(
                              maxLines: 4,
                              controller: _commentController,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                hintText: "Tienes algún comentario",
                                errorText: _errorText ? _errorMessage : null,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextButton(
                              /*color: Theme.of(context)
                                  .buttonTheme
                                  .colorScheme
                                  .secondary,*/
                              onPressed: () {
                                if (_commentController.text.trim().isNotEmpty) {
                                  if (_commentController.text.length < 281) {
                                    setState(() {
                                      print("TV - HERE POST COMMENT ");
                                      _errorText = false;
                                    });
                                    postComment();
                                  } else {
                                    setState(() {
                                      _errorText = true;
                                      _errorMessage =
                                          "* Mensaje demasiado largo intenta con menos de 280 caracteres";
                                    });
                                  }
                                } else {
                                  setState(() {
                                    _errorText = true;
                                    _errorMessage =
                                        "* No olvides llenar este campo";
                                  });
                                }
                              },
                              child: Text("Comentar")),
                        ),
                        _errorPostComment
                            ? Column(
                                children: [
                                  SizedBox(height: 10.0),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Lo sentimos, hubo un error al enviar tu comentario',
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6,
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              )
                            : Row(),
                        SizedBox(
                          height: 10.0,
                        ),
                        _loadingComments
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  TvcinoCircularProgressBar(
                                    color: Colors.white,
                                    textColor: Theme.of(context).primaryColor,
                                  )
                                ],
                              )
                            : Offstage(),
                        comments.isNotEmpty
                            ? buildComments(comments, context)
                            : Text("Este Post aún no tiene comentarios"),
                        SizedBox(
                          height: 30.0,
                        ),
                      ],
                    ),
                  ),
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  void _showDialogWithMessage(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("tuvcino"),
          content: Text(message),
          actions: [
            TextButton(
              child: Text("Cancelar"),
              onPressed: () async {
                Navigator.pop(context);
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                primary: Colors.white,
              ),
              child: Text('Reportar'),
              //color: Theme.of(context).buttonTheme.colorScheme.secondary,
              onPressed: () async {
                //Just dismiss the alert
                bool reported = await _reportPost();
                //bool reported = true;

                Navigator.of(context).pop();
                if (reported) {
                  showBottomMessage(Icons.done,
                      'Post reportado, ¡muchas gracias por cuidar a la comunidad!');
                } else {
                  showBottomMessage(Icons.error,
                      'No pudimos reportar el post, intenta nuevamente más tarde');
                }
              },
            ),
          ],
        );
      },
    );
  }

  showBottomMessage(IconData icon, String message) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(25.0),
          color: Theme.of(context).accentColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildComments(List<Comment> comments, BuildContext context) {
    return Column(
      children: comments.map((e) {
        //return Text("${e.name}");
        return buildComment(context, e);
      }).toList(),
    );
  }

  Column buildComment(BuildContext context, Comment e) {
    return Column(
      children: [
        Divider(),
        Material(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: CircleAvatar(
                        radius: 15.0,
                        backgroundColor: const Color(0xFF778899),
                        child: Text(e.user.name != null
                            ? e.user.name.substring(0, 2)
                            : '/'),
                      ),
                    ),
                    Expanded(
                        flex: 5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Comentario:',
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            //Text("Fraccionamiento Arboledas"),
                            Text(e.createdAt,
                                style: TextStyle(
                                    fontWeight: FontWeight.w200,
                                    fontSize: 12.0,
                                    color: Colors.black45)),
                          ],
                        )),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 17.5, bottom: 2.0, right: 2.0, left: 2.0),
                  child: Text(
                    e.content,
                    textAlign: TextAlign.left,
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Row(
                  children: [
                    /* Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          FontAwesome.heart_empty,
                          color: Theme.of(context).primaryColor,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text("Me gusta"),
                        )
                      ],
                    )), */
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Icon(
                          FontAwesome.commenting,
                          color: Theme.of(context).primaryColor,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Text("Por ${e.user.name}"),
                        )
                      ],
                    )),
                  ],
                )
              ],
            ),
          ),
        ),
        // Divider(
        //   color: Colors.black38,
        //   height: 1,
        // ),
      ],
    );
  }

  /// =============================================
  /// Function
  /// update post in the TVCINO DB
  /// =============================================
  Future<bool> _reportPost() async {
    print("TV - ADD POST TO DB");

    String title = _post.title;
    int type = 1;
    int status = _post.status! + 1;
    String? shortDescription = _post.shortDescription;
    String? content = _post.shortDescription;
    //String userId = preferences.getString('userId') as String;
    String categoryId = "55cbe008-c6b6-45df-b86f-5b5c11b25024";

    String url = CustomFunctions.url + "/posts/${_post.id}";

    try {
      Map<String, String> datos = Map<String, String>();

      shortDescription = _post.shortDescription!.replaceAll(RegExp('"'), '\'');
      content = _post.shortDescription!.replaceAll(RegExp('"'), '\'');

      datos["data"] =
          '{"title": "$title", "type": $type, "status": $status, "short_description": "$shortDescription","content": "$content", "date": "${_post.date}", "category_id": "$categoryId"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken();
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      await request.send().then((response) async {
        if (response.statusCode == 200) {
          print("Reported!");
          return true;
          //============================================/
          /// 200 is everything ok
          //============================================/
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          return false;
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      return false;
    }
    return true;
  }
}
