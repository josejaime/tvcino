import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/task.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class ImportantTasksPage extends StatefulWidget {
  @override
  _ImportantTasksPageState createState() => _ImportantTasksPageState();
}

class _ImportantTasksPageState extends State<ImportantTasksPage> {
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  late Dio dio;
  List<Task> _listOfTasks = List<Task>.empty(growable: true);
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  @override
  initState() {
    _user = instance.currentUser as auth.User;
    getImportantTasks();
    super.initState();
  }

  /// ==============================
  /// Get posts from server
  /// ==============================
  getImportantTasks() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String neighborhoodId = preferences.getString('neighborhoodId') as String;

    Response response = await dio
        .get('/tasks?neighborhood_id=$neighborhoodId&priority=3')
        .catchError((onError) {
      print("ERROR DOING THE CALL");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            _listOfTasks.add(Task.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SingleChildScrollView(
              child: Column(
              children: [
                Align(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.5),
                    child: Text(
                      "Tareas de prioridad alta en el vecindario",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  ),
                ),
                _error
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: 28.0,
                            color: Colors.black,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(_message, style: TextStyle(color: Colors.black)),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: _listOfTasks.isNotEmpty
                              ? _listOfTasks.map((e) {
                                  return buildTask(e);
                                }).toList()
                              : [
                                  Text(
                                    "No hay tareas urgentes",
                                    style: TextStyle(color: Colors.black),
                                  )
                                ],
                        ),
                      ),
              ],
            )),
    );
  }

  GestureDetector buildTask(Task task) {
    Color color = Colors.white;

    if (task.priority == 1) {
      color = Colors.green;
    } else if (task.priority == 2) {
      color = Colors.orange;
    } else if (task.priority >= 3) {
      color = Colors.red;
    }

    return GestureDetector(
      onTap: () {
        print("Gesture detector");
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 2.0,
        child: Container(
          padding: EdgeInsets.all(15.0),
          width: MediaQuery.of(context).size.width * 0.9,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        "Tarea prioridad alta",
                        style: Theme.of(context).primaryTextTheme.headline4,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8.0,
              ),
              Text(task.content as String),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Creada por:",
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Text(task.createdBy!.name),
                    ],
                  ),
                  CircleAvatar(
                    radius: 20.0,
                    backgroundColor: color,
                    child: Text(
                      task.user!.name.substring(0, 2).toUpperCase(),
                      style: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: 15.0),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
