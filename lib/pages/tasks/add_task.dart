import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
//import 'package:tvcino/pages/user/admin_user.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:http/http.dart' as http;

import 'index_tasks.dart';

class AddTaskPage extends StatefulWidget {
  @override
  _AddTaskPageState createState() => _AddTaskPageState();
}

class _AddTaskPageState extends State<AddTaskPage> {
  bool _high = false;
  bool _medium = false;
  bool _low = true;

  TextEditingController _contentController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  int priority = 1;

  /// ==============================
  /// DIO and more
  /// ==============================
  late Dio dio;
  bool _isLoading = false;
  bool _error = false;
  String _message = "";
  late SharedPreferences preferences;
  List<InterUser> _listOfUsers = List<InterUser>.empty(growable: true);
  String _selectedValue = "";

  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getNeighborhoodUsers();
    getUserInformation();
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    //token = await _user.getIdToken();
    //print("The user Token is = $token");
  }

  /// =================================
  /// Get users fomr the neighborhood
  /// =================================
  getNeighborhoodUsers() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers['authorization'] = "$token";
    bool safeCall = true;

    preferences = await SharedPreferences.getInstance();
    String neighborhoodId = preferences.getString('neighborhoodId') as String;
    print("TV Neighborhood Id is : $neighborhoodId");

    Response response = await dio
        .get('/neighborhoods/$neighborhoodId/users')
        .catchError((onError) {
      print("ERRORORORORORO");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        //List data = response.data;
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print(data[i]['name']);
            //_listOfUsers.add(InterUser.fromJson(_data[i]));
            _listOfUsers.add(InterUser.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
            _selectedValue = _listOfUsers[0].id;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            _error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addTask() async {
    print("TV - ADD GROUP TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    preferences = await SharedPreferences.getInstance();
    String content = _contentController.text;
    String name = _nameController.text;
    String userId = preferences.getString('userId') as String;

    String url = CustomFunctions.url + "/tasks/";

    try {
      if (_high) {
        priority = 3;
      }
      if (_medium) {
        priority = 2;
      }
      if (_low) {
        priority = 1;
      }
      String data =
          '{"content": "$content", "name": "$name", "limit_date": "14-01-2021T13:28:51", "priority": "$priority", "user_id": "$_selectedValue", "created_by": "$userId", "active": "true"}';

      print("TV - DATA");
      print(data.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      http
          .post(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
        body: data,
      )
          .then((response) {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          print("TV - TASK CREATED ALL OK");
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          /* Navigator.of(context).pushNamedAndRemoveUntil(
              '/tasks', (Route<dynamic> route) => false); */
          Navigator.pushNamed(context, '/admin');
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => IndexTasksPage()));
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV something went wrong ${response.request}");
          print("TV HTTP ERROR - Status Code ${response.body}");
          setState(() {
            _error = true;
            _isLoading = false;
            _message = "Hubo un error en el servicio intenta más tarde";
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
      });
      return false;
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Container(
                    width: width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 3.5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.97,
                          padding: EdgeInsets.all(20.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Text(
                            "Nueva Tarea",
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.97,
                          padding: EdgeInsets.all(20.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Column(
                            children: [
                              Form(
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'Asigna un usuario',
                                          textAlign: TextAlign.left,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline4,
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    _listOfUsers.isNotEmpty
                                        ? DropdownButton<String>(
                                            value: _selectedValue,
                                            isExpanded: true,
                                            isDense: false,
                                            icon: Icon(Icons.arrow_downward),
                                            iconSize: 24,
                                            elevation: 16,
                                            onChanged: (String? newValue) {
                                              setState(() {
                                                _selectedValue =
                                                    newValue as String;
                                              });
                                              print(
                                                  'TV - Selected new value $_selectedValue');
                                            },
                                            items: _listOfUsers
                                                .map<DropdownMenuItem<String>>(
                                                    (InterUser user) {
                                              return DropdownMenuItem<String>(
                                                value: user.id,
                                                child: Text(user.name),
                                              );
                                            }).toList(),
                                          )
                                        : Text('No hay usuarios disponibles'),
                                    TextFormField(
                                      controller: _nameController,
                                      decoration: InputDecoration(
                                          labelText: "Nombre",
                                          hintText:
                                              "Cuál nombre le darías a la tarea"),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    TextField(
                                      maxLines: 4,
                                      controller: _contentController,
                                      decoration: InputDecoration.collapsed(
                                          hintText: "¿Qué se debe hacer?"),
                                    ),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Text('Prioridad'),
                                    Container(
                                      height: 85.0,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: [
                                          ChoiceChip(
                                            selectedColor: Colors.red,
                                            label: Text("Alta"),
                                            selected: _high,
                                            onSelected: (bool value) {
                                              setState(() {
                                                _high = value;
                                                _medium = false;
                                                _low = false;
                                              });
                                            },
                                          ),
                                          SizedBox(
                                            width: 2.0,
                                          ),
                                          ChoiceChip(
                                            label: Text("Media"),
                                            selectedColor: Colors.orange,
                                            selected: _medium,
                                            onSelected: (bool value) {
                                              setState(() {
                                                _high = false;
                                                _medium = value;
                                                _low = false;
                                              });
                                            },
                                          ),
                                          SizedBox(
                                            width: 2.0,
                                          ),
                                          ChoiceChip(
                                              label: Text("Baja"),
                                              selectedColor: Colors.green,
                                              selected: _low,
                                              onSelected: (bool selected) {
                                                print("Hello there $selected");
                                                setState(() {
                                                  _high = false;
                                                  _medium = false;
                                                  _low = selected;
                                                });
                                              }),
                                          SizedBox(
                                            width: 2.0,
                                          ),
                                        ],
                                      ),
                                    ),
                                    TextButton(
                                      child: Text(
                                        "Crear",
                                        style: TextStyle(fontSize: 20.0),
                                      ),
                                      onPressed: () {
                                        _addTask();
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
    );
  }
}
