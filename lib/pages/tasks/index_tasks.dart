import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/task.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/tasks/add_task.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart' as auth;

class IndexTasksPage extends StatefulWidget {
  @override
  _IndexTasksPageState createState() => _IndexTasksPageState();
}

class _IndexTasksPageState extends State<IndexTasksPage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  //
  List<Task> _listOfTasks = List<Task>.empty(growable: true);
  bool _isLoading = false;
  bool _error = false;
  String _message = "";
  late SharedPreferences preferences;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getTasks();
    super.initState();
  }

  ///==============================
  /// Now Lets get the comments
  ///==============================
  Future<bool> update(bool status, String id, int index) async {
    String url = CustomFunctions.url;
    if (status) {
      url += '/tasks/$id/postpone';
    } else {
      url += '/tasks/$id/enable';
    }

    print("TV URL UPDATE TASK = $url");

    try {
      setState(() {
        _isLoading = true;
      });
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      //print(token);

      var postUri = Uri.parse(url);
      http.put(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
      ).then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          //============================================/

          /* showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: Text("¡Tarea Actualizada!"),
                //content: Text("¿Te quieres unir?"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Aceptar"),
                    color: Theme.of(context).buttonTheme.colorScheme.primary,
                    onPressed: () async {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          ); */

          setState(() {
            _listOfTasks[index].active = !status;
            _isLoading = false;
          });
        } else {
          print("TV - ADD as Favorte went wrong ${response.statusCode}");
          print("TV - something went wrong ${response.request}");
          print("TV - HTTP ERROR - Status Code ${response.body}");
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _isLoading = true;
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _isLoading = true;
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _isLoading = true;
      });
      return false;
    }
    return false;
  }

  ///==============================
  /// Now Lets get the comments
  ///==============================
  Future<bool> delete(String id, int index) async {
    String url = CustomFunctions.url;

    url += '/tasks/$id';
    print("TV URL UPDATE TASK = $url");

    try {
      setState(() {
        _isLoading = true;
      });
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      //print(token);

      var postUri = Uri.parse(url);
      http.delete(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
      ).then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          //============================================/

          /* showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: Text("¡Tarea Actualizada!"),
                //content: Text("¿Te quieres unir?"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Aceptar"),
                    color: Theme.of(context).buttonTheme.colorScheme.primary,
                    onPressed: () async {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          ); */

          setState(() {
            _listOfTasks.removeAt(index);
            _isLoading = false;
          });
        } else {
          print("TV - ADD as Favorte went wrong ${response.statusCode}");
          print("TV - something went wrong ${response.request}");
          print("TV - HTTP ERROR - Status Code ${response.body}");
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _isLoading = true;
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _isLoading = true;
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _isLoading = true;
      });
      return false;
    }
    return false;
  }

  ///==============================
  /// Now Lets get the comments
  ///==============================
  Future<bool> getTasks() async {
    bool safeCall = true;
    Dio dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers['authorization'] = "$token";
    preferences = await SharedPreferences.getInstance();
    String neighborhoodId = preferences.getString('neighborhoodId') as String;

    setState(() {
      _isLoading = true;
      _error = false;
      _message = "";
    });

    Response response = await dio
        .get('/tasks?neighborhood_id=$neighborhoodId')
        .catchError((onError) {
      print("TV - ERROR MAKING CALL TO TASKS ${onError.toString()}");
      safeCall = false;
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un problema con el servidor, intenta más tarde";
      });
    });

    if (safeCall) {
      List data = response.data['items'];
      if (data.length > 0) {
        setState(() {
          _isLoading = false;
          _error = false;
          _message = "";
        });
        print(data);
        for (int i = 0; i < data.length; i++) {
          print(data[i]['content']);
          setState(() {
            _listOfTasks.add(Task.fromJson(data[i]));
          });
        }
        setState(() {
          _isLoading = false;
          _error = false;
          _message = "";
        });
      } else {
        /// There is not comments
        print("TV - NEIGHBORHOOD WITH NO TASKS");
        setState(() {
          _isLoading = false;
          _error = true;
          _message = "No hay tareas en este momento";
        });
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(),
      body: SingleChildScrollView(
        child: Container(
          width: width,
          child: Column(
            children: [
              Divider(
                indent: 15.0,
                endIndent: 15.0,
              ),
              Text(
                "Tareas".toUpperCase(),
                style: Theme.of(context).textTheme.headline3,
              ),
              Divider(
                indent: 15.0,
                endIndent: 15.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 10.0),
                child: Container(
                  width: width * 0.9,
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddTaskPage()));
                    },
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Nueva tarea",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize:
                                Theme.of(context).textTheme.headline6!.fontSize,
                            fontFamily: Theme.of(context)
                                .textTheme
                                .headline6!
                                .fontFamily,
                          ),
                          //style: Theme.of(context).textTheme.headline5.fontFamily,
                        )),
                        Icon(Icons.add),
                      ],
                    ),
                  ),
                ),
              ),
              _isLoading
                  ? TvcinoCircularProgressBar(
                      color: Colors.white,
                      textColor: Theme.of(context).primaryColor,
                    )
                  : _error
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.error_outline,
                              size: 28.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(_message),
                          ],
                        )
                      : Row(),
              _listOfTasks.isNotEmpty ? buildTaskList(_listOfTasks) : Row(),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  Widget buildTaskList(List<Task> tasks) {
    int i = 0;
    return Column(
      children: tasks.map((e) {
        i++;
        return buildCard(e, (i - 1));
      }).toList(),
    );
  }

  GestureDetector buildCard(Task task, int index) {
    Color color = Colors.white;

    if (task.priority == 1) {
      color = Colors.green;
    } else if (task.priority == 2) {
      color = Colors.orange;
    } else if (task.priority >= 3) {
      color = Colors.red;
    }

    return GestureDetector(
      onTap: () {
        print("Gesture detector");
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10), // if you need this
          /* side: BorderSide(
                    color: Colors.green,
                    width: 1,
                  ), */
        ),
        elevation: 2.0,
        child: Container(
          padding: EdgeInsets.all(15.0),
          width: MediaQuery.of(context).size.width * 0.9,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        "${task.name}",
                        style: Theme.of(context).primaryTextTheme.headline4,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: color,
                      ),
                      height: 20,
                      width: 20,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8.0,
              ),
              Text(task.content as String),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: task.active
                          ? Theme.of(context).buttonTheme.colorScheme!.primary
                          : Colors.transparent,
                    ),
                    onPressed: () async {
                      print("click status index $index");
                      await update(task.active, task.id, index);
                    },
                    child: Text(task.active ? "Activa" : "Pausada"),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.red,
                      primary: Colors.white,
                    ),
                    onPressed: () async {
                      print("click status index $index");
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          // return object of type Dialog
                          return AlertDialog(
                            title: Text("¡Borrar!"),
                            content:
                                Text("¿Realmente quieres borrar esta tarea?"),
                            actions: <Widget>[
                              TextButton(
                                child: Text("Cerrar"),
                                onPressed: () async {
                                  Navigator.of(context).pop();
                                },
                              ),
                              TextButton(
                                style: TextButton.styleFrom(
                                  backgroundColor: Colors.red,
                                  primary: Colors.white,
                                ),
                                child: Text("Borrar"),
                                onPressed: () async {
                                  await delete(task.id, index);
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Text("Borrar"),
                  ),
                  CircleAvatar(
                    radius: 20.0,
                    backgroundColor: const Color(0xFF778899),
                    child: Text(
                      task.user!.name.substring(0, 2).toUpperCase(),
                      style: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: 15.0),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
