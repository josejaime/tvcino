import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:http/http.dart' as http;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/directories/directories.dart';
import 'package:tvcino/pages/search/filters.dart';
import 'package:tvcino/utils/custom_functions.dart';

class AddPlacePage extends StatefulWidget {
  @override
  _AddPlacePageState createState() => _AddPlacePageState();
}

class _AddPlacePageState extends State<AddPlacePage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  ///////////////////////////////////
  bool isChecked = true;
  List<String> tagList = [
    "Fontanería",
    "Mascotas",
    "Mecánico",
    "Limpieza",
    "Tecnología",
    "Lavandería",
    "Otros",
    "Comida",
    "Jardinería",
    "Protección",
    "Aprendizaje",
    "Construcción",
    "Salud",
    "Electricidad",
  ];
  List<String> tagIds = [
    "0f5e164b-c3e0-415a-ba62-a76b27cb90bf",
    "12742472-8e32-4488-a313-bc624e94ec70",
    "1a8770c4-ea4a-46d8-a2c6-2c0cd9ee2d70",
    "29b5cbff-4503-4d71-be3e-c44cd45c81d3",
    "4fa78691-3b79-43f9-adad-9f228b6301ae",
    "6b758959-3ac7-4fa1-aa75-3c710775b65f",
    "83e4ac69-50d3-4fb8-8a9f-e7ca045804a0",
    "8c5096da-1355-4d75-bb34-68614e07d851",
    "9240bf3f-1386-4002-8ca1-aaf13a8aa28f",
    "984711d5-1ad1-40e0-860e-bf187e2c1d41",
    "cd5a6b2a-50c1-41a9-9d7d-de8d8d439ec3",
    "d441a166-aa8e-46b6-8864-36122090044d",
    "e0af9ebb-b2f8-42d9-a784-5279544d4548",
    "ee826ea8-37d7-4522-bf10-d1ad174d43b1",
  ];

  List<Widget> choices = List.empty();
  String selectedChoice = "";
  late String tagId;

  /// ==============================
  /// Images
  /// ==============================
  File? _image;
  final picker = ImagePicker();
  TextEditingController _imageTextController = TextEditingController();
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  bool _errorText = false;
  //===============================*/
  // Controlls
  //===============================*/
  final GlobalKey<FormState> _placeAddKey =
      GlobalKey<FormState>(debugLabel: '_placeAddKey');
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _locationController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _stateController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _whatsappController = TextEditingController();
  final TextEditingController _facebookController = TextEditingController();
  final TextEditingController _instagramController = TextEditingController();

  String uuid = "";
  String userId = "";
  //
  String name = "";
  String location = "";
  int rating = 0;
  int votes = 0;
  String description = "";
  String phone = "";
  String facebook = "";
  String tags = "";
  String instagram = "";
  String whatsapp = "";
  String active = "";
  String neighborhoodId = "";
  //
  String country = "";
  String city = "";
  String state = "";

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    tagId = tagIds[0];
    selectedChoice = tagList[0];
    getUserInformation();
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
    print("The user Token is = $token");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print("TV Retrived data");
    country = preferences.getString('neighbordhoodCountry') ?? '';
    state = preferences.getString('neighbordhoodState') ?? '';
    city = preferences.getString('neighbordhoodCity') ?? '';
    //location = preferences.getString('address') as String;
    location = '';
    _locationController.text = location;
    _cityController.text = city;
    _stateController.text = state;
    print("TV User country  = ${preferences.getString('country')}");
    print("TV User state  = ${preferences.getString('state')}");
    print("TV User city  = ${preferences.getString('city')}");
    print("TV User address  = ${preferences.getString('address')}");
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addPlace() async {
    print("TV - ADD PLACE TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    uuid = preferences.getString('uuid') as String;
    location = _locationController.text;
    userId = preferences.getString('userId') as String;
    name = _titleController.text;
    location = _locationController.text;
    state = _stateController.text;
    city = _cityController.text;
    rating = 0;
    votes = 0;
    description = _descriptionController.text;
    phone = _phoneController.text;
    facebook = _facebookController.text;
    tags = tagId;
    instagram = _instagramController.text;
    whatsapp = _whatsappController.text;
    active = true.toString();
    neighborhoodId = preferences.getString('neighborhoodId') as String;

    String _fileName = _imageTextController.text.split('/').last;

    String url = CustomFunctions.url + "/places/";

    try {
      Map<String, String> datos = Map<String, String>();

      description = description.replaceAll(RegExp('"'), '\'');

      datos["data"] =
          '{"name": "$name", "location": "$location", "state": "$state", "city": "$city", "country": "$country", "rating": $rating, "votes": $votes, "description": "$description", "phone": "$phone", "facebook": "$facebook", "tags": ["$tags"], "instagram": "$instagram", "whatsapp": "$whatsapp", "active": true, "neighborhood_id": "$neighborhoodId", "user_id": "$userId"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("POST", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      if (_image != null) {
        //============================================/
        /// CHECK IF THE IMAGE IS AVAILABLE
        //============================================/
        request.files.add(http.MultipartFile.fromBytes(
            'image', File(_image!.path).readAsBytesSync(),
            filename: _fileName));
      }
      request.send().then((response) async {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - PLACE CREATED ALL OK");
          /*Navigator.of(context).pushNamedAndRemoveUntil(
              '/filters', (Route<dynamic> route) => false);*/
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => FiltersPage()));
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DirectoriesPage(
                        tagFilterId: tags,
                      )));
          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
          CustomFunctions.showBottomMessage(Icons.warning, '¡Error!',
              'Hubo un error al guardar el lugar.', context);
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      CustomFunctions.showBottomMessage(Icons.warning, '¡Error!',
          'Hubo un error al guardar el lugar.', context);
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      CustomFunctions.showBottomMessage(Icons.warning, '¡Error!',
          'Hubo un error al guardar el lugar.', context);
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      CustomFunctions.showBottomMessage(Icons.warning, '¡Error!',
          'Hubo un error al guardar el lugar.', context);
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      CustomFunctions.showBottomMessage(Icons.warning, '¡Error!',
          'Hubo un error al guardar el lugar.', context);
      return false;
    }
    return false;
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print("TV: Image selected");
        print(_image);
        //_imageTextController.text = _image.path;
        _imageTextController.text = _image!.path.split('/').last;
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /***********/
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Nuevo Negocio",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información general",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Form(
                              key: _placeAddKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: _titleController,
                                    decoration:
                                        InputDecoration(labelText: "Nombre"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar tu nombre";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con un nombre más largo";
                                      }
                                      return null;
                                    },
                                  ),

                                  /// =================================
                                  /// Address
                                  /// =================================
                                  TextFormField(
                                    controller: _cityController,
                                    decoration:
                                        InputDecoration(labelText: "Ciudad"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar una dirección";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con una dirección más larga";
                                      }
                                      return null;
                                    },
                                  ),
                                  TextFormField(
                                    controller: _stateController,
                                    decoration:
                                        InputDecoration(labelText: "Estado"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar una dirección";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con una dirección más larga";
                                      }
                                      return null;
                                    },
                                  ),
                                  TextFormField(
                                    controller: _locationController,
                                    decoration:
                                        InputDecoration(labelText: "Calle"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar una dirección";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con una dirección más larga";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 3.0,
                                  ),
                                  Text(
                                      "* Se está la dirección de tu vecindario actual"),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  //MultiSelectChip(tagList),
                                  Wrap(
                                    children: _buildChoiceList(),
                                  ),
                                  SizedBox(
                                    height: 8.0,
                                  ),
                                  TextField(
                                    maxLines: 6,
                                    controller: _descriptionController,
                                    textCapitalization:
                                        TextCapitalization.sentences,
                                    decoration: InputDecoration(
                                        errorText: _errorText
                                            ? "* No olvides llenar este campo"
                                            : null,
                                        hintText:
                                            "Agrega una descripción aquí"),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  _image != null
                                      ? Column(
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.95,
                                              height: 150,
                                              child: Image.file(
                                                _image!,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 5.0),
                                              child: Text(
                                                "* Para una mejor visualización utiliza imágenes horizontales",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w100,
                                                    fontSize: 12.5,
                                                    fontStyle:
                                                        FontStyle.italic),
                                              ),
                                            ),
                                          ],
                                        )
                                      : Offstage(),
                                  TextFormField(
                                    onTap: () {
                                      getImage();
                                    },
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: "Imagen",
                                      suffixIcon: Icon(Icons.file_upload),
                                    ),
                                    controller: _imageTextController,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes poner una imagen";
                                      }
                                      return null;
                                    },
                                  ),
                                  TextFormField(
                                    controller: _phoneController,
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                      labelText: "Teléfono",
                                    ),
                                  ),
                                  TextFormField(
                                    controller: _whatsappController,
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                        labelText: "Número de Whatsapp"),
                                  ),
                                  TextFormField(
                                    controller: _facebookController,
                                    keyboardType: TextInputType.url,
                                    decoration: InputDecoration(
                                        labelText: "Facebook",
                                        hintText: "https://facebook.com/"),
                                  ),
                                  TextFormField(
                                    controller: _instagramController,
                                    keyboardType: TextInputType.url,
                                    decoration: InputDecoration(
                                        labelText: "Instagram",
                                        hintText: "https://instagram.com/"),
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    /* padding: EdgeInsets.symmetric(
                                        horizontal: 35, vertical: 12), */
                                    child: Text(
                                      "Publicar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    /* color: Theme.of(context)
                                        .buttonTheme
                                        .colorScheme
                                        .primary, */
                                    onPressed: () {
                                      if (_placeAddKey.currentState!
                                          .validate()) {
                                        if (_descriptionController
                                            .text.isNotEmpty) {
                                          print("TV - VALIDATE TRUE");

                                          setState(() {
                                            _errorText = false;
                                          });
                                          _addPlace();
                                        } else {
                                          print("TV - VALIDATE FALSE");
                                          setState(() {
                                            _errorText = true;
                                          });
                                          CustomFunctions.showBottomMessage(
                                              Icons.warning,
                                              '¡Advertencia!',
                                              'Recuerda poner una descrpción.',
                                              context);
                                        }
                                      } else {
                                        print("TV - VALIDATE FALSE 1");
                                        CustomFunctions.showBottomMessage(
                                            Icons.warning,
                                            '¡Advertencia!',
                                            'Recuerda rellenar todos los campos.',
                                            context);
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  _buildChoiceList() {
    List<Widget> choices = List.empty(growable: true);
    tagList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.only(right: 2.0, left: 2.0),
        child: ChoiceChip(
          visualDensity: VisualDensity.compact,
          label: Text(item),
          selected: selectedChoice == item,
          onSelected: (selected) {
            setState(() {
              selectedChoice = item;
              tagId = tagIds[tagList.indexOf(selectedChoice)];
              print(
                  "TV - SELECTED TAG CHOICE = $selectedChoice and ID [$tagId]");
            });
          },
        ),
      ));
    });
    return choices;
  }
}

class MultiSelectChip extends StatefulWidget {
  final List<String> tagList;
  MultiSelectChip(this.tagList);
  @override
  _MultiSelectChipState createState() => _MultiSelectChipState();
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  String selectedChoice = "";
  // this function will build and return the choice list
  _buildChoiceList() {
    List<Widget> choices = List.empty(growable: true);
    widget.tagList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.only(right: 2.0, left: 2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoice == item,
          onSelected: (selected) {
            setState(() {
              selectedChoice = item;
              print("TV - SELECTED TAG CHOICE = $selectedChoice");
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
