import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/place.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/directories/update_place.dart';
import 'package:url_launcher/url_launcher.dart';

class PlacePage extends StatefulWidget {
  final Place place;

  PlacePage({
    required this.place,
  });

  @override
  _PlacePageState createState() => _PlacePageState();
}

class _PlacePageState extends State<PlacePage> {
  /// ==============================
  /// DIO GET THE POSTS
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = false;
  bool _error = false;
  //Place _place;
  String? _userId;

  @override
  void initState() {
    _getPreferences();
    super.initState();
  }

  _getPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    /* print("TV Retrived data");
    print("TV User Name = ${preferences.getString('name')}");
    print("TV User userame = ${preferences.getString('username')}");
    print("TV User email  = ${preferences.getString('email')}");
    print("TV User UUID  = ${preferences.getString('uuid')}");
    print("TV User ID  = ${preferences.getString('userId')}"); */
    setState(() {
      _userId = preferences.getString('userId') as String;
      print('TV - userId $_userId && placeId ${widget.place.id}');
    });
    /* print("TV User birthdate  = ${preferences.getString('birthdate')}");
    print("TV User country  = ${preferences.getString('country')}");
    print("TV User state  = ${preferences.getString('state')}");
    print("TV User city  = ${preferences.getString('city')}");
    print("TV User address  = ${preferences.getString('address')}"); */

    /// CONTROL
    /* print("TV - CONTROL");
    print("TV User Created  = ${preferences.getBool('userCreated')}");
    print("TV Neighborhood ID = ${preferences.getString('neighborhoodId')}");
    print("TV Neighborhood set? = ${preferences.getBool('neighborhoodSet')}"); */
  }

  /// ==============================
  /// Get posts from server
  /// ==============================
  /* getPost() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    bool safeCall = true;
    print("ID- ${widget.place.id}");
    Response response =
        await dio.get('/place/${widget.place.id}').catchError((onError) {
      print("ERRORORORORORO ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response);
        print("DIO Trying to convert data");
        //List data = response.data['items'];
        setState(() {
          _place = Place.fromJson(response.data);
          _isLoading = false;
          _error = false;
          _message = "";
        });
        print("Converted POST:");
        print(_place);
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  } */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
        title: Image(
          image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
          height: 21.0,
        ),
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Theme.of(context).primaryColor,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SafeArea(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height / 3.8,
                          decoration: BoxDecoration(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.7),
                              image: DecorationImage(
                                image: NetworkImage(widget.place.image),
                                fit: BoxFit.cover,
                              )),
                          child: Stack(
                            fit: StackFit.expand,
                            children: [
                              Positioned(
                                bottom: 0,
                                right: -15,
                                child: Column(
                                  children: [
                                    _userId != null &&
                                            _userId == widget.place.userId
                                        ? RawMaterialButton(
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          UpdatePlacePage(
                                                            place: widget.place,
                                                          )));
                                            },
                                            shape: CircleBorder(),
                                            fillColor: Colors.white,
                                            child: Icon(Icons.settings,
                                                color: Theme.of(context)
                                                    .primaryColor),
                                          )
                                        : Offstage(),
                                    /* RawMaterialButton(
                                      onPressed: () {
                                        print("Icon Share Button");
                                      },
                                      shape: CircleBorder(),
                                      fillColor: Colors.white,
                                      child: Icon(FontAwesome.forward,
                                          color:
                                              Theme.of(context).primaryColor),
                                    ), */
                                    /* RawMaterialButton(
                                      onPressed: () {
                                        print("Icon Share Button");
                                      },
                                      shape: CircleBorder(),
                                      fillColor: Colors.white,
                                      //child: Icon( Icons.thumb_up , color: Theme.of(context).primaryColor,),
                                      child: Icon(FontAwesome.heart_empty,
                                          color:
                                              Theme.of(context).primaryColor),
                                    ), */
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        /*====================================*/
                        /* Event title */
                        /*====================================*/
                        Container(
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              widget.place.name,
                              style: Theme.of(context).textTheme.headline1,
                              textScaleFactor: 0.7,
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        /*====================================*/
                        /* Event Chips */
                        /*====================================*/
                        Row(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 4.0),
                              child: Wrap(
                                spacing: 10.0,
                                /* children: [
                                  Chip(
                                    label: Text('Directorio'),
                                  ),
                                ], */
                                children: widget.place.tags.isNotEmpty
                                    ? widget.place.tags.map((e) {
                                        return Chip(label: Text(e.name ?? ''));
                                      }).toList()
                                    : [Chip(label: Text('Directorio'))],
                              ),
                            ),
                          ],
                        ),
                        /*====================================*/
                        /* Event Place and Time */
                        /*====================================*/
                        Divider(),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Row(
                            children: [
                              Expanded(
                                flex: 3,
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          8.0, 4.0, 8.0, 4.0),
                                      child: Icon(Icons.place,
                                          color:
                                              Theme.of(context).primaryColor),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                            "${widget.place.state} (${widget.place.city})"),
                                        Text(
                                          widget.place.location,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600),
                                        ),
                                        /* Text(
                                          "Col. Providencia",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w100),
                                          textScaleFactor: 0.9,
                                        ), */
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              /* Expanded(
                                flex: 2,
                                child: Row(
                                  children: [
                                    Flexible(
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.star,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          Icon(
                                            Icons.star,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          Icon(
                                            Icons.star_border,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ), */
                            ],
                          ),
                        ),
                        /*====================================*/
                        /* Event Info */
                        /*====================================*/
                        Divider(),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Más Información',
                                style: Theme.of(context).textTheme.headline4,
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(widget.place.description),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            widget.place.whatsapp == null ||
                                    widget.place.whatsapp.isEmpty
                                ? Container()
                                : IconButton(
                                    icon: Icon(FontAwesome.whatsapp),
                                    onPressed: widget.place.whatsapp == null ||
                                            widget.place.whatsapp.isEmpty
                                        ? null
                                        : () {
                                            print("TV - PLACE WHATSAPP CLICK");
                                            //_launchMyURL(widget.place.whatsapp);
                                            _launchWhatsapp();
                                          },
                                  ),
                            widget.place.facebook == null ||
                                    widget.place.facebook.isEmpty
                                ? Container()
                                : IconButton(
                                    icon: Icon(FontAwesome.facebook),
                                    onPressed: widget.place.facebook == null ||
                                            widget.place.facebook.isEmpty
                                        ? null
                                        : () {
                                            print("TV - PLACE FACEBOOK CLICK");
                                            _launchMyURL(widget.place.facebook);
                                          },
                                  ),
                            widget.place.instagram == null ||
                                    widget.place.instagram.isEmpty
                                ? Container()
                                : IconButton(
                                    icon: Icon(FontAwesome.instagram),
                                    onPressed: widget.place.instagram == null ||
                                            widget.place.instagram.isEmpty
                                        ? null
                                        : () {
                                            print("TV - PLACE INSTAGRAM CLICK");
                                            _launchMyURL(
                                                widget.place.instagram);
                                          },
                                  ),
                          ],
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TextButton(
                                    /* color: Theme.of(context)
                                        .buttonTheme
                                        .colorScheme
                                        .primary, */
                                    onPressed: widget.place.phone == null ||
                                            widget.place.phone.isEmpty
                                        ? null
                                        : () {
                                            _launchURL();
                                          },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                            widget.place.phone == null ||
                                                    widget.place.phone.isEmpty
                                                ? "Teléfono no disponible"
                                                : "Llamar".toUpperCase(),
                                            textAlign: TextAlign.center),
                                      ],
                                    )),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 50.0,
                        )
                      ],
                    ),
                  ),
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  _launchURL() async {
    String url = 'tel:${widget.place.phone}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchWhatsapp() async {
    String url = 'https://wa.me/${widget.place.whatsapp}/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _launchMyURL(String url) async {
    //String url = 'tel:${widget.place.phone}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
