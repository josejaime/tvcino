import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/place.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_card_first.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/directories/add_place.dart';
import 'package:tvcino/pages/directories/place_page.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class DirectoriesPage extends StatefulWidget {
  final String? tagFilterId;

  const DirectoriesPage({Key? key, this.tagFilterId}) : super(key: key);

  @override
  _DirectoriesPageState createState() => _DirectoriesPageState();
}

class _DirectoriesPageState extends State<DirectoriesPage> {
  List<String> filterList = [
    "Comida",
    "Limpieza",
    "Ferretería",
    "Lavandería",
    "Servicios",
    "Tintorería",
  ];
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;

  /// ==============================
  /// DIO GET THE Places
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = false;
  bool _error = false;
  bool _empty = false;

  /// ==============================
  /// The Posts
  /// ==============================
  List<Place> _listOfTags = List.empty(growable: true);
  late SharedPreferences preferences;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getPlacesTag();
    super.initState();
  }

  Future<String> _getCity() async {
    preferences = await SharedPreferences.getInstance();
    return preferences.getString('neighbordhoodCity') as String;
  }

  /// ==============================
  /// Get posts from server
  /// ==============================
  getPlacesTag() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
      _empty = false;
    });
    String city = await _getCity();

    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================

    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;

    String url =
        "/places?tag=${widget.tagFilterId}&country=México&start=0&limit=100&";

    if (widget.tagFilterId == '83e4ac69-50d3-4fb8-8a9f-e7ca045804a0') {
      url = "/places?country=México&start=0&limit=100&";
    }
    Response response = await dio.get(url).catchError((onError) {
      print("ERRORORORORORO");
      print("TV DIO ERROR - Status Code ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        //List data = response.data;
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print(data[i]['city'] + " <-> " + city);
            if (data[i]['city'] == city) {
              //_listOfUsers.add(InterUser.fromJson(_data[i]));
              _listOfTags.add(Place.fromJson(data[i]));
            }
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            _error = true;
            _empty = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
        title: Text(
          "Directorio",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Theme.of(context).primaryColor,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      _empty
                          ? Align(
                              child: Container(
                                width: MediaQuery.of(context).size.width / 1.1,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(8.0),
                                  padding: EdgeInsets.all(5.0),
                                  width:
                                      MediaQuery.of(context).size.width / 1.1,
                                  height: 175,
                                  decoration: BoxDecoration(
                                      color: Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                          image: ExactAssetImage(
                                              "assets/images/tuvcino-ilustracion-servicios.png"),
                                          alignment: Alignment.centerRight)),
                                  child: Stack(
                                    children: [
                                      Positioned(
                                        top: 15,
                                        left: 5,
                                        //child: Text("Novedades", style: Theme.of(context).textTheme.headline2 ),
                                        child: Text(
                                            "¡Este \nlugar está \nvacío!",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 24)),
                                      ),
                                      Positioned(
                                        bottom: 2.5,
                                        left: 5,
                                        child: TextButton(
                                          style: TextButton.styleFrom(
                                            backgroundColor: Colors.white,
                                            primary: Colors.black,
                                          ),
                                          onPressed: () {
                                            print(
                                                "TV - CREATE A NEW PLACE PAGE");
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        AddPlacePage()));
                                          },
                                          child: Text('¿Crear uno nuevo?'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          : Offstage(),
                      _empty
                          ? SizedBox(
                              height: 15.0,
                            )
                          : Offstage(),
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 7.5,
                      ),
                      //TODO: Implement search on places
                      /* Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: TextFormField(
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(left: 8),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      borderSide: BorderSide(
                                          color: Theme.of(context).primaryColor,
                                          width: 1.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(15.0)),
                                      borderSide: BorderSide(
                                        color: Theme.of(context).primaryColor,
                                        width: 1.0,
                                      ),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                      borderSide: BorderSide(
                                        width: 1.0,
                                      ),
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                      borderSide: BorderSide(
                                        color: Colors.red,
                                        width: 2.0,
                                      ),
                                    ),
                                    hintText: "¿Qué estás buscando?",
                                    suffixIcon: IconButton(
                                        icon: Icon(
                                          Icons.search,
                                          color: Theme.of(context).primaryColor,
                                        ),
                                        onPressed: () {})),
                                onFieldSubmitted: (String value) {},
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return "Debes ingresar una búsqueda";
                                  }
                                  if (value.length < 5) {
                                    return "Intenta con mínimo 5 caracteres";
                                  }
                                  return null;
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ), */
                      /* Container(
                        height: 45,
                        child: MultiSelectChip(filterList),
                      ), */
                      SizedBox(
                        height: 5.0,
                      ),
                      Flexible(
                        child: ListView(
                          children: [
                            Align(
                              child: Container(
                                width: MediaQuery.of(context).size.width / 1.1,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(8.0),
                                  padding: EdgeInsets.all(5.0),
                                  width:
                                      MediaQuery.of(context).size.width / 1.1,
                                  height: 175,
                                  decoration: BoxDecoration(
                                      color: Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                          image: ExactAssetImage(
                                              "assets/images/tuvcino-ilustracion-servicios.png"),
                                          alignment: Alignment.centerRight)),
                                  child: Stack(
                                    children: [
                                      Positioned(
                                        top: 15,
                                        left: 5,
                                        //child: Text("Novedades", style: Theme.of(context).textTheme.headline2 ),
                                        child: Text(
                                            "¿Qué \nnecesitas \nvecino?",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 24)),
                                      ),
                                      Positioned(
                                          bottom: 2.5,
                                          left: 10,
                                          child: TextButton(
                                              style: TextButton.styleFrom(
                                                backgroundColor: Colors.white,
                                                primary: Colors.black,
                                              ),
                                              onPressed: () {
                                                print(
                                                    "TV - CREATE A NEW PLACE PAGE");
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            AddPlacePage()));
                                              },
                                              child: Text(
                                                'Crear Negocio',
                                              ))),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            _listOfTags.isNotEmpty
                                ? buildTagList(_listOfTags, context)
                                : Offstage(),
                            SizedBox(
                              height: 15.0,
                            ),
                            //buildCardWithImage(context, 1, "Auto Sospechoso", 'assets/images/imagen_home_auto.jpg'),
                            SizedBox(
                              height: 15.0,
                            ),
                            //buildCardWithImage(context, 2, "Taller de mecánica", 'assets/images/curso_vecinos_1.jpg'),
                            SizedBox(
                              height: 15.0,
                            ),
                            //buildCardWithImage(context, 1, "Clase de Yoga", 'assets/images/yoga_home_.jpg'),
                            SizedBox(
                              height: 15.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }
}

Widget buildTagList(List<Place> tags, BuildContext context) {
  return Wrap(
    children: tags.map((e) {
      //return Text("${e.name}");
      return TvcinoExtendedCard(
        funct: () {
          print("Where Am I?");
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PlacePage(
                place: e,
              ),
            ),
          );
        },
        image: e.image,
        title: e.name,
        subtitle: e.description,
      );
    }).toList(),
  );
}

class MultiSelectChip extends StatefulWidget {
  final List<String> reportList;
  MultiSelectChip(this.reportList);
  @override
  _MultiSelectChipState createState() => _MultiSelectChipState();
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  String selectedChoice = "";
  // this function will build and return the choice list
  _buildChoiceList() {
    List<Widget> choices = List.empty();
    widget.reportList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.only(right: 2.0, left: 2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoice == item,
          onSelected: (selected) {
            setState(() {
              selectedChoice = item;
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      scrollDirection: Axis.horizontal,
      children: _buildChoiceList(),
    );
  }
}
