import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/place.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:http/http.dart' as http;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/directories/directories.dart';
import 'package:tvcino/pages/search/filters.dart';
import 'package:tvcino/utils/custom_functions.dart';

class UpdatePlacePage extends StatefulWidget {
  final Place place;

  UpdatePlacePage({
    required this.place,
  });

  @override
  _UpdatePlacePageState createState() => _UpdatePlacePageState();
}

class _UpdatePlacePageState extends State<UpdatePlacePage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  ///////////////////////////////////
  bool isChecked = true;

  List<Widget> choices = List.empty(growable: true);
  String selectedChoice = "";
  late String tagId;

  /// ==============================
  /// Images
  /// ==============================
  File? _image;
  final picker = ImagePicker();
  TextEditingController _imageTextController = TextEditingController();
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  bool _errorText = false;
  //===============================*/
  // Controlls
  //===============================*/
  final GlobalKey<FormState> _placeUpdateKey =
      GlobalKey<FormState>(debugLabel: '_placeUpdateKey');
  late TextEditingController _titleController;
  late TextEditingController _placeController;
  late TextEditingController _descriptionController;
  late TextEditingController _phoneController;
  late TextEditingController _whatsappController;
  late TextEditingController _facebookController;
  late TextEditingController _instagramController;

  String uuid = "";
  String userId = "";
  //
  String name = "";
  String location = "";
  String description = "";
  String phone = "";
  String facebook = "";
  String instagram = "";
  String whatsapp = "";
  String active = "";

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getUserInformation();
    _titleController = TextEditingController(text: widget.place.name);
    _placeController = TextEditingController(text: widget.place.location);
    _descriptionController =
        TextEditingController(text: widget.place.description);
    _phoneController = TextEditingController(text: widget.place.phone);
    _whatsappController = TextEditingController(text: widget.place.whatsapp);
    _facebookController = TextEditingController(text: widget.place.facebook);
    _instagramController = TextEditingController(text: widget.place.instagram);
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _updatePlace() async {
    print("TV - UPDATE PLACE TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    uuid = preferences.getString('uuid') as String;
    location = _placeController.text;
    //userId = preferences.getString('userId') as String;
    name = _titleController.text;
    location = _placeController.text;
    description = _descriptionController.text;
    phone = _phoneController.text;
    facebook = _facebookController.text;
    instagram = _instagramController.text;
    whatsapp = _whatsappController.text;

    String _fileName = _imageTextController.text.split('/').last;

    String url = CustomFunctions.url + "/places/${widget.place.id}";

    try {
      Map<String, String> datos = Map<String, String>();

      description = description.replaceAll(RegExp('"'), '\'');
      String tags = "";
      for (int i = 0; i < widget.place.tags.length; i++) {
        tags += widget.place.tags[i].id ?? '' + ",";
      }
      tags = tags.substring(0, (tags.length - 1));

      datos["data"] =
          '{"name": "$name", "tags": ["$tags"], "location": "$location", "description": "$description", "phone": "$phone", "facebook": "$facebook", "instagram": "$instagram", "whatsapp": "$whatsapp"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      //print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      if (_image != null) {
        //============================================/
        /// CHECK IF THE IMAGE IS AVAILABLE
        //============================================/
        request.files.add(http.MultipartFile.fromBytes(
            'image', File(_image!.path).readAsBytesSync(),
            filename: _fileName));
      }
      request.send().then((response) async {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - PLACE UPDATED ALL OK");
          //TODO: THIS ONES REDIRECTS WONT ALWAYS WORK
          // FOR EXAMPLE IN WHEN THEY COME FROM THE LISTS
          // TODO: OK WHAT ABOUT THIS, JUST MANAGE THE MESSAGE
          // UPDATED OK, CONGRATS MATE :D
          //Navigator.popUntil(context, ModalRoute.withName('/places'));
          //Navigator.of(context).popUntil(ModalRoute.withName('/places'));
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => FiltersPage()));
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DirectoriesPage(
                        tagFilterId: widget.place.tags[0].id,
                      )));
          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print("TV: Image selected");
        print(_image);
        //_imageTextController.text = _image.path;
        _imageTextController.text = _image!.path.split('/').last;
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /***********/
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Actualizar Lugar",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información general",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Form(
                              key: _placeUpdateKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: _titleController,
                                    decoration:
                                        InputDecoration(labelText: "Nombre"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar tu nombre";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con un nombre más largo";
                                      }
                                      return null;
                                    },
                                  ),
                                  TextFormField(
                                    controller: _placeController,
                                    decoration:
                                        InputDecoration(labelText: "Dirección"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar una dirección";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con una dirección más larga";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  SizedBox(
                                    height: 8.0,
                                  ),
                                  TextField(
                                    maxLines: 6,
                                    controller: _descriptionController,
                                    decoration: InputDecoration(
                                        errorText: _errorText
                                            ? "* No olvides llenar este campo"
                                            : null,
                                        hintText:
                                            "Agrega una descripción aquí"),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  TextFormField(
                                    decoration: InputDecoration(
                                      labelText: "Imagen",
                                      suffixIcon: IconButton(
                                          icon: Icon(Icons.file_upload),
                                          onPressed: () {
                                            getImage();
                                          }),
                                    ),
                                    controller: _imageTextController,
                                  ),
                                  TextFormField(
                                    controller: _phoneController,
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                      labelText: "Teléfono",
                                    ),
                                  ),
                                  TextFormField(
                                    controller: _whatsappController,
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                        labelText: "Número de Whatsapp"),
                                  ),
                                  TextFormField(
                                    controller: _facebookController,
                                    keyboardType: TextInputType.url,
                                    decoration: InputDecoration(
                                        labelText: "Facebook",
                                        hintText: "https://facebook.com/"),
                                  ),
                                  TextFormField(
                                    controller: _instagramController,
                                    keyboardType: TextInputType.url,
                                    decoration: InputDecoration(
                                        labelText: "Instagram",
                                        hintText: "https://instagram.com/"),
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    /* padding: EdgeInsets.symmetric(
                                        horizontal: 35, vertical: 12), */
                                    child: Text(
                                      "Actualizar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    /* color: Theme.of(context)
                                        .buttonTheme
                                        .colorScheme
                                        .primary, */
                                    onPressed: () {
                                      if (_placeUpdateKey.currentState!
                                          .validate()) {
                                        if (_descriptionController
                                            .text.isNotEmpty) {
                                          print("TV - VALIDATE TRUE");

                                          setState(() {
                                            _errorText = false;
                                          });
                                          _updatePlace();
                                        } else {
                                          print("TV - VALIDATE FALSE");
                                          setState(() {
                                            _errorText = true;
                                          });
                                        }
                                      } else {
                                        print("TV - VALIDATE FALSE");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
