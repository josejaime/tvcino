import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/groups/update_group.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class UserCreatedGroups extends StatefulWidget {
  @override
  _UserCreatedGroupsState createState() => _UserCreatedGroupsState();
}

class _UserCreatedGroupsState extends State<UserCreatedGroups> {
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  late Dio dio;
  List<Group> _listOfGroups = List<Group>.empty(growable: true);

  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;

  @override
  initState() {
    _user = instance.currentUser as auth.User;
    getUserCreatedGroups();
    super.initState();
  }

  /// ==============================
  /// Get user groups from server
  /// ==============================
  getUserCreatedGroups() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================

    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    Response response =
        await dio.get('/groups?author_id=$userId').catchError((onError) {
      print("ERROR DOING THE CALL");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            _listOfGroups.add(Group.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SingleChildScrollView(
              child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Align(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.5),
                    child: Text(
                      "Los grupos que has creado",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize:
                              Theme.of(context).textTheme.headline3!.fontSize,
                          fontFamily: Theme.of(context)
                              .textTheme
                              .headline3!
                              .fontFamily),
                    ),
                  ),
                ),
                _error
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: 28.0,
                            color: Colors.white,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(_message, style: TextStyle(color: Colors.white)),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: _listOfGroups.isNotEmpty
                              ? _listOfGroups.map((e) {
                                  return Card(
                                    child: ListTile(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    UpdateGroupPage(
                                                      group: e,
                                                    )));
                                      },
                                      title: Text(e.name),
                                      subtitle: Text(e.shortDescription),
                                      trailing: Icon(
                                        Icons.edit,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    ),
                                  );
                                }).toList()
                              : [
                                  Text(
                                    "No hay nada para mostrar aún",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                        ),
                      ),
              ],
            )),
    );
  }
}
