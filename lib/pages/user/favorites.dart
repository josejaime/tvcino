import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/posts.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/events/event.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:dio/dio.dart';

class UserFavorites extends StatefulWidget {
  @override
  _UserFavoritesState createState() => _UserFavoritesState();
}

class _UserFavoritesState extends State<UserFavorites> {
  bool _isLoading = true;
  String _message = "";
  bool _error = false;
  late Dio dio;
  List<Post> _listOfPosts = List<Post>.empty(growable: true);

  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;

  /// ==============================
  /// Get users post favorites
  /// ==============================
  getFavoritePosts() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    Response response =
        await dio.get('/users/$userId/favorites/posts').catchError((onError) {
      print("TV - Error getting posts ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print(data[i]['title']);
            Post temPost = Post.fromJsonNoUser(data[i]);
            if (temPost.groupId == null) {
              _listOfPosts.add(temPost);
            }
            //_listOfPosts.add(Post.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getFavoritePosts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Tus Favoritos",
                          style: Theme.of(context).textTheme.headline3,
                        ),
                        _listOfPosts.isNotEmpty
                            ? _buildPostsFromGet(_listOfPosts)
                            : Text('No tienes favoritos ¡Agrega algunos!'),
                      ],
                    ),
                  ),
                ),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  Widget _buildPostsFromGet(List<Post> posts) {
    return Column(
      children: posts.map((e) {
        return buildCardWithImage(context, e);
      }).toList(),
    );
  }

  Widget buildCardWithImage(BuildContext context, Post post) {
    return Column(
      children: [
        SizedBox(
          height: 10.0,
        ),
        Align(
          child: GestureDetector(
            onTap: () {
              print("Hello new gesture detector");
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EventPage(
                            id: post.id,
                            tile: post.title,
                            image: post.image,
                            type: 1,
                          )));
            },
            child: Container(
              height: 275,
              width: MediaQuery.of(context).size.width / 1.1,
              decoration: BoxDecoration(
                image: post.image != null
                    ? DecorationImage(
                        //image: ExactAssetImage(image),
                        image: NetworkImage(post.image),
                        fit: BoxFit.cover,
                        alignment: Alignment.topCenter)
                    : null,
                borderRadius: BorderRadius.circular(15.0),
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.25),
                    spreadRadius: 3,
                    blurRadius: 3,
                    offset: Offset(2, 4), // changes position of shadow
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Positioned(
                    bottom: 0,
                    width: MediaQuery.of(context).size.width / 1.1,
                    child: Container(
                      //color: Colors.white,
                      padding: EdgeInsets.all(10.0),
                      height: 120,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.5),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Flexible(
                                child: RichText(
                                  overflow: TextOverflow.ellipsis,
                                  strutStyle: StrutStyle(fontSize: 12.0),
                                  text: TextSpan(
                                      style:
                                          Theme.of(context).textTheme.subtitle1,
                                      text: post.title),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Text((post.shortDescription!.length > 125
                              ? post.shortDescription!.substring(0, 123) + "..."
                              : post.shortDescription) as String),
                          SizedBox(
                            height: 10.0,
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                      right: 2.5,
                      bottom: 100,
                      //top: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          /*border: Border.all(
                                color: Colors.black,
                                width: 0.1
                              ),*/
                          borderRadius: BorderRadius.circular(50.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.25),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset:
                                  Offset(1, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: IconButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EventPage(
                                          id: post.id,
                                          tile: post.title,
                                          image: post.image,
                                          type: 1,
                                        )));
                          },
                          icon: Icon(Icons.send),
                          //icon: Image.asset('assets/iconos/enviar.png'),
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
      ],
    );
  }
}
