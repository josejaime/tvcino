import 'package:flutter/material.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/pages/fees/index.dart';
import 'package:tvcino/pages/neighborhoods/neighborhood_users.dart';
import 'package:tvcino/pages/tasks/important_tasks.dart';
import 'package:tvcino/pages/tasks/index_tasks.dart';

class AdminiNeighborhoodMain extends StatefulWidget {
  @override
  _AdminiNeighborhoodMainState createState() => _AdminiNeighborhoodMainState();
}

class _AdminiNeighborhoodMainState extends State<AdminiNeighborhoodMain> {
  @override
  Widget build(BuildContext context) {
    double widht = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(
        title: Text("Administración", style: TextStyle(color: Colors.white)),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: widht,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Material(
                  elevation: 2.0,
                  child: Container(
                    width: widht,
                    padding: EdgeInsets.all(20.0),
                    decoration: BoxDecoration(
                      //color: Theme.of(context).primaryColor,
                      color: Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "¡Hola Vecino!",
                          style: TextStyle(
                              //color: Colors.white,
                              fontSize: 18.0,
                              fontFamily: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .fontFamily),
                        ),
                        /* Text(
                          "Fraccionamiento del bosque",
                        ), */
                        Divider(
                          color: Colors.black38,
                        ),
                        Text(
                          "Esta es tu área de administración",
                          style: TextStyle(
                            //color: Colors.white,
                            color: Theme.of(context).primaryColor,
                            fontSize: 17.0,
                            fontFamily: Theme.of(context)
                                .textTheme
                                .headline6!
                                .fontFamily,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: widht,
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                      //color: Theme.of(context).primaryColor,
                      ),
                  child: Text("Administra tu vecindario"),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => IndexTasksPage()));
                      },
                      borderRadius: BorderRadius.circular(15.0),
                      child: Container(
                        width: widht / 2.25,
                        margin: EdgeInsets.all(5.0),
                        height: 100.0,
                        clipBehavior: Clip.hardEdge,
                        decoration: BoxDecoration(
                          //color: Colors.white,
                          color: Theme.of(context).textTheme.headline6!.color,
                          borderRadius: BorderRadius.circular(15.0),
                          border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 1,
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 1.5,
                              blurRadius: 1.5,
                              offset:
                                  Offset(2, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Tareas",
                              //style: Theme.of(context).textTheme.subtitle1,
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .fontFamily,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .fontSize,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        /* Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SoonPage(),
                          ),
                        ); */
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FeesIndexPage()));
                      },
                      borderRadius: BorderRadius.circular(15.0),
                      child: Container(
                        width: widht / 2.25,
                        margin: EdgeInsets.all(5.0),
                        height: 100.0,
                        clipBehavior: Clip.hardEdge,
                        decoration: BoxDecoration(
                          //color: Colors.white,
                          color: Theme.of(context).textTheme.headline6!.color,
                          borderRadius: BorderRadius.circular(15.0),
                          border: Border.all(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 1,
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 1.5,
                              blurRadius: 1.5,
                              offset:
                                  Offset(2, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Gestión",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .fontFamily,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .fontSize,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                buildAdminButton(
                    'Vecinos', 'assets/iconos/usuario.png', Icons.arrow_right,
                    () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NeighborhoodUsers()));
                }),
                buildAdminButton('Tareas Urgentes',
                    'assets/iconos/notificacion.png', Icons.arrow_right, () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ImportantTasksPage()));
                }),
                /* buildAdminButton('Consultar roles', 'assets/iconos/manos.png',
                    Icons.arrow_right, () {}), */
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: TvcinoBottomAppBar(
        hideMiddle: true,
        disabledButton: 4,
      ),
    );
  }

  Widget buildAdminButton(String title, String image, IconData icon, function) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: Colors.white,
        ),
        /* color: Theme.of(context).buttonTheme.colorScheme.surface,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17.5), */
        onPressed: function,
        child: Row(
          children: [
            Image.asset(
              image,
              width: 35.0,
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Text(
                title,
                style: Theme.of(context).textTheme.headline5,
              ),
            )),
            Icon(icon),
          ],
        ),
      ),
    );
  }
}
