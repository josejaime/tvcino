import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/user/update_profile.dart';
import 'package:tvcino/utils/custom_functions.dart';

class UserProfilePage extends StatefulWidget {
  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late Dio dio;
  late SharedPreferences preferences;
  InterUser? _interUser;

  /// ==============================
  /// DIO
  /// ==============================
  String _message = "";
  bool _isLoading = false;
  bool _error = false;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    _getPreferences();
    getUser();
    super.initState();
  }

  _getPreferences() async {
    preferences = await SharedPreferences.getInstance();
    print("TV Retrived data");
    print("TV User Name = ${preferences.getString('name')}");
    print("TV User userame = ${preferences.getString('username')}");
    print("TV User email  = ${preferences.getString('email')}");
    print("TV User UUID  = ${preferences.getString('uuid')}");
    print("TV User ID  = ${preferences.getString('userId')}");
    print("TV User birthdate  = ${preferences.getString('birthdate')}");
    print("TV User country  = ${preferences.getString('country')}");
    print("TV User state  = ${preferences.getString('state')}");
    print("TV User city  = ${preferences.getString('city')}");
    print("TV User address  = ${preferences.getString('address')}");

    ///CONTROL
    print("TV - CONTROL");
    print("TV User Created  = ${preferences.getBool('userCreated')}");
    print("TV Neighborhood ID = ${preferences.getString('neighborhoodId')}");
    print("TV Neighborhood set? = ${preferences.getBool('neighborhoodSet')}");
  }

  /// ==============================
  /// Get the user from the server
  /// ==============================
  getUser() async {
    preferences = await SharedPreferences.getInstance();
    dio = CustomFunctions.getDio();

    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    String userId = preferences.getString('userId') as String;

    Response response = await dio.get('/users/$userId').catchError((onError) {
      print("ERRORORORORORO ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        print("Hubo un error en el servidor regresa e intenta nuevamente");
        setState(() {
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _isLoading = false;
          _error = true;
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response);
        print("DIO Trying to convert data");

        setState(() {
          _interUser = InterUser.fromJson(response.data);
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _isLoading = false;
          _error = false;
        });
        print("Converted USER:");
        print(_interUser);
      }
    } else {
      print("Hubo un error en el servidor, intenta más tarde");
      setState(() {
        _message = "Hubo un error en el servidor regresa e intenta nuevamente";
        _isLoading = false;
        _error = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
        title: _user.displayName != null
            ? Text(_user.displayName as String,
                style: TextStyle(color: Colors.white))
            : Text("Jon Doe", style: TextStyle(color: Colors.white)),
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : _interUser != null
                  ? SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: (MediaQuery.of(context).size.height * 0.20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        Container(
                                            height: 120,
                                            width: 120,
                                            child: _user.photoURL != null
                                                ? CircleAvatar(
                                                    radius: 50.0,
                                                    backgroundColor:
                                                        Colors.white,
                                                    backgroundImage:
                                                        NetworkImage(
                                                            _user.photoURL
                                                                as String))
                                                : CircleAvatar(
                                                    radius: 50.0,
                                                    backgroundColor:
                                                        Colors.white,
                                                    backgroundImage:
                                                        ExactAssetImage(
                                                            'assets/user_2.png'),
                                                  )),
                                        /* RawMaterialButton(
                                          onPressed: () {},
                                          elevation: 2.0,
                                          fillColor: Colors.white,
                                          child: Icon(
                                            Icons.edit,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          padding: EdgeInsets.all(5.0),
                                          shape: CircleBorder(),
                                        ) */
                                      ],
                                    ),
                                    SizedBox(width: 15.0),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        /* Text(
                                          "Cuenta Premium",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.w600),
                                          textAlign: TextAlign.left,
                                        ), */
                                        Text(
                                          (_user.displayName != null
                                              ? _user.displayName
                                              : "Jon Doe") as String,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w600),
                                          textAlign: TextAlign.left,
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            //height: MediaQuery.of(context).size.height * 0.64,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15.0)),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 15.0,
                                ),
                                Text(
                                  "Tus datos",
                                  style: Theme.of(context).textTheme.headline2,
                                  textAlign: TextAlign.left,
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                buildProfileLine(
                                    context,
                                    'Nombre',
                                    (_user.displayName != null
                                        ? _user.displayName
                                        : "Jon Doe") as String),
                                buildProfileLine(
                                    context,
                                    'Correo',
                                    (_user.email != null
                                        ? _user.email
                                        : "Jon Doe") as String),
                                // !_user.emailVerified
                                //     ? Align(
                                //         alignment: Alignment.centerLeft,
                                //         child: Column(
                                //           crossAxisAlignment:
                                //               CrossAxisAlignment.start,
                                //           children: [
                                //             Padding(
                                //               padding: const EdgeInsets.only(
                                //                   left: 15.0),
                                //               child: Text(
                                //                 "* No has verificado tu cuenta de correo",
                                //                 style:
                                //                     TextStyle(fontSize: 12.0),
                                //               ),
                                //             ),
                                //             Padding(
                                //               padding: const EdgeInsets.only(
                                //                   left: 15.0),
                                //               child: TextButton(
                                //                 onPressed: () {
                                //                   //_user.sendEmailVerification();
                                //                   showDialog(
                                //                     context: context,
                                //                     builder:
                                //                         (BuildContext context) {
                                //                       // return object of type Dialog
                                //                       return AlertDialog(
                                //                         title: Text(
                                //                             "¡Mensaje Enviado!"),
                                //                         //content: Text("¿Te quieres unir?"),
                                //                         actions: <Widget>[
                                //                           TextButton(
                                //                             child:
                                //                                 Text("Aceptar"),
                                //                             onPressed:
                                //                                 () async {
                                //                               Navigator.of(
                                //                                       context)
                                //                                   .pop();
                                //                             },
                                //                           ),
                                //                         ],
                                //                       );
                                //                     },
                                //                   );
                                //                 },
                                //                 child: Text("Enviar correo"),
                                //               ),
                                //             ),
                                //           ],
                                //         ),
                                //       )
                                //     : Row(),
                                /* buildProfileLine(
                                    context, 'Número', '336598745'), */
                                buildProfileLine(context, 'Cuenta', 'Premium'),
                                buildProfileLine(
                                    context, 'País', _interUser!.country ?? ''),
                                buildProfileLine(
                                    context, 'Estado', _interUser!.state ?? ''),
                                buildProfileLine(context, 'Localidad',
                                    _interUser!.city ?? ''),
                                buildProfileLine(context, 'Dirección',
                                    _interUser!.address ?? ''),
                                /* buildProfileLine(
                                    context, 'Dirección', _interUser.address), */
                                SizedBox(
                                  height: 10.0,
                                ),
                                TextButton(
                                  /* padding: EdgeInsets.symmetric(
                                      horizontal: 35, vertical: 12),
                                  color: Theme.of(context)
                                      .buttonTheme
                                      .colorScheme
                                      .secondaryVariant, */
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                UpdateProfilePage()));
                                  },
                                  child: Text(
                                    "Editar",
                                    style: TextStyle(fontSize: 20.0),
                                  ),
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Center(
                      child: Text(
                        "Cargando información...",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
    );
  }

  Padding buildProfileLine(BuildContext context, String first, String content) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(first.toUpperCase(),
                style: Theme.of(context).primaryTextTheme.headline6),
          ),
          Expanded(
            flex: 2,
            child: Text(content, textAlign: TextAlign.left),
          ),
        ],
      ),
    );
  }
}
