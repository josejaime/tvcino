import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/task.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class AssignedUserTasks extends StatefulWidget {
  @override
  _AssignedUserTasksState createState() => _AssignedUserTasksState();
}

class _AssignedUserTasksState extends State<AssignedUserTasks> {
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  late Dio dio;
  List<Task> _listOfTasks = List<Task>.empty(growable: true);
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  @override
  initState() {
    _user = instance.currentUser as auth.User;
    getUserAssignedTasks();
    super.initState();
  }

  /// ==============================
  /// Get assigned tasks
  /// from a particular user
  /// ==============================
  getUserAssignedTasks() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    Response response =
        await dio.get('/tasks?user_id=$userId').catchError((onError) {
      print("ERROR DOING THE CALL");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            _listOfTasks.add(Task.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(),
      //backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SingleChildScrollView(
              child: Column(
              children: [
                Align(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.5),
                    child: Text(
                      "Tareas que te han asignado",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline3,
                    ),
                  ),
                ),
                _error
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: 28.0,
                            color: Colors.black,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(_message, style: TextStyle(color: Colors.black)),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: _listOfTasks.isNotEmpty
                              ? _listOfTasks.map((e) {
                                  return buildTask(e);
                                }).toList()
                              : [
                                  Text(
                                    "No hay nada para mostrar aún",
                                    style: TextStyle(color: Colors.black),
                                  )
                                ],
                        ),
                      ),
              ],
            )),
    );
  }

  GestureDetector buildTask(Task task) {
    Color color = Colors.green;

    if (task.priority == 2) {
      color = Colors.orange;
    } else if (task.priority >= 3) {
      color = Colors.red;
    }

    return GestureDetector(
      onTap: () {
        print("Gesture detector");
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 2.0,
        child: Container(
          padding: EdgeInsets.all(15.0),
          width: MediaQuery.of(context).size.width * 0.9,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        "Tarea",
                        style: Theme.of(context).primaryTextTheme.headline4,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8.0,
              ),
              Text(task.content as String),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Asignada por:",
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      Text(task.createdBy!.name),
                    ],
                  ),
                  CircleAvatar(
                    radius: 20.0,
                    backgroundColor: color,
                    child: Text(
                      '',
                      style: TextStyle(
                          fontWeight: FontWeight.w400, fontSize: 15.0),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
