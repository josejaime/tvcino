import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/classes/neighborhood.dart';
import 'package:tvcino/pages/login/login.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:geocoding/geocoding.dart' as geo;
import 'package:http/http.dart' as http;

class ChangeNeighborhood extends StatefulWidget {
  @override
  _ChangeNeighborhoodState createState() => _ChangeNeighborhoodState();
}

class _ChangeNeighborhoodState extends State<ChangeNeighborhood> {
  final GlobalKey<FormState> _formRegisterZipCode =
      GlobalKey<FormState>(debugLabel: 'formRegisterZipCode');
  final _zipController = TextEditingController();
  late auth.FirebaseAuth _instance;
  late auth.User _user;
  String _selectedValue = "México";
  String _calculatedZipCode = "";
  List data = [];
  List<Neighbordhood> _listOfNeighborhoods =
      List<Neighbordhood>.empty(growable: true);
  String _message = "";
  bool _isLoading = false;
  bool _error = false;
  late Dio dio;
  bool _checkDays = false;

  @override
  void initState() {
    _instance = auth.FirebaseAuth.instance;
    _zipController.text = _calculatedZipCode;
    _user = _instance.currentUser as auth.User;
    _getCurrentPosition();
    //_getNeighborhoods();
    _verifyDays();
    super.initState();
  }

  _verifyDays() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    DateTime time = DateTime.now();
    if (preferences.containsKey('lastChange')) {
      String lastTime = preferences.getString('lastChange') as String;
      DateTime lastDateTime = DateTime.parse(lastTime);
      debugPrint("The DIff IS THE NEXT " +
          time.difference(lastDateTime).inMinutes.toString());
      int diffInDays = time.difference(lastDateTime).inMinutes;
      if (diffInDays > 15) {
        _checkDays = true;
      } else {
        _checkDays = false;
        debugPrint("No hay suficiente tiempo desde el último cambio");
      }
    }
  }

  _getCurrentPosition() async {
    Position position = await _determinePosition();
    print('Lat = ${position.latitude}');
    print('Long = ${position.longitude}');
    List<geo.Placemark> placemarks = await geo.placemarkFromCoordinates(
        position.latitude, position.longitude,
        localeIdentifier: 'es_MX');
    print('GEOLOCATION');
    print(placemarks);
    if (placemarks.isNotEmpty) {
      setState(() {
        if (placemarks.first.isoCountryCode == 'MX') {
          _selectedValue = 'México';
        }
        /* else if (placemarks.first.isoCountryCode == 'US') {
          _selectedValue = 'Estados Unidos';
        } */
        _calculatedZipCode = placemarks.first.postalCode as String;
        _zipController.text = _calculatedZipCode;
      });
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.setString(
          'country', placemarks.first.country as String);
      await preferences.setString('state', placemarks.first.locality as String);
      await preferences.setString(
          'city', placemarks.first.administrativeArea as String);
      await preferences.setString('address', 'N/A');
      _getNeighborhoods(_calculatedZipCode);
    }
  }

  _getNeighborhoods(String zipcode) async {
    setState(() {
      _isLoading = true;
      _error = false;
      _listOfNeighborhoods.clear();
    });
    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    try {
      Response response = await dio.get(
          '/neighborhoods?country=$_selectedValue&zipcode=$zipcode&start=0&limit=100');
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        //_data = jsonDecode(response.data.toString());
        data = response.data['items'];

        print("CONVERTED DATA ¿?");
        print("DATA is: ");
        //print(data[0]);

        print("MAPPING is: ");
        for (int i = 0; i < data.length; i++) {
          print(data[i]['name']);
          //_listOfUsers.add(InterUser.fromJson(data[i]));
          _listOfNeighborhoods.add(Neighbordhood.fromJson(data[i]));
        }
        print("TV THE LIST OF NEIGHBORHOODS IS :");
        print(_listOfNeighborhoods);
        //print("THE FIRST ONE IS: ${_listOfUsers[0].name}");

        setState(() {
          _isLoading = false;
          _error = false;
          _message = "";
        });
      }
    } on DioError catch (error) {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = error.toString();
      });
    }
  }

  /// =============================================
  /// Function
  /// update User in the TVCINO DB
  /// =============================================
  Future<bool> _updateUser(
      String neighborhoodId, Neighbordhood neighbordhood) async {
    print("TV - UPDATE USER IN THE DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    String url = CustomFunctions.url + "/users/$userId";

    try {
      Map<String, String> datos = Map<String, String>();

      print("TV User Name = ${preferences.getString('name')}");

      String name = preferences.getString('name') as String;

      datos["data"] =
          '{"name": "$name", "role_id": "72d20d3f-d620-4b4f-932c-0b0acfe75272", "neighborhood_id": "$neighborhoodId"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      String token = await _user.getIdToken();
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;
      //11ebc005-78a2-5bb4-8767-fa163e7a966d
      //11ebc005-78a2-5bb4-8767-fa163e7a966d
      //11ebc001-2309-06f2-8767-fa163e7a966d
      await request.send().then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          String value = await response.stream.bytesToString();
          InterUser updatedUser = InterUser.fromJson(jsonDecode(value));
          print("TV - UPDATED USER =====");
          print(updatedUser.neighborhoodId);

          DateTime time = DateTime.now();
          debugPrint("The last change of neighborhood was " + time.toString());

          preferences.setString("lastChange", time.toString());

          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          await preferences.setString('neighborhoodId', neighborhoodId);
          await preferences.setString(
              'neighbordhoodCountry', neighbordhood.country);
          await preferences.setString(
              'neighbordhoodState', neighbordhood.state);
          await preferences.setString('neighbordhoodCity', neighbordhood.city);

          print("TV - USER UPDATED ALL OK");

          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Stack(
                children: [
                  Container(
                    width: double.infinity,
                    height: 70,
                    color: Colors.white,
                    child: Text(
                      "tuvcino",
                      style: Theme.of(context).textTheme.headline1,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Positioned(
                    left: 0,
                    top: 12,
                    child: IconButton(
                      color: Theme.of(context).accentColor,
                      onPressed: () {
                        return Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: (MediaQuery.of(context).size.height * 0.20) -
                    (MediaQuery.of(context).padding.top + 70),
              ),
              Container(
                width: double.infinity,
                //height: MediaQuery.of(context).size.height * 0.70,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0),
                    topLeft: Radius.circular(15.0),
                    bottomLeft: Radius.circular(15.0),
                  ),
                ),
                child: Form(
                  key: _formRegisterZipCode,
                  child: Align(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.85,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 30.0,
                          ),
                          Text(
                            "Primero necesitamos tu ZIPCODE",
                            style: Theme.of(context).textTheme.headline2,
                            textAlign: TextAlign.center,
                          ),
                          DropdownButton<String>(
                            value: _selectedValue,
                            isExpanded: true,
                            icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            onChanged: (String? newValue) {
                              print('TV - Selected new value $newValue');
                              setState(() {
                                _selectedValue = newValue as String;
                              });
                            },
                            items: <String>[
                              //'Estados Unidos',
                              'México',
                              //'Canada'
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                          TextFormField(
                            controller: _zipController,
                            keyboardType: TextInputType.streetAddress,
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: "ZIPCODE",
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "* Debes ingresar una ciudad";
                              }
                              if (value.length < 3) {
                                return "* Intenta con un valor más largo";
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          TextButton(
                            child: Text(
                              "Busca un vecindario",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            onPressed: () async {
                              /******************************************/
                              // Search ZIPCODE Neighborhoods
                              /******************************************/
                              _getNeighborhoods(_zipController.text);
                            },
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            children: [
                              Text(
                                'Selecciona tu vecindario',
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                          _listOfNeighborhoods.isEmpty
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'No hay vecindarios',
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline3,
                                  ),
                                )
                              : Offstage(),
                          _isLoading
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        CircularProgressIndicator(),
                                        SizedBox(
                                          height: 15.0,
                                        ),
                                        Text("Cargando..."),
                                      ],
                                    ),
                                  ],
                                )
                              : Offstage(),
                          //_error ? Text(_message) : Row(),
                          _listOfNeighborhoods.isNotEmpty
                              ? SingleChildScrollView(
                                  child:
                                      buildNeighborhoods(_listOfNeighborhoods))
                              : Row(),
                          SizedBox(
                            height: 15.0,
                          ),
                          GestureDetector(
                            onTap: () async {
                              await _instance.signOut();
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginPage()));
                            },
                            child: Text(
                              "Cerrar sesión",
                              style: TextStyle(
                                fontSize: 16.0,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildNeighborhoods(List<Neighbordhood> neighborhoods) {
    int i = -1;
    return Column(
      children: neighborhoods.map((e) {
        //return Text("${e.name}");
        i++;
        return buildNeighborhoodContainer(context, e, i);
      }).toList(),
    );
  }

  Widget buildNeighborhoodContainer(
      BuildContext context, Neighbordhood neighborhood, int index) {
    return Column(
      children: [
        GestureDetector(
          onTap: () async {
            DateTime time = DateTime.now();
            SharedPreferences preferences =
                await SharedPreferences.getInstance();
            if (preferences.containsKey('lastChange')) {
              String lastTime = preferences.getString('lastChange') as String;
              DateTime lastDateTime = DateTime.parse(lastTime);
              debugPrint("The DIff IS THE NEXT " +
                  time.difference(lastDateTime).inMinutes.toString());
              int diffInDays = time.difference(lastDateTime).inMinutes;
              if (diffInDays > 15) {
                _showEnterConfirmation(context, neighborhood.id, neighborhood);
              } else {
                showBottomMessage(Icons.dangerous,
                    "¡Advertencia! \n Solo puedes cambiar de vecindario cada 15 días, van $diffInDays días");
                debugPrint("No hay suficiente tiempo desde el último cambio");
              }
            } else {
              _showEnterConfirmation(context, neighborhood.id, neighborhood);
            }
          },
          child: Material(
            borderRadius: BorderRadius.circular(5),
            elevation: 2.0,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              padding: EdgeInsets.all(7.5),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          child: Container(
                            width: double.infinity,
                            child: Text(
                              neighborhood.name,
                              style: TextStyle(
                                //color: Colors.white,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .fontSize,
                                fontFamily: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .fontFamily,
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          endIndent: 20,
                        ),
                        Text(
                          "${neighborhood.country} \n${neighborhood.state}",
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize:
                                Theme.of(context).textTheme.headline6!.fontSize,
                            fontFamily: Theme.of(context)
                                .textTheme
                                .headline6!
                                .fontFamily,
                          ),
                          //style: Theme.of(context).textTheme.headline5.fontFamily,
                        ),
                      ],
                    ),
                  ),
                  Icon(Icons.chevron_right),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  void _showEnterConfirmation(
      BuildContext context, String id, Neighbordhood neighbordhood) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Confirmar"),
          content: Text("¿Quieres unirte a este vecindario?"),
          actions: [
            TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.grey,
              ),
              child: Text('Cancelar'),
              //color: Theme.of(context).buttonTheme.colorScheme.secondary,
              onPressed: () {
                //Just dismiss the alert
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Sí, Continuar'),
              onPressed: () async {
                //bool result = await _addUserToNeighborhood(id);
                DateTime time = DateTime.now();
                SharedPreferences preferences =
                    await SharedPreferences.getInstance();
                if (preferences.containsKey('lastChange')) {
                  String lastTime =
                      preferences.getString('lastChange') as String;
                  DateTime lastDateTime = DateTime.parse(lastTime);
                  debugPrint("The DIff IS THE NEXT " +
                      time.difference(lastDateTime).inMinutes.toString());
                  int diffInDays = time.difference(lastDateTime).inMinutes;
                  if (diffInDays > 15) {
                    bool result = await _updateUser(id, neighbordhood);
                  } else {
                    debugPrint(
                        "No hay suficiente tiempo desde el último cambio");
                  }
                } else {
                  bool result = await _updateUser(id, neighbordhood);
                }

                /* if (result) {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => HomePage()));
                } else {
                  showBottomMessage(Icons.dangerous,
                      "Hubo un error al registrar tu usuario en el vecindario ¡intenta nuevamente!");
                } */
              },
            ),
          ],
        );
      },
    );
  }

  /// =============================================
  /// Function
  /// Add user to a Neighborhood in the TVCINO DB
  /// =============================================
  _addUserToNeighborhood(String neighborhoodId) async {
    print("TV - ADD USER TO NEIGHBORHOOD ID $neighborhoodId");

    bool added = true;

    if (added == false) {
      print("TV - ADDED FAILED");
      return false;
    } else {
      print("TV - ADDED WENT TRUE");

      SharedPreferences preferences = await SharedPreferences.getInstance();
      String userId = preferences.getString('userId') as String;
      setState(() {
        _error = false;
        _isLoading = true;
        _message = "";
      });

      try {
        var url = CustomFunctions.url +
            '/neighborhoods/$neighborhoodId/users/$userId';

        print("TV - URL - $url");
        print("TV - Neighborhood ID - $neighborhoodId");
        print("TV - User ID - $userId");
        String token = await _user.getIdToken();

        var response = await http
            .post(Uri.parse(url), headers: {'Authorization': '$token'});

        print("Response headers" + response.headers.toString());
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');

        if (response.statusCode != 200) {
          print("TV HTTP ERROR - Status Code ${response.statusCode}");
          setState(() {
            _message =
                "Hubo un error en el servidor regresa e intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        } else {
          /******************************************/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          /******************************************/
          await preferences.setString('neighborhoodId', neighborhoodId);
          await preferences.setBool('neighborhoodSet', true);
          /* _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Vecindario creado, vamos a la página principal',
            ),
          ),
        );
        */
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          /* Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage())); */
          return true;
        }
      } on SocketException {
        print('TV EXCEPTION - No Internet connection');
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - No Internet connection";
        });
        return false;
      } on HttpException {
        print("TV EXCEPTION - Couldn't find the post");
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - Couldn't find the post";
        });
        return false;
      } on FormatException {
        print("TV EXCEPTION - Bad response format");
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - Bad response format";
        });
        return false;
      }
    }
  }

  showBottomMessage(IconData icon, String message) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(25.0),
          color: Theme.of(context).accentColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
