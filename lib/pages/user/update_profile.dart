import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/user/profile.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:http/http.dart' as http;

class UpdateProfilePage extends StatefulWidget {
  @override
  _UpdateProfilePageState createState() => _UpdateProfilePageState();
}

class _UpdateProfilePageState extends State<UpdateProfilePage> {
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late Dio dio;
  late SharedPreferences preferences;
  InterUser? _interUser;
  late String userId;

  /// ==============================
  /// DIO
  /// ==============================
  String _message = "";
  bool _isLoading = false;
  bool _error = false;

  /// ==============================
  /// Images
  /// ==============================
  File? _image;
  final picker = ImagePicker();
  bool updateImage = false;
  TextEditingController _imageTextController = TextEditingController();

  /// ==============================
  /// More Crontrollers
  /// ==============================
  String _selectedState = "Aguascalientes";
  String _selectedValue = "México";
  List<String> states = [];
  TextEditingController _nameController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _cityController = TextEditingController();

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    _getPreferences();
    getUser();

    ///Controllers
    _nameController.text = _user.displayName as String;
    states = CustomFunctions.mexicanStates() as List<String>;

    super.initState();
  }

  _getPreferences() async {
    preferences = await SharedPreferences.getInstance();
    print("TV Retrived data");
    print("TV User Name = ${preferences.getString('name')}");
    print("TV User userame = ${preferences.getString('username')}");
    print("TV User email  = ${preferences.getString('email')}");
    print("TV User UUID  = ${preferences.getString('uuid')}");
    print("TV User ID  = ${preferences.getString('userId')}");
    print("TV User birthdate  = ${preferences.getString('birthdate')}");
    print("TV User country  = ${preferences.getString('country')}");
    print("TV User state  = ${preferences.getString('state')}");
    print("TV User city  = ${preferences.getString('city')}");
    print("TV User address  = ${preferences.getString('address')}");

    ///CONTROL
    print("TV - CONTROL");
    print("TV User Created  = ${preferences.getBool('userCreated')}");
    print("TV Neighborhood ID = ${preferences.getString('neighborhoodId')}");
    print("TV Neighborhood set? = ${preferences.getBool('neighborhoodSet')}");
  }

  /// ==============================
  /// Get the user from the server
  /// ==============================
  getUser() async {
    preferences = await SharedPreferences.getInstance();
    dio = CustomFunctions.getDio();

    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    userId = preferences.getString('userId') as String;

    Response response = await dio.get('/users/$userId').catchError((onError) {
      print("ERRORORORORORO ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        print("Hubo un error en el servidor regresa e intenta nuevamente");
        setState(() {
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _isLoading = false;
          _error = true;
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response);
        print("DIO Trying to convert data");

        setState(() {
          _interUser = InterUser.fromJson(response.data);
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _isLoading = false;
          _error = false;

          _addressController.text = _interUser!.address!;
          _cityController.text = _interUser!.city!;
          _selectedState = _interUser!.state!;
        });
        print("Converted USER:");
        print(_interUser);
      }
    } else {
      print("Hubo un error en el servidor, intenta más tarde");
      setState(() {
        _message = "Hubo un error en el servidor regresa e intenta nuevamente";
        _isLoading = false;
        _error = true;
      });
    }
  }

  /// =============================================
  /// Function
  /// update User in the TVCINO DB
  /// =============================================
  Future<bool> _updateUser() async {
    print("TV - UPDATE USER IN THE DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    userId = preferences.getString('userId') as String;
    String _fileName = _imageTextController.text.split('/').last;

    String url = CustomFunctions.url + "/users/$userId";

    try {
      Map<String, String> datos = Map<String, String>();

      datos["data"] =
          '{"name": "${_nameController.text}", "role_id": "${_interUser!.roleId}", "country": "$_selectedValue", "state": "$_selectedState", "city": "${_cityController.text}", "address": "${_addressController.text}"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      String token = await _user.getIdToken();
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      if (_image != null) {
        //============================================/
        /// CHECK IF THE IMAGE IS AVAILABLE
        //============================================/
        request.files.add(
          http.MultipartFile.fromBytes(
              'image', File(_image!.path).readAsBytesSync(),
              filename: _fileName),
        );
      }
      await request.send().then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          String value = await response.stream.bytesToString();
          InterUser updatedUser = InterUser.fromJson(jsonDecode(value));
          print("TV - UPDATED USER =====");
          print(updatedUser.picture);
          print(updatedUser.name);
          print("TV - UPDATED USER =====");
          await _user.updateDisplayName(_nameController.text);
          await _user.updatePhotoURL(updatedUser.picture);
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - USER UPDATED ALL OK");

          //Navigator.popUntil(context, ModalRoute.withName('/profile'));
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          /* Navigator.pop(context);
          Navigator.pop(context); */
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => UserProfilePage(),
            ),
          );
          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print("TV: Image selected");
        print(_image);
        //_imageTextController.text = _image.path;
        _imageTextController.text = _image!.path.split('/').last;
        updateImage = true;
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
        title: _user.displayName != null
            ? Text(_user.displayName ?? '',
                style: TextStyle(color: Colors.white))
            : Text("Jon Doe", style: TextStyle(color: Colors.white)),
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : _interUser != null
                  ? SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: (MediaQuery.of(context).size.height * 0.20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        updateImage
                                            ? Container(
                                                /* height: 120,
                                                width: 120, */
                                                child: ClipOval(
                                                  child: Image.file(
                                                    _image!,
                                                    width: 120,
                                                    height: 120,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              )
                                            : Container(
                                                height: 120,
                                                width: 120,
                                                child: _user.photoURL != null
                                                    ? CircleAvatar(
                                                        radius: 50.0,
                                                        backgroundColor:
                                                            Colors.white,
                                                        backgroundImage:
                                                            NetworkImage(
                                                                _user.photoURL
                                                                    as String))
                                                    : CircleAvatar(
                                                        radius: 50.0,
                                                        backgroundColor:
                                                            Colors.white,
                                                        backgroundImage:
                                                            ExactAssetImage(
                                                                'assets/user_2.png'),
                                                      ),
                                              ),
                                      ],
                                    ),
                                    SizedBox(width: 15.0),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          (_user.displayName != null
                                              ? _user.displayName
                                              : "Jon Doe") as String,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w600),
                                          textAlign: TextAlign.left,
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            width: double.infinity,
                            //height: MediaQuery.of(context).size.height * 0.64,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15.0)),
                            child: Column(
                              children: [
                                /* _image != null
                                    ? Column(
                                        children: [
                                          SizedBox(
                                            height: 20.0,
                                          ),
                                          Container(
                                            height: 120.0,
                                            width: 120.0,
                                            decoration: BoxDecoration(
                                              color: const Color(0xff7c94b6),
                                              borderRadius: BorderRadius.all(
                                                  const Radius.circular(60.0)),
                                              border: Border.all(
                                                  color:
                                                      const Color(0xFF28324E)),
                                            ),
                                            child: Image.file(
                                              _image,
                                              fit: BoxFit.cover,
                                            ),
                                            clipBehavior: Clip.hardEdge,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 5.0),
                                            child: Text(
                                              "* Puede cambiar la forma de verse al guardar",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w100,
                                                  fontSize: 10.0,
                                                  fontStyle: FontStyle.italic),
                                            ),
                                          ),
                                        ],
                                      )
                                    : Offstage(), */
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  child: Form(
                                    child: Column(children: [
                                      TextFormField(
                                        readOnly: true,
                                        onTap: () {
                                          getImage();
                                        },
                                        decoration: InputDecoration(
                                          labelText: "Imagen de perfil",
                                          hintText: "Selecciona aquí...",
                                          suffixIcon: Icon(Icons.file_upload),
                                        ),
                                        controller: _imageTextController,
                                      ),
                                      Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8.0),
                                            child: Text(
                                              'Nombre',
                                              textAlign: TextAlign.left,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline6,
                                            ),
                                          ),
                                        ],
                                      ),
                                      TextFormField(
                                        controller: _nameController,
                                        decoration:
                                            InputDecoration(labelText: null),
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return "* Debes ingresar tu nombre";
                                          }
                                          if (value.length < 4) {
                                            return "* Intenta con un nombre más largo";
                                          }
                                          return null;
                                        },
                                      ),
                                      Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8.0),
                                            child: Text(
                                              'País',
                                              textAlign: TextAlign.left,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline6,
                                            ),
                                          ),
                                        ],
                                      ),
                                      DropdownButton<String>(
                                        value: _selectedValue,
                                        isExpanded: true,
                                        icon: Icon(Icons.arrow_downward),
                                        iconSize: 24,
                                        elevation: 16,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            _selectedValue = newValue as String;
                                            if (newValue == "Canada") {
                                              states =
                                                  CustomFunctions.canadaStates()
                                                      as List<String>;
                                              _selectedState = states[0];
                                            } else if (newValue ==
                                                "Estados Unidos") {
                                              states =
                                                  CustomFunctions.unitedStates()
                                                      as List<String>;
                                              _selectedState = states[0];
                                            } else {
                                              states = CustomFunctions
                                                      .mexicanStates()
                                                  as List<String>;
                                              _selectedState = states[0];
                                            }
                                          });
                                          print(
                                              'TV - Selected new value $_selectedValue');
                                        },
                                        items: <String>[
                                          'Estados Unidos',
                                          'México',
                                          'Canada'
                                        ].map<DropdownMenuItem<String>>(
                                            (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      ),
                                      Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8.0),
                                            child: Text(
                                              'Estado',
                                              textAlign: TextAlign.left,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline6,
                                            ),
                                          ),
                                        ],
                                      ),
                                      DropdownButton<String>(
                                        value: _selectedState,
                                        isExpanded: true,
                                        icon: Icon(Icons.arrow_downward),
                                        iconSize: 24,
                                        elevation: 16,
                                        onChanged: (String? newValue) {
                                          setState(() {
                                            _selectedState = newValue as String;
                                          });
                                          print(
                                              'TV - Selected state $_selectedState');
                                        },
                                        items: states
                                            .map<DropdownMenuItem<String>>(
                                                (String value) {
                                          return DropdownMenuItem<String>(
                                            value: value,
                                            child: Text(value),
                                          );
                                        }).toList(),
                                      ),
                                      Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8.0),
                                            child: Text(
                                              'Ciudad',
                                              textAlign: TextAlign.left,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline6,
                                            ),
                                          ),
                                        ],
                                      ),
                                      TextFormField(
                                        controller: _cityController,
                                        decoration:
                                            InputDecoration(labelText: null),
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return "* Debes ingresar una dirección";
                                          }
                                          if (value.length < 4) {
                                            return "* No olvides llenar este campo";
                                          }
                                          return null;
                                        },
                                      ),
                                      Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 8.0),
                                            child: Text(
                                              'Dirección',
                                              textAlign: TextAlign.left,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline6,
                                            ),
                                          ),
                                        ],
                                      ),
                                      TextFormField(
                                        controller: _addressController,
                                        decoration:
                                            InputDecoration(labelText: null),
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return "* Debes ingresar una dirección";
                                          }
                                          if (value.length < 4) {
                                            return "* No olvides llenar este campo";
                                          }
                                          return null;
                                        },
                                      ),
                                    ]),
                                  ),
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                TextButton(
                                  onPressed: () {
                                    _updateUser();
                                  },
                                  child: Text(
                                    "Actualizar",
                                    style: TextStyle(fontSize: 20.0),
                                  ),
                                ),
                                SizedBox(
                                  height: 22.5,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Center(
                      child: Text(
                        "Cargando información...",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
    );
  }

  Padding buildProfileLine(BuildContext context, String first, String content) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(first.toUpperCase(),
                style: Theme.of(context).primaryTextTheme.headline6),
          ),
          Expanded(
            flex: 2,
            child: Text(content, textAlign: TextAlign.left),
          ),
        ],
      ),
    );
  }
}
