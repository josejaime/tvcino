import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/neighborhood.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/user/change_neighborhood.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class ViewNeighborhood extends StatefulWidget {
  @override
  _ViewNeighborhoodState createState() => _ViewNeighborhoodState();
}

class _ViewNeighborhoodState extends State<ViewNeighborhood> {
  late Dio dio;
  late SharedPreferences preferences;
  Neighbordhood? _neighborhood;
  late String token;

  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;

  /// ==============================
  /// DIO
  /// ==============================
  String _message = "";
  bool _isLoading = false;
  bool _error = false;

  @override
  initState() {
    _user = instance.currentUser as auth.User;
    getNeighborhood();
    super.initState();
  }

  /// ==============================
  /// Get the current Neighborhood
  /// ==============================
  getNeighborhood() async {
    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================
    preferences = await SharedPreferences.getInstance();
    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    String neighborhoodId = preferences.getString('neighborhoodId') as String;

    Response response =
        await dio.get('/neighborhoods/$neighborhoodId').catchError((onError) {
      print("ERROR ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        print("Hubo un error en el servidor regresa e intenta nuevamente");
        setState(() {
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _isLoading = false;
          _error = true;
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response);
        print("DIO Trying to convert data");

        setState(() {
          _neighborhood = Neighbordhood.fromJson(response.data);
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _isLoading = false;
          _error = false;
        });
        print("Converted USER:");
        print(_neighborhood);
      }
    } else {
      print("Hubo un error en el servidor, intenta más tarde");
      setState(() {
        _message = "Hubo un error en el servidor regresa e intenta nuevamente";
        _isLoading = false;
        _error = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : _neighborhood != null
                  ? SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            padding: EdgeInsets.all(8.0),
                          ),
                          Container(
                            width: double.infinity,
                            //height: MediaQuery.of(context).size.height * 0.64,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15.0)),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 15.0,
                                ),
                                Text(
                                  "Tu Vecindario",
                                  style: Theme.of(context).textTheme.headline2,
                                  textAlign: TextAlign.left,
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  "Estos son los datos del vecindario al que perteneces",
                                  style: Theme.of(context).textTheme.bodyText2,
                                  textAlign: TextAlign.left,
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                buildProfileLine(
                                    context, 'Nombre', _neighborhood!.name),
                                buildProfileLine(
                                    context, 'País', _neighborhood!.country),
                                buildProfileLine(
                                    context, 'Estado', _neighborhood!.state),
                                buildProfileLine(
                                    context, 'Localidad', _neighborhood!.city),
                                buildProfileLine(context, 'Código Postal',
                                    _neighborhood!.zipcode),
                                SizedBox(
                                  height: 10.0,
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ChangeNeighborhood()));
                                  },
                                  child: Text("Cambiar Vecindario"),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Center(
                      child: TvcinoCircularProgressBar(
                        color: Colors.white,
                        textColor: Colors.white,
                      ),
                    ),
    );
  }

  Padding buildProfileLine(BuildContext context, String first, String content) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(first.toUpperCase(),
                style: Theme.of(context).primaryTextTheme.headline6),
          ),
          Expanded(
            flex: 2,
            child: Text(content, textAlign: TextAlign.left),
          ),
        ],
      ),
    );
  }
}
