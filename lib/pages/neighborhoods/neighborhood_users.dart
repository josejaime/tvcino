import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class NeighborhoodUsers extends StatefulWidget {
  @override
  _NeighborhoodUsersState createState() => _NeighborhoodUsersState();
}

class _NeighborhoodUsersState extends State<NeighborhoodUsers> {
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  late Dio dio;
  List<InterUser> _listOfInterUsers = List<InterUser>.empty(growable: true);
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  @override
  initState() {
    _user = instance.currentUser as auth.User;
    getNeighborhoodUsers();
    super.initState();
  }

  /// ==============================
  /// Get posts from server
  /// ==============================
  getNeighborhoodUsers() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String neighborhoodId = preferences.getString('neighborhoodId') as String;

    //7eda0aa3-624d-426a-93c5-f59b6f10b4a4
    Response response = await dio
        .get('/neighborhoods/$neighborhoodId/users')
        .catchError((onError) {
      print("ERROR DOING THE CALL ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        //print(response.data['items'][2]);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print('User: ' + data[i]['name'] + " id: " + data[i]['id']);
            _listOfInterUsers.add(InterUser.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(),
      //backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.black,
              ),
            )
          : SingleChildScrollView(
              child: Column(
              children: [
                Align(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12.5, horizontal: 8.5),
                    child: Text(
                      "Usuarios dentro del vecindario",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize:
                              Theme.of(context).textTheme.headline3!.fontSize,
                          fontFamily: Theme.of(context)
                              .textTheme
                              .headline3!
                              .fontFamily),
                    ),
                  ),
                ),
                _error
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: 28.0,
                            color: Colors.black,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(_message, style: TextStyle(color: Colors.black)),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: _listOfInterUsers.isNotEmpty
                              ? _listOfInterUsers.map((e) {
                                  return buildUser(e);
                                }).toList()
                              : [
                                  Text(
                                    "No hay nada para mostrar aún",
                                    style: TextStyle(color: Colors.black),
                                  )
                                ],
                        ),
                      ),
              ],
            )),
    );
  }

  GestureDetector buildUser(InterUser user) {
    return GestureDetector(
      onTap: () {
        print("Gesture detector");
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10), // if you need this
          /* side: BorderSide(
                    color: Colors.green,
                    width: 1,
                  ), */
        ),
        elevation: 2.0,
        child: Container(
          padding: EdgeInsets.all(15.0),
          width: MediaQuery.of(context).size.width * 0.9,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        user.name,
                        style: Theme.of(context).primaryTextTheme.headline4,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                child: Divider(
                  color: Theme.of(context).colorScheme.secondary,
                ),
                height: 10.0,
              ),
              user.phoneNumber == null || user.phoneNumber == "null"
                  ? buildRowUser('Teléfono:', 'N/A')
                  : buildRowUser('Teléfono:', user.phoneNumber),
              user.email != null
                  ? buildRowUser('Email:', user.email as String)
                  : buildRowUser('Email:', 'N/A'),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildRowUser(String label, String text) {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              label,
              style: Theme.of(context).textTheme.headline6,
            ),
            Text(text),
          ],
        ),
      ],
    );
  }
}
