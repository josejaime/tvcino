import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:firebase_messaging/firebase_messaging.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/classes/neighborhood.dart';
import 'package:tvcino/classes/posts.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_drawer.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/alerts/map.dart';
import 'package:tvcino/pages/events/event.dart';
import 'package:tvcino/pages/login/register_second.dart';
import 'package:tvcino/pages/posts/add_post.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:geocoding/geocoding.dart' as geo;
import 'package:http/http.dart' as http;

import 'alerts/my_alert_bottom.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  late FirebaseMessaging messaging;

  /// ==============================
  /// DIO GET THE POSTS
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = false;
  bool _error = false;
  Key textKey = Key('dissmisible');
  String? deviceToken;

  /// ==============================
  /// The Posts
  /// ==============================
  List<Post> _listOfPosts = List<Post>.empty(growable: true);

  List<String> choices = <String>[
    "Atraco",
    "Aviso",
  ];

  List<String> notifications = <String>[
    "No hay nada aquí",
  ];
  late String dataTitle;
  late String dataBody;
  late double latitude;
  late double longitude;

  String inAppMessage = "";
  String inAppTitle = "";
  bool _showNotification = false;

  // for use in the filters
  late String filterId;
  String filterZipcode = "";
  String filterCity = "";
  int activeFilter = 1;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    initFirebase();

    ///=============================
    /// FIRE BASE MESSAGING
    ///=============================
    // _firebaseMessaging.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     setState(() {
    //       print("Message $message on Message");

    //       print("Message on Resume $message on Resume");
    //       dataTitle = message['data']['title'];
    //       dataBody = message['data']['body'];
    //       print("The data body is");
    //       print(dataBody);
    //       print("The data title is");
    //       print(dataTitle);

    //       List<String> stringData = dataBody.split(',');
    //       latitude = double.parse(stringData[0]);
    //       longitude = double.parse(stringData[1]);

    //       print('Latitude: $latitude');
    //       print('Longitude: $longitude');

    //       //inAppMessage = "Mira la ubicación dando click en el botón" + dataBody;
    //       inAppMessage = "Mira la ubicación dando click en el botón";
    //       //inAppTitle = dataTitle;
    //       inAppTitle = "Alguien lanzó una alerta";
    //       _showNotification = true;
    //       //_message = '$message';
    //       //print(inAppMessage);
    //       notifications.add(message['body']);
    //     });
    //   },
    //   onLaunch: (message) async {
    //     setState(() {
    //       print("Message on Resume $message on Resume");
    //       dataTitle = message['data']['title'];
    //       dataBody = message['data']['body'];
    //       print("The data body is");
    //       print(dataBody);
    //       print("The data title is");
    //       print(dataTitle);

    //       List<String> stringData = dataBody.split(',');
    //       latitude = double.parse(stringData[0]);
    //       longitude = double.parse(stringData[1]);

    //       print('Latitude: $latitude');
    //       print('Longitude: $longitude');

    //       inAppMessage = "Mira la ubicación " + dataBody;
    //       //inAppTitle = dataTitle;
    //       inAppTitle = "Alguien lanzó una alerta";
    //       _showNotification = true;
    //       //_message = '$message';
    //     });
    //   },
    //   onResume: (message) async {
    //     setState(() {
    //       print("Message on Resume $message on Resume");
    //       dataTitle = message['data']['title'];
    //       dataBody = message['data']['body'];
    //       print("The data body is");
    //       print(dataBody);
    //       print("The data title is");
    //       print(dataTitle);

    //       List<String> stringData = dataBody.split(',');
    //       latitude = double.parse(stringData[0]);
    //       longitude = double.parse(stringData[1]);

    //       print('Latitude: $latitude');
    //       print('Longitude: $longitude');

    //       inAppMessage = "Mira la ubicación " + dataBody;
    //       //inAppTitle = dataTitle;
    //       inAppTitle = "Alguien lanzó una alerta";
    //       _showNotification = true;
    //       //_message = '$message';
    //     });
    //   },
    // );
    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   setState(() {
    //     print("The Device token is $token");
    //     deviceToken = token;
    //   });
    // });
    getUserInformation();
    getNeighborhood();
    print("TV - GOELOCATION");
    _getCurrentPosition();
    print("TV - END GOELOCATION");
    super.initState();
  }

  void initFirebase() {
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value) {
      print(value);
      deviceToken = value;
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      handleNotification(event);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('On Background push notification clicked!');
      handleNotification(message);
    });
  }

  handleNotification(RemoteMessage event) {
    setState(() {
      print("Message recieved");
      Map<String, dynamic> data = event.data;
      print(event.notification!.title);
      dataTitle = data['title'] as String;
      dataBody = data['body'] as String;
      print("The data body is");
      print(dataBody);
      print("The data title is");
      print(dataTitle);

      List<String> stringData = dataBody.split(',');
      latitude = double.parse(stringData[0]);
      longitude = double.parse(stringData[1]);

      print('Latitude: $latitude');
      print('Longitude: $longitude');
      inAppMessage = "Mira la ubicación dando click en el botón";
      inAppTitle = "Alguien lanzó una alerta";
      _showNotification = true;
    });
  }

  _getCurrentPosition() async {
    Position position = await _determinePosition();
    print('Lat = ${position.latitude}');
    print('Long = ${position.longitude}');
    List<geo.Placemark> placemarks = await geo.placemarkFromCoordinates(
        position.latitude, position.longitude,
        localeIdentifier: 'es_MX');
    print('GEOLOCATION');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setDouble('latitude', position.latitude);
    await preferences.setDouble('longitude', position.longitude);
    print(placemarks);
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    /* print("The user Token is = $token");
    print("Token Lenght: ${token.length}");
    print("PART 1: ${token.substring(0, 600)}");
    print("PART 2: ${token.substring(600)}"); */

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    Response response =
        await dio.get('/users?email=${_user.email}').catchError((onError) {
      print("TV - ERROR ON USER ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");
        print("Hubo un error en el servidor regresa e intenta nuevamente");
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response);

        if (response.data['items'].length == 0) {
          print("TV - Message - There is no user");
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => RegisterSecondPage()));
        } else {
          print("DIO Trying to convert data");
          print("Converted USER:");
          InterUser _userInter = InterUser.fromJson(response.data["items"][0]);
          print(_userInter.picture);

          if (_userInter.neighborhoodId == null) {
            print("TV - Message - User with out a neighborhood");
          }

          /// ================================================================
          /// Save some preferences
          /// ================================================================
          SharedPreferences preferences = await SharedPreferences.getInstance();
          await preferences.setString('userId', _userInter.id);
          await preferences.setString('name', _userInter.name);
          await preferences.setString('uuid', _user.uid);
          await preferences.setString('email', _user.email as String);
          await preferences.setString(
              'neighborhoodId', _userInter.neighborhoodId as String);
          await preferences.setString('country', _userInter.country as String);
          await preferences.setString('state', _userInter.state as String);
          await preferences.setString('city', _userInter.city as String);
          await preferences.setString('address', _userInter.address as String);
          await preferences.setString(
              'neighborhoodId', _userInter.neighborhoodId as String);
          print("DIO - Preferences userId ${preferences.getString('userId')}");
          print("DIO - Preferences UUID ${preferences.getString('uuid')}");

          filterId = _userInter.neighborhoodId!;
          _user.updateDisplayName(_userInter.name);

          getNeighborhoodPosts(_userInter.neighborhoodId as String, 1);
          if (_userInter.picture != null) {
            print('TV - Updating user profile from server');
            _user.updatePhotoURL(_userInter.picture);
          }
          getFavoritePosts(_userInter.id);
          //TODO: THIS IS BY NOW, AND SHOULD BE DELETED LATER
          if (deviceToken != null && deviceToken != '') {
            updateUser(_userInter);
          }

          /* if (!_user.emailVerified) {
            _user.sendEmailVerification();
          } */
        }
      }
    } else {
      print("Hubo un error en el servidor, intenta más tarde");
    }
  }

  updateUser(InterUser user) async {
    print("TV - UPDATE USER IN THE DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    String url = CustomFunctions.url + "/users/${user.id}";

    try {
      Map<String, String> datos = Map<String, String>();

      datos["data"] =
          '{"name": "${user.name}", "role_id": "${user.roleId}", "device_token": "$deviceToken"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      String token = await _user.getIdToken();
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      await request.send().then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          String value = await response.stream.bytesToString();
          InterUser updatedUser = InterUser.fromJson(jsonDecode(value));
          print("TV - UPDATED USER =====");
          print(updatedUser.name);
          print("TV - UPDATED USER =====");
          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print(
              "TV something went wrong updating user in the home ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
        }
      });
    } on SocketException {
      print(
          'TV EXCEPTION On updating user in the home - No Internet connection');
      return false;
    } on HttpException {
      print(
          "TV EXCEPTION On updating user in the home - Couldn't find the post");
      return false;
    } on FormatException {
      print("TV EXCEPTION On updating user in the home - Bad response format");
      return false;
    } on FileSystemException {
      print(
          "TV FileSystemException On updating user in the home - Bad response format");
      return false;
    }
    return false;
  }

  /// ==============================
  /// Get posts from server
  /// ==============================
  getNeighborhoodPosts(String neighborhoodId, int filter) async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================

    dio = CustomFunctions.getDio();

    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;

    String request = '/posts?';

    activeFilter = filter;

    if (filter == 1) {
      request = request + 'neighborhood_id=$neighborhoodId';
    } else if (filter == 2) {
      request = request + 'zipcode=$filterZipcode';
    } else if (filter == 3) {
      request = request + 'city=$filterCity';
    }

    //request = request + '&status=1&start=0&limit=200';
    request = request + '&start=0&limit=200';
    print('TV - REQUSET $request');

    /*Response response = await dio
        .get(
            '/posts?neighborhood_id=$neighborhoodId&status=1&start=0&limit=200')
        //.get('/posts?start=0&limit=200')
        .catchError((onError) {
      print("TV - Error getting posts ${onError.toString()}");
      safeCall = false;
    });*/
    Response response = await dio.get(request)
        //.get('/posts?start=0&limit=200')
        .catchError((onError) {
      print("TV - Error getting posts ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        _listOfPosts.clear();
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print(data[i]);
            Post temPost = Post.fromJson(data[i]);
            if (temPost.groupId == null) {
              _listOfPosts.add(temPost);
            }
            //_listOfPosts.add(Post.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getFavoritePosts(String userId) async {
    dio = CustomFunctions.getDio();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    Response response =
        await dio.get('/users/$userId/favorites/posts').catchError((onError) {
      print("TV - ERROR ON USER ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print(
            "TV DIO ERROR Favorite Posts- Status Code ${response.statusCode}");
        print(
            "TV DIO ERROR Favorite Posts- Status Message ${response.statusMessage}");
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Favorite Posts Response RAW Data");
        print(response);
        SharedPreferences preferences = await SharedPreferences.getInstance();
        List<String> favoritePosts = List<String>.empty(growable: true);
        List data = response.data['items'];
        print("TV - DATA FAVORITES LENGHT = ${data.length}");

        for (int i = 0; i < data.length; i++) {
          //Post tempPost = Post.fromJson(data[i]);
          favoritePosts.add(data[i]['id']);
        }
        debugPrint(
            'TV - Favorite Post List created size = ${favoritePosts.length}');

        /// ================================================================
        /// Save some preferences
        /// ================================================================
        await preferences.setStringList('favoritePosts', favoritePosts);
      }
    } else {
      print("Hubo un error en el servidor, intenta más tarde");
    }
  }

  /// ==============================
  /// Get the current Neighborhood
  /// ==============================
  getNeighborhood() async {
    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================
    SharedPreferences preferences = await SharedPreferences.getInstance();
    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    bool safeCall = true;
    String neighborhoodId = preferences.getString('neighborhoodId') as String;

    Response response =
        await dio.get('/neighborhoods/$neighborhoodId').catchError((onError) {
      print("ERROR ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print(
            "TV DIO ERROR GET NEIGHBORHOOD - Status Code ${response.statusCode}");
        print(
            "TV DIO ERROR GET NEIGHBORHOOD - Status Message ${response.statusMessage}");
      } else {
        print("TV DIO Status Code GET NEIGHBORHOOD  ${response.statusCode}");
        print(response);

        Neighbordhood neighborhood = Neighbordhood.fromJson(response.data);
        filterZipcode = neighborhood.zipcode;
        filterCity = neighborhood.city;

        print("TV SAVING NEIGHBORHOOD PREFERENCES");
        await preferences.setString(
            'neighbordhoodCountry', neighborhood.country);
        await preferences.setString('neighbordhoodState', neighborhood.state);
        await preferences.setString('neighbordhoodCity', neighborhood.city);
        print(neighborhood);
      }
    } else {
      print(
          "Hubo un error en el servidor al consultar el vecindario, intenta más tarde");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
          title: Image(
            image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
            height: 21.0,
          ),
          centerTitle: true,
          //backgroundColor: Colors.white,
          backgroundColor: Theme.of(context).primaryColor,
          //iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
          actions: <Widget>[
            IconButton(
              iconSize: 45.0,
              onPressed: () {
                showModalBottomSheet(
                  backgroundColor: Theme.of(context).primaryColor,
                  isDismissible: true,
                  isScrollControlled: true,
                  context: context,
                  builder: (_) => MyBottomSheet(),
                );
              },
              icon: Image.asset('assets/iconos/emergencia_blanco.png'),
            ),
          ]),
      drawer: TvcinoDrawer(
        user: _user,
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    Flexible(
                      child: ListView(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              TextButton(
                                style: TextButton.styleFrom(
                                    backgroundColor: activeFilter == 1
                                        ? Theme.of(context).primaryColor
                                        : Theme.of(context).highlightColor,
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0))),
                                onPressed: () {
                                  getNeighborhoodPosts(filterId, 1);
                                },
                                child: Text("Vecindario"),
                              ),
                              TextButton(
                                style: TextButton.styleFrom(
                                    backgroundColor: activeFilter == 2
                                        ? Theme.of(context).primaryColor
                                        : Theme.of(context).highlightColor,
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0))),
                                onPressed: () {
                                  getNeighborhoodPosts(filterZipcode, 2);
                                },
                                child: Text("Zona"),
                              ),
                              TextButton(
                                style: TextButton.styleFrom(
                                    backgroundColor: activeFilter == 3
                                        ? Theme.of(context).primaryColor
                                        : Theme.of(context).highlightColor,
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0))),
                                onPressed: () {
                                  getNeighborhoodPosts(filterCity, 3);
                                },
                                child: Text("Ciudad"),
                              ),
                            ],
                          ),
                          _showNotification
                              ? Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  elevation: 2.0,
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 10.0),
                                  child: Container(
                                    padding: EdgeInsets.all(15.0),
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 8.0),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  inAppTitle,
                                                  style: Theme.of(context)
                                                      .primaryTextTheme
                                                      .headline4,
                                                ),
                                              ),
                                              IconButton(
                                                  icon: Icon(Icons.close),
                                                  onPressed: () {
                                                    setState(() {
                                                      _showNotification = false;
                                                    });
                                                  })
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 8.0,
                                        ),
                                        Text(inAppMessage),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          child: TextButton(
                                            onPressed: () {
                                              print('Latitude: $latitude');
                                              print('Longitude: $longitude');
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          AlertMap(
                                                            latitude: latitude,
                                                            longitude:
                                                                longitude,
                                                          )));
                                            },
                                            child: Text(
                                                'Revisar alerta'.toUpperCase()),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              : Offstage(),
                          Align(
                            child: Container(
                              width: MediaQuery.of(context).size.width / 1.1,
                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Material(
                                elevation: 2.0,
                                borderRadius: BorderRadius.circular(15),
                                color: Theme.of(context).primaryColor,
                                child: Container(
                                  margin: EdgeInsets.all(8.0),
                                  padding: EdgeInsets.all(5.0),
                                  width:
                                      MediaQuery.of(context).size.width / 1.1,
                                  height: 175,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.circular(15),
                                    image: DecorationImage(
                                      image: ExactAssetImage(
                                          "assets/images/tuvcino-ilustracion-seguridad.png"),
                                      alignment: Alignment.centerRight,
                                      fit: BoxFit.scaleDown,
                                    ),
                                  ),
                                  child: Stack(
                                    children: [
                                      Positioned(
                                        top: 15,
                                        left: 5,
                                        //child: Text("Novedades", style: Theme.of(context).textTheme.headline2 ),
                                        child: Text(
                                            "Publicaciones \nmás \nrecientes",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 24)),
                                      ),
                                      Positioned(
                                          bottom: 2.5,
                                          left: 25,
                                          child: TextButton(
                                              style: TextButton.styleFrom(
                                                backgroundColor: Colors.white,
                                                primary: Colors.black,
                                                textStyle: TextStyle(
                                                  color: Colors.black,
                                                ),
                                              ),
                                              onPressed: () {
                                                print(
                                                    "Hello there, general kenobi");
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            AddPostPage()));
                                              },
                                              child: Text(
                                                'Crear ahora',
                                                /* style: Theme.of(context)
                                                      .textTheme
                                                      .button */
                                              ))),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          !_isLoading && _error ? Text(_message) : Offstage(),
                          _listOfPosts.isNotEmpty
                              ? _buildPostsFromGet(_listOfPosts)
                              : Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 15.0,
                                      ),
                                      Text(
                                        "No hay publicaciones para mostrar aquí, vuelve pronto",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline4,
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                          SizedBox(
                            height: 15.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(
        disabledButton: 1,
      ),
    );
  }

  Widget _buildPostsFromGet(List<Post> posts) {
    return Column(
      children: posts.map((e) {
        return buildCardWithImage(context, e.id, e.image, e);
      }).toList(),
    );
  }

  Widget buildCardWithImage(
      BuildContext context, String index, String image, Post post) {
    return Column(
      children: [
        SizedBox(
          height: 10.0,
        ),
        Align(
          child: GestureDetector(
            onTap: () {
              print("Hello new gesture detector");
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EventPage(
                            id: index,
                            tile: post.title,
                            image: image,
                            type: 1,
                          )));
            },
            child: Container(
              height: 275,
              width: MediaQuery.of(context).size.width / 1.1,
              decoration: BoxDecoration(
                image: image != null
                    ? DecorationImage(
                        //image: ExactAssetImage(image),
                        image: NetworkImage(image),
                        fit: BoxFit.cover,
                        alignment: Alignment.center)
                    : null,
                borderRadius: BorderRadius.circular(15.0),
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.25),
                    spreadRadius: 3,
                    blurRadius: 3,
                    offset: Offset(2, 4), // changes position of shadow
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Positioned(
                    bottom: 0,
                    width: MediaQuery.of(context).size.width / 1.1,
                    child: Container(
                      //color: Colors.white,
                      padding: EdgeInsets.all(10.0),
                      height: 120,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.5),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Flexible(
                                child: RichText(
                                  overflow: TextOverflow.ellipsis,
                                  strutStyle: StrutStyle(fontSize: 12.0),
                                  text: TextSpan(
                                      style:
                                          Theme.of(context).textTheme.subtitle1,
                                      text: post.title),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Text((post.shortDescription!.length > 125
                              ? post.shortDescription!.substring(0, 123) + "..."
                              : post.shortDescription) as String),
                          SizedBox(
                            height: 10.0,
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                      right: 2.5,
                      bottom: 100,
                      //top: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          /*border: Border.all(
                                color: Colors.black,
                                width: 0.1
                              ),*/
                          borderRadius: BorderRadius.circular(50.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.25),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset:
                                  Offset(1, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: IconButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EventPage(
                                          id: post.id,
                                          tile: post.title,
                                          image: image,
                                          type: 1,
                                        )));
                          },
                          icon: Icon(Icons.send),
                          //icon: Image.asset('assets/iconos/enviar.png'),
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
      ],
    );
  }

  InkWell buildFilter(BuildContext context, String title, String imageSource,
      bool selected, tapFunction) {
    return InkWell(
      onTap: tapFunction,
      borderRadius: BorderRadius.circular(15.0),
      child: Container(
        width: MediaQuery.of(context).size.width / 3,
        margin: EdgeInsets.all(5.0),
        height: 100.0,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          color: selected ? Colors.white38 : Colors.white,
          borderRadius: BorderRadius.circular(15.0),
          border: Border.all(
            color: Colors.black45,
            style: selected ? BorderStyle.solid : BorderStyle.none,
            width: 0.5,
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 1.5,
              blurRadius: 1.5,
              offset: Offset(2, 2), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
              image: AssetImage(imageSource),
              height: 55.0,
            ),
            /*IconButton(
              icon: Image.asset('assets/images/servicios_i.png'),
              onPressed: () {
              }
            ),*/
            Text(
              title,
              style: Theme.of(context).textTheme.subtitle1,
            )
          ],
        ),
      ),
    );
  }

  showBottomMessage(IconData icon, String message) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (BuildContext context,
            StateSetter setModalState /*You can rename this!*/) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.4,
            padding: EdgeInsets.all(25.0),
            color: Theme.of(context).accentColor,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    icon,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    "$message",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          );
        });
      },
    );
  }
}
