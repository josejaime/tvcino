import 'package:flutter/material.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/events/event.dart';

import 'filters.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        title: Text(
          "Búsqueda",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 8),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(
                              color: Theme.of(context).primaryColor,
                              width: 1.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(
                            color: Theme.of(context).primaryColor,
                            width: 1.0,
                          ),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          borderSide: BorderSide(
                            width: 1.0,
                          ),
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          borderSide: BorderSide(
                            color: Colors.red,
                            width: 2.0,
                          ),
                        ),
                        hintText: "¿Qué necesitas vecino?",
                        suffixIcon: IconButton(
                            icon: Icon(
                              Icons.search,
                              color: Theme.of(context).primaryColor,
                            ),
                            onPressed: () {})),
                    onFieldSubmitted: (String value) {},
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Debes ingresar una búsqueda";
                      }
                      if (value.length < 5) {
                        return "Intenta con mínimo 5 caracteres";
                      }
                      return null;
                    },
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 6.0),
            child: Text(
              "Resultados de la búsqueda",
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          /*Padding(
            padding: const EdgeInsets.symmetric( horizontal: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FlatButton(
                  color: Theme.of(context).buttonTheme.colorScheme.secondary,
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => FiltersPage()));
                  }, 
                  child: Text("Filtros")
                ),
                FlatButton(
                  color: Theme.of(context).buttonTheme.colorScheme.secondary,
                  onPressed: (){}, 
                  child: Text("Mapa")
                ),
              ],
            ),
          ),*/
          Flexible(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FiltersPage()));
                          },
                          child: Text("Filtros")),
                      /* FlatButton(
                        color: Theme.of(context).buttonTheme.colorScheme.secondary,
                        onPressed: (){}, 
                        child: Text("Mapa")
                      ), */
                    ],
                  ),
                  buildResult(context, "assets/images/yoga_home_.jpg"),
                  SizedBox(
                    height: 12.5,
                  ),
                  buildResult(context, "assets/images/imagen_home_3.jpg"),
                  SizedBox(
                    height: 12.5,
                  ),
                  buildResult(context, "assets/images/imagen_home_4.jpg"),
                  SizedBox(
                    height: 12.5,
                  ),
                  buildResult(context, "assets/images/imagen_home_ 2.jpg"),
                  SizedBox(
                    height: 12.5,
                  ),
                  buildResult(context, "assets/images/imagen_home_arbol.jpg"),
                  SizedBox(
                    height: 12.5,
                  ),
                  buildResult(context, "assets/images/imagen_home_auto.jpg"),
                  SizedBox(
                    height: 12.5,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  GestureDetector buildResult(BuildContext context, String image) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EventPage(
                      id: 1.toString(),
                      image: image,
                      tile: "Resultado de Búsqueda",
                    )));
      },
      child: Container(
        clipBehavior: Clip.hardEdge,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            //color: Colors.indigo.withOpacity(0.5),
            color: Colors.white,
            borderRadius: BorderRadius.circular(15.0)),
        child: Row(
          children: [
            Container(
              width: 105,
              height: 105,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(image),
                    fit: BoxFit.cover,
                    alignment: Alignment.center),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 4.0, horizontal: 6.0),
                    child: Text("Resultado de búsqueda",
                        style: Theme.of(context).primaryTextTheme.headline5),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 4.0, horizontal: 6.0),
                    child: Text("Lorem ipsum etmi acse vertium serum relac"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
