import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:tvcino/classes/tag.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/alerts/map.dart';
import 'package:tvcino/pages/alerts/my_alert_bottom.dart';
import 'package:tvcino/pages/directories/directories.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class FiltersPage extends StatefulWidget {
  @override
  _FiltersPageState createState() => _FiltersPageState();
}

class _FiltersPageState extends State<FiltersPage> {
  /// ==============================
  /// DIO GET THE Tags
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = false;
  bool _error = false;

  /// ==============================
  /// The Tags
  /// ==============================
  List<Tag> _listOfTags = List<Tag>.empty(growable: true);
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;

  String inAppMessage = "";
  String inAppTitle = "";
  late FirebaseMessaging messaging;
  bool _showNotification = false;

  List<String> choices = <String>[
    "Atraco",
    "Aviso",
  ];

  late String dataTitle;
  late String dataBody;
  late double latitude;
  late double longitude;

  /// ==============================
  /// Get posts from server
  /// ==============================
  getTags() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================

    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;

    Response response = await dio.get('/tags').catchError((onError) {
      print("ERRORORORORORO");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data);
        print("DIO Trying to convert data");
        //List data = response.data['items'];
        List data = response.data;
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            /* print(data[i]['name']);
            print(data[i]['icon']); */
            //_listOfUsers.add(InterUser.fromJson(_data[i]));
            _listOfTags.add(Tag.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  void initState() {
    _user = instance.currentUser as auth.User;

    initFirebase();
    getTags();
    super.initState();
  }

  void initFirebase() {
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value) {
      print(value);
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      handleNotification(event);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('On Background push notification clicked!');
      handleNotification(message);
    });
  }

  handleNotification(RemoteMessage event) {
    setState(() {
      print("Message recieved");
      Map<String, dynamic> data = event.data;
      print(event.notification!.title);
      dataTitle = data['title'] as String;
      dataBody = data['body'] as String;
      print("The data body is");
      print(dataBody);
      print("The data title is");
      print(dataTitle);

      List<String> stringData = dataBody.split(',');
      latitude = double.parse(stringData[0]);
      longitude = double.parse(stringData[1]);

      print('Latitude: $latitude');
      print('Longitude: $longitude');
      inAppMessage = "Mira la ubicación dando click en el botón";
      inAppTitle = "Alguien lanzó una alerta";
      _showNotification = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Image(
            image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
            height: 21.0,
          ),
          centerTitle: true,
          //backgroundColor: Colors.white,
          backgroundColor: Theme.of(context).primaryColor,
          //iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
          actions: <Widget>[
            IconButton(
              iconSize: 45.0,
              onPressed: () {
                showModalBottomSheet(
                  backgroundColor: Theme.of(context).primaryColor,
                  isDismissible: true,
                  isScrollControlled: true,
                  context: context,
                  builder: (_) => MyBottomSheet(),
                );
              },
              icon: Image.asset('assets/iconos/emergencia_blanco.png'),
            ),
          ]),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Theme.of(context).primaryColor,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Column(
                    children: [
                      _showNotification
                          ? Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              elevation: 2.0,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 10.0),
                              child: Container(
                                padding: EdgeInsets.all(15.0),
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 8.0),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              inAppTitle,
                                              style: Theme.of(context)
                                                  .primaryTextTheme
                                                  .headline4,
                                            ),
                                          ),
                                          IconButton(
                                              icon: Icon(Icons.close),
                                              onPressed: () {
                                                setState(() {
                                                  _showNotification = false;
                                                });
                                              })
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 8.0,
                                    ),
                                    Text(inAppMessage),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: TextButton(
                                        onPressed: () {
                                          print('Latitude: $latitude');
                                          print('Longitude: $longitude');
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      AlertMap(
                                                        latitude: latitude,
                                                        longitude: longitude,
                                                      )));
                                        },
                                        child: Text(
                                            'Revisar alerta'.toUpperCase()),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          : Offstage(),
                      Padding(
                        //padding: const EdgeInsets.only(bottom: 6.0, top: 6.0, left: 8.0),
                        padding: const EdgeInsets.symmetric(
                            vertical: 15.0, horizontal: 15.0),
                        child: Text(
                          "¿Qué tipo de lugar te gustaría ver?",
                          style: Theme.of(context).textTheme.headline3,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      GridView.count(
                        crossAxisCount: 2,
                        primary: false,
                        padding: const EdgeInsets.all(20.0),
                        crossAxisSpacing: 7.5,
                        mainAxisSpacing: 7.5,
                        shrinkWrap: true,
                        children: [
                          /* buildFilter(
                              context,
                              Tag(
                                description: "lorem",
                                icon: 'tuvcino-filtro-otros-i.png',
                                id: '99',
                                name: 'Todos',
                              )), */
                          for (var item in _listOfTags)
                            buildFilter(context, item),
                        ],
                      )
                    ],
                  ),
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(
        disabledButton: 3,
      ),
    );
  }
}

Widget buildTagList(List<Tag> tags, BuildContext context) {
  return Wrap(
    children: tags.map((e) {
      //return Text("${e.name}");
      return buildFilter(context, e);
    }).toList(),
  );
}

InkWell buildFilter(BuildContext context, Tag element) {
  return InkWell(
    onTap: () {
      print('TV - Click on filter ${element.id}');
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DirectoriesPage(
                    tagFilterId: element.id,
                  )));
    },
    borderRadius: BorderRadius.circular(15.0),
    child: Container(
      //width: MediaQuery.of(context).size.width / 2,
      margin: EdgeInsets.all(5.0),
      //height: 100.0,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15.0),
        border: Border.all(
          color: Theme.of(context).primaryColor.withOpacity(0.5),
          style: BorderStyle.solid,
          width: 0.5,
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 1.5,
            blurRadius: 1.5,
            offset: Offset(2, 2), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            image: AssetImage('assets/filtros/${element.icon}'),
            height: 70.0,
          ),
          Text(
            element.name as String,
            style: Theme.of(context).textTheme.subtitle1,
            textAlign: TextAlign.center,
          )
        ],
      ),
    ),
  );
}
