import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:http/http.dart' as http;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/groups/group.dart';
import 'package:tvcino/utils/custom_functions.dart';

class AddGrupPostPage extends StatefulWidget {
  final Group group;

  AddGrupPostPage({
    required this.group,
  });

  @override
  _AddGrupPostPageState createState() => _AddGrupPostPageState();
}

class _AddGrupPostPageState extends State<AddGrupPostPage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  ///////////////////////////////////
  bool isChecked = true;
  List<String> reportList = [
    "General",
  ];
  List<Widget> choices = List.empty(growable: true);
  String selectedChoice = "";

  /// ==============================
  /// Images
  /// ==============================
  late File _image;
  final picker = ImagePicker();
  TextEditingController _imageTextController = TextEditingController();
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  //===============================*/
  // Controlls
  //===============================*/
  final GlobalKey<FormState> _postAddKey = GlobalKey<FormState>();
  final TextEditingController _titleController = TextEditingController();
  //final TextEditingController _placeController = TextEditingController();
  final TextEditingController _shortController = TextEditingController();
  bool _errorText = false;

  String name = "";
  String uuid = "";
  String userId = "";
  String title = "";
  int type = 0;
  int status = 0;
  String shortDescription = "";
  String content = "";
  String date = "";
  String location = "";
  String categoryId = "";

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getUserInformation();
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
    print("The user Token is = $token");
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addPost() async {
    print("TV - ADD POST TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    uuid = preferences.getString('uuid') as String;
    //userId = preferences.getString('userId') as String;
    title = _titleController.text;
    type = 1;
    status = 1;
    shortDescription = _shortController.text;
    content = _shortController.text;
    DateTime myDate = DateTime.now();
    DateFormat format = DateFormat('dd-MM-yyyy');
    date = format.format(myDate);
    //location = _placeController.text;
    String location = "null";
    userId = preferences.getString('userId') as String;
    categoryId = "55cbe008-c6b6-45df-b86f-5b5c11b25024";

    String _fileName = _imageTextController.text.split('/').last;

    String url = CustomFunctions.url + "/posts/";

    try {
      Map<String, String> datos = Map<String, String>();

      shortDescription = shortDescription.replaceAll(RegExp('"'), '\'');
      content = shortDescription.replaceAll(RegExp('"'), '\'');

      datos["data"] =
          '{"title": "$title", "type": $type, "status": $status, "short_description": "$shortDescription","content": "$content", "date": "$date", "location": "$location","user_id": "$userId", "category_id": "$categoryId", "group_id": "${widget.group.id}"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("POST", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      if (_image != null) {
        //============================================/
        /// CHECK IF THE IMAGE IS AVAILABLE
        //============================================/
        request.files.add(http.MultipartFile.fromBytes(
            'image', File(_image.path).readAsBytesSync(),
            filename: _fileName));
      }
      request.send().then((response) async {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - POST GROUP CREATED ALL OK");
          /* Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false); */
          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => GroupPage(
                group: widget.group,
              ),
            ),
          );
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });

      /* var response = await http.post(url,
          headers: {"authorization": "$token"},
          //headers: {"Content-Type": "application/json"},
          body: datos);

      print('TV - ADD POST - Response status: ${response.statusCode}');
      print('TV - ADD POST - Response Headres: ${response.headers}');
      print('TV - ADD POST - Response body: ${response.body}');

      if (response.statusCode != 200) {
        print("TV HTTP ERROR - Status Code ${response.statusCode}");
        setState(() {
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _error = true;
          _isLoading = false;
        });
      } else {
        //============================================/
        /// 200 is everything ok
        /// Set preferences neighborhoodSet = true
        //============================================/

        setState(() {
          _isLoading = false;
          _error = false;
          _message = "";
        });

        print("TV - POST CREATED ALL OK");
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
        return true;
      } */
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print("TV: Image selected");
        print(_image);
        //_imageTextController.text = _image.path;
        _imageTextController.text = _image.path.split('/').last;
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /***********/
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Nueva Publicación",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información general",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            _image != null
                                ? Column(
                                    children: [
                                      GestureDetector(
                                        onTap: getImage,
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.95,
                                          height: 150,
                                          child: Image.file(
                                            _image,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          "* Puede cambiar la forma de verse al guardar",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w100,
                                              fontSize: 10.0,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                    ],
                                  )
                                : Column(
                                    children: [
                                      GestureDetector(
                                        onTap: getImage,
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.95,
                                          height: 150,
                                          child: Image.asset(
                                              'assets/images/no_image_placeholder.png'),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          "* Para una mejor visualización utiliza imágenes horizontales",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w100,
                                              fontSize: 12.5,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                    ],
                                  ),
                            Form(
                              key: _postAddKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                    readOnly: true,
                                    controller: TextEditingController(
                                        text: widget.group.name),
                                    decoration:
                                        InputDecoration(labelText: "Grupo"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar una dirección";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con una dirección más larga";
                                      }
                                      return null;
                                    },
                                  ),
                                  TextFormField(
                                    controller: _titleController,
                                    decoration:
                                        InputDecoration(labelText: "Nombre"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar tu nombre";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con un nombre más largo";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  /* Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "¿Publicar directamente?",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                      Transform.scale(
                                        scale: 1.2,
                                        child: Checkbox(
                                            value: isChecked,
                                            onChanged: (bool value) {
                                              setState(() {
                                                //isChecked = value;
                                                isChecked = true;
                                              });
                                            }),
                                      ),
                                    ],
                                  ), */
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  TextField(
                                    maxLines: 6,
                                    controller: _shortController,
                                    decoration: InputDecoration(
                                      hintText: "Agrega una descripción aquí",
                                      errorText: _errorText
                                          ? "* No olvides llenar este campo"
                                          : null,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  TextFormField(
                                    onTap: () {
                                      getImage();
                                    },
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: "Imagen",
                                      /* suffixIcon: IconButton(
                                          icon: Icon(Icons.file_upload),
                                          onPressed: () {
                                            getImage();
                                          }), */
                                      suffixIcon: Icon(Icons.file_upload),
                                    ),
                                    controller: _imageTextController,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes poner una imagen";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Publicar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    onPressed: () {
                                      if (_postAddKey.currentState!
                                          .validate()) {
                                        if (_shortController.text.isNotEmpty) {
                                          print("TV - VALIDATE TRUE");
                                          setState(() {
                                            _errorText = false;
                                          });
                                          _addPost();
                                        } else {
                                          setState(() {
                                            _errorText = true;
                                          });
                                          print("TV - VALIDATE MISS CONTENT");
                                        }
                                      } else {
                                        print("TV - VALIDATE FALSE");
                                      }
                                      //_addPost();
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}

class MultiSelectChip extends StatefulWidget {
  final List<String> reportList;
  MultiSelectChip(this.reportList);
  @override
  _MultiSelectChipState createState() => _MultiSelectChipState();
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  String selectedChoice = "";
  // this function will build and return the choice list
  _buildChoiceList() {
    List<Widget> choices = List.empty(growable: true);
    widget.reportList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.only(right: 2.0, left: 2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoice == item,
          onSelected: (selected) {
            setState(() {
              selectedChoice = item;
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
