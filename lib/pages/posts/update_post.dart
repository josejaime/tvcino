import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/posts.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:http/http.dart' as http;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/events/event.dart';
import 'package:tvcino/utils/custom_functions.dart';

class UpdatePost extends StatefulWidget {
  final Post post;

  UpdatePost({
    required this.post,
  });

  @override
  _UpdatePostState createState() => _UpdatePostState();
}

class _UpdatePostState extends State<UpdatePost> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  ///////////////////////////////////
  bool isChecked = true;
  List<String> reportList = [
    "General",
  ];
  List<Widget> choices = List.empty(growable: true);
  String selectedChoice = "";

  /// ==============================
  /// Images
  /// ==============================
  File? _image;
  final picker = ImagePicker();
  TextEditingController _imageTextController = TextEditingController();
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  //===============================*/
  // Controlls
  //===============================*/
  final GlobalKey<FormState> _postUpdateKey = GlobalKey<FormState>();
  late TextEditingController _titleController;
  late TextEditingController _shortController;
  bool _errorText = false;

  late String name;
  late String uuid;
  late String userId;
  late String title;
  late int type;
  late int status;
  late String shortDescription;
  late String content;
  late String categoryId;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getUserInformation();
    _titleController = TextEditingController(text: widget.post.title);
    _shortController = TextEditingController(text: widget.post.content);
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
    print("The user Token is = $token");
  }

  /// =============================================
  /// Function
  /// update post in the TVCINO DB
  /// =============================================
  Future<bool> _updatePost() async {
    print("TV - ADD POST TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    uuid = preferences.getString('uuid') as String;
    //userId = preferences.getString('userId') as String;
    title = _titleController.text;
    type = 1;
    status = 1;
    shortDescription = _shortController.text;
    content = _shortController.text;
    userId = preferences.getString('userId') as String;
    categoryId = "55cbe008-c6b6-45df-b86f-5b5c11b25024";

    String _fileName = _imageTextController.text.split('/').last;

    String url = CustomFunctions.url + "/posts/${widget.post.id}";

    try {
      Map<String, String> datos = Map<String, String>();

      shortDescription = shortDescription.replaceAll(RegExp('"'), '\'');
      content = shortDescription.replaceAll(RegExp('"'), '\'');

      datos["data"] =
          '{"title": "$title", "type": $type, "status": $status, "short_description": "$shortDescription","content": "$content", "date": "${widget.post.date}", "category_id": "$categoryId"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken();
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      if (_image != null) {
        //============================================/
        /// CHECK IF THE IMAGE IS AVAILABLE
        //============================================/
        request.files.add(http.MultipartFile.fromBytes(
            'image', File(_image!.path).readAsBytesSync(),
            filename: _fileName));
      }
      request.send().then((response) async {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - POST UPDATED ALL OK");
          //Navigator.pop(context);
          //Navigator.popUntil(context, ModalRoute.withName('/home'));
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => EventPage(
                        id: widget.post.id,
                        image: widget.post.image,
                        tile: widget.post.title,
                        type: 1,
                      )));

          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print("TV: Image selected");
        print(_image);
        //_imageTextController.text = _image.path;
        _imageTextController.text = _image!.path.split('/').last;
      } else {
        print('No image selected.');
      }
    });
  }

  /// =============================================
  /// Function
  /// update post in the TVCINO DB
  /// =============================================
  Future<bool> _deletePost() async {
    print("TV - ADD POST TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    String url = CustomFunctions.url + "/posts/${widget.post.id}";

    try {
      token = await _user.getIdToken();
      //print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("DELETE", postUri);
      request.headers['authorization'] = "$token";

      request.send().then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - POST DELETED ALL OK");
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    /***********/
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Actualizar el Post",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información general",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            _image != null
                                ? Column(
                                    children: [
                                      GestureDetector(
                                        onTap: getImage,
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.95,
                                          height: 150,
                                          child: Image.file(
                                            _image!,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          "* Puede cambiar la forma de verse al guardar",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w100,
                                              fontSize: 10.0,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                    ],
                                  )
                                : Column(
                                    children: [
                                      GestureDetector(
                                        onTap: getImage,
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.95,
                                          height: 150,
                                          child: Image(
                                            fit: BoxFit.cover,
                                            image:
                                                NetworkImage(widget.post.image),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          "* Puede cambiar la forma de verse al guardar",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w100,
                                              fontSize: 10.0,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                    ],
                                  ),
                            Form(
                              key: _postUpdateKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: _titleController,
                                    decoration:
                                        InputDecoration(labelText: "Nombre"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar tu nombre";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con un nombre más largo";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  /* Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "¿Publicar directamente?",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                      Transform.scale(
                                        scale: 1.2,
                                        child: Checkbox(
                                            value: isChecked,
                                            onChanged: (bool value) {
                                              setState(() {
                                                //isChecked = value;
                                                isChecked = true;
                                              });
                                            }),
                                      ),
                                    ],
                                  ), */
                                  SizedBox(
                                    height: 7.5,
                                  ),
                                  //MultiSelectChip(reportList),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  TextField(
                                    maxLines: 6,
                                    controller: _shortController,
                                    decoration: InputDecoration(
                                      hintText: "Agrega una descripción aquí",
                                      errorText: _errorText
                                          ? "* No olvides llenar este campo"
                                          : null,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  TextFormField(
                                    decoration: InputDecoration(
                                      labelText: "Imagen",
                                      suffixIcon: IconButton(
                                          icon: Icon(Icons.file_upload),
                                          onPressed: () {
                                            getImage();
                                          }),
                                    ),
                                    controller: _imageTextController,
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Actualizar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    onPressed: () {
                                      if (_postUpdateKey.currentState!
                                          .validate()) {
                                        if (_shortController.text.isNotEmpty) {
                                          print("TV - VALIDATE TRUE");
                                          setState(() {
                                            _errorText = false;
                                          });
                                          _updatePost();
                                        } else {
                                          setState(() {
                                            _errorText = true;
                                          });
                                          print("TV - VALIDATE MISS CONTENT");
                                        }
                                      } else {
                                        print("TV - VALIDATE FALSE");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 2.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Borrar el Post",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                backgroundColor: Colors.red,
                                primary: Colors.white,
                              ),
                              child: Text(
                                "Borrar",
                                style: TextStyle(fontSize: 20.0),
                              ),
                              onPressed: () {
                                _showDialog();
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Borrar"),
          content: Text("¿Realmente quieres borrar el post?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            TextButton(
              child: Text("Cerrar"),
              //color: Theme.of(context).buttonTheme.colorScheme.primary,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                primary: Colors.white,
              ),
              child: Text("Aceptar"),
              onPressed: () async {
                Navigator.pop(context);
                await _deletePost();
              },
            ),
          ],
        );
      },
    );
  }
}

class MultiSelectChip extends StatefulWidget {
  final List<String> reportList;
  MultiSelectChip(this.reportList);
  @override
  _MultiSelectChipState createState() => _MultiSelectChipState();
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  String selectedChoice = "";
  // this function will build and return the choice list
  _buildChoiceList() {
    List<Widget> choices = List.empty(growable: true);
    widget.reportList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.only(right: 2.0, left: 2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoice == item,
          onSelected: (selected) {
            setState(() {
              selectedChoice = item;
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
