import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/posts.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart' as auth;

class UsersPosts extends StatefulWidget {
  @override
  _UsersPostsState createState() => _UsersPostsState();
}

class _UsersPostsState extends State<UsersPosts> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  bool _isLoading = true;
  String _message = "";
  bool _error = false;
  late Dio dio;
  List<Post> _listOfPosts = List<Post>.empty(growable: true);

  /// ==============================
  /// Get users post favorites
  /// ==============================
  getUserPosts() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    Response response =
        await dio.get('/posts?author_id=$userId').catchError((onError) {
      print("TV - Error getting posts ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print(data[i]['title']);
            Post temPost = Post.fromJsonNoUser(data[i]);
            if (temPost.groupId == null) {
              _listOfPosts.add(temPost);
            }
            //_listOfPosts.add(Post.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  _updatePost(Post post, int status, int index) async {
    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    String url = CustomFunctions.url + "/posts/${post.id}";

    try {
      Map<String, String> datos = Map<String, String>();

      datos["data"] =
          '{"title": "${post.title}", "type": ${post.type}, "status": $status, "short_description": "${post.shortDescription}","content": "${post.content}", "date": "${post.date}", "category_id": "${post.categoryId}"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken();
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;

      request.send().then((response) async {
        if (response.statusCode == 200) {
          print("TV - POST UPDATED!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
            _listOfPosts[index].status = status;
          });

          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getUserPosts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "Tus Post creados",
                            style: Theme.of(context).textTheme.headline3,
                          ),
                        ),
                        _listOfPosts.isNotEmpty
                            ? _buildPostsFromGet(_listOfPosts)
                            : Text('¡No tienes post creados!'),
                      ],
                    ),
                  ),
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  Widget _buildPostsFromGet(List<Post> posts) {
    int i = -1;
    return Column(
      children: posts.map((e) {
        i++;
        return buildCardWithImage(context, e, i);
      }).toList(),
    );
  }

  Widget buildCardWithImage(BuildContext context, Post post, index) {
    return Column(
      children: [
        SizedBox(
          height: 10.0,
        ),
        Align(
          child: Container(
            height: 275,
            width: MediaQuery.of(context).size.width / 1.1,
            decoration: BoxDecoration(
              image: post.image != null
                  ? DecorationImage(
                      //image: ExactAssetImage(image),
                      image: NetworkImage(post.image),
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter)
                  : null,
              borderRadius: BorderRadius.circular(15.0),
              color: Theme.of(context).primaryColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.25),
                  spreadRadius: 3,
                  blurRadius: 3,
                  offset: Offset(2, 4), // changes position of shadow
                ),
              ],
            ),
            child: Stack(
              children: [
                Positioned(
                  bottom: 0,
                  width: MediaQuery.of(context).size.width / 1.1,
                  child: Container(
                    //color: Colors.white,
                    padding: EdgeInsets.all(10.0),
                    height: 120,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.5),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Flexible(
                              child: RichText(
                                overflow: TextOverflow.ellipsis,
                                strutStyle: StrutStyle(fontSize: 12.0),
                                text: TextSpan(
                                    style:
                                        Theme.of(context).textTheme.subtitle1,
                                    text: post.title),
                              ),
                            ),
                          ],
                        ),
                        // post.status == 1
                        //     ? TextButton(
                        //         style: TextButton.styleFrom(
                        //           backgroundColor: Colors.red,
                        //           primary: Colors.white,
                        //         ),
                        //         onPressed: () {
                        //           _updatePost(post, 2, index);
                        //         },
                        //         child: Text("Pausar"),
                        //       )
                        //     : TextButton(
                        //         onPressed: () {
                        //           _updatePost(post, 1, index);
                        //         },
                        //         child: Text("Publicar"),
                        //       ),
                        // Text(
                        //     'Tal vez tengas que reiniciar la app para ver los cambios en la página de inicio',
                        //     style: TextStyle(
                        //       fontSize: 10,
                        //       fontStyle: FontStyle.italic,
                        //     )),
                      ],
                    ),
                  ),
                ),
                Positioned(
                    right: 2.5,
                    bottom: 100,
                    //top: 5,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        /*border: Border.all(
                              color: Colors.black,
                              width: 0.1
                            ),*/
                        borderRadius: BorderRadius.circular(50.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.25),
                            spreadRadius: 2,
                            blurRadius: 2,
                            offset: Offset(1, 2), // changes position of shadow
                          ),
                        ],
                      ),
                      /* child: IconButton(
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EventPage(
                                        id: post.id,
                                        tile: post.title,
                                        image: post.image,
                                        type: 1,
                                      )));
                        },
                        //icon: Image.asset('assets/iconos/enviar.png'),

                      ) */
                    )),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
      ],
    );
  }
}
