import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ForgottenPassword extends StatefulWidget {
  @override
  _ForgottenPasswordState createState() => _ForgottenPasswordState();
}

class _ForgottenPasswordState extends State<ForgottenPassword> {
  final _forgottenKey = GlobalKey<FormState>(debugLabel: 'forgottenKey');
  final _scaffold = GlobalKey<ScaffoldState>();
  final _emailController = TextEditingController();
  bool _emailSent = false;

  @override
  void initState() {
    _setSplashSeen();
    super.initState();
  }

  _setSplashSeen() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('splashSeen', true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                //height: 70,
                color: Colors.white,
                child: Text(
                  "tuvcino",
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.center,
                ),
                /*Image.asset(
                  'assets/splash/tuvcino_logo_720_M.png',
                  height: 50.0,
                ),*/
              ),
              SizedBox(
                height: (MediaQuery.of(context).size.height * 0.20) -
                    (MediaQuery.of(context).padding.top + 70),
              ),
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.80,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15.0),
                        topLeft: Radius.circular(15.0))),
                child: Form(
                  key: _forgottenKey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 35.0,
                      ),
                      Text(
                        "¿Olvidaste tu contraseña?",
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                labelText: "Correo",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar tu correo electrónico";
                                }

                                Pattern pattern =
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                RegExp regex = RegExp(pattern.toString());

                                if (!regex.hasMatch(value)) {
                                  return "Asegurate de colocar un email válido";
                                }

                                return null;
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Theme.of(context)
                              .buttonTheme
                              .colorScheme!
                              .secondaryVariant,
                        ),
                        onPressed: _emailSent
                            ? null
                            : () async {
                                /*****************************************/
                                // Check current state of the login form
                                /*****************************************/
                                print(
                                    'Login Form Validation State = ${_forgottenKey.currentState!.validate()}');
                                if (_forgottenKey.currentState!.validate()) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                          content: Text("Enviando correo")));

                                  auth.FirebaseAuth instance =
                                      auth.FirebaseAuth.instance;

                                  try {
                                    setState(() {
                                      _emailSent = true;
                                    });
                                    instance.sendPasswordResetEmail(
                                        email: _emailController.text);
                                  } on auth.FirebaseAuthException catch (e) {
                                    print('Failed with error code: ${e.code}');
                                    print(e.message);
                                    String authError =
                                        "Error al enviar el correo, intenta nuevamente\n ${e.message}";
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        // return object of type Dialog
                                        return AlertDialog(
                                          title: Text("¡Error!"),
                                          content: Text(authError),
                                        );
                                      },
                                    );
                                    setState(() {
                                      _emailSent = false;
                                    });
                                  }
                                }
                              },
                        child: Text(
                          "Enviar correo",
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showBottomMessage(IconData icon, String message) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(25.0),
          color: Theme.of(context).accentColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
