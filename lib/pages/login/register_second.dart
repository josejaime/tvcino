import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/Rol.dart';
import 'package:tvcino/pages/login/login.dart';
import 'package:tvcino/pages/login/register_zip_code.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class RegisterSecondPage extends StatefulWidget {
  @override
  _RegisterSecondPageState createState() => _RegisterSecondPageState();
}

class _RegisterSecondPageState extends State<RegisterSecondPage> {
  String _selectedState = "Aguascalientes";
  String _selectedValue = "México";
  List<String> states = [];
  //final _addressController = TextEditingController();
  //final _postcodeController = TextEditingController();
  final _cityController = TextEditingController();
  final GlobalKey<FormState> _formRegisterSecondPart =
      GlobalKey<FormState>(debugLabel: 'formRegisterSecondPart');
  late SharedPreferences preferences;
  late auth.FirebaseAuth _instance;
  late auth.User _user;
  //
  late Dio dio;
  List _data = [];
  //Map<String, dynamic> _data;
  List<Rol> _listOfNeighborhoods = List<Rol>.empty(growable: true);

  @override
  void initState() {
    states = CustomFunctions.mexicanStates() as List<String>;
    _instance = auth.FirebaseAuth.instance;
    _user = _instance.currentUser as auth.User;

    if (!_user.emailVerified) {
      debugPrint("TV - Email Verification Sent");
      _user.sendEmailVerification();
    }

    _getPreferences();
    _getRoles();
    super.initState();
  }

  _getPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print("TV Retrived data");
    await preferences.setString('name', _user.displayName ?? '');
    print("TV User Name = ${preferences.getString('name')}");
    //print("TV User userame = ${preferences.getString('username')}");
    await preferences.setString('email', _user.email ?? '');
    print("TV User email  = ${preferences.getString('email')}");
    await preferences.setString('uuid', _user.uid);
    //print("TV User birthdate  = ${preferences.getString('birthdate')}");
    print("TV User UUID  = ${preferences.getString('uuid')}");
  }

  _getRoles() async {
    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    Response response = await dio.get('/roles');
    if (response.statusCode != 200) {
      print("TV DIO ERROR - Status Code ${response.statusCode}");
      print("TV DIO ERROR - Status Message ${response.statusMessage}");
    } else {
      print("DIO Status Code ${response.statusCode}");
      print("DIO - Response RAW Data");
      print(response.data);
      print("DIO Trying to convert data");
      //_data = jsonDecode(response.data.toString());
      _data = response.data;

      print("CONVERTED DATA ¿?");
      print("DATA is: ");
      //print(_data[0]);

      print("MAPPING is: ");
      for (int i = 0; i < _data.length; i++) {
        print(_data[i]['name']);
        //_listOfUsers.add(InterUser.fromJson(_data[i]));
        _listOfNeighborhoods.add(Rol.fromJson(_data[i]));
      }
      print("TV THE LIST OF NEIGHBORHOODS IS :");
      print(_listOfNeighborhoods);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                height: 70,
                color: Colors.white,
                child: Text(
                  "tuvcino",
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: (MediaQuery.of(context).size.height * 0.20) -
                    (MediaQuery.of(context).padding.top + 70),
              ),
              Container(
                width: double.infinity,
                //height: MediaQuery.of(context).size.height * 0.70,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0),
                    topLeft: Radius.circular(15.0),
                    bottomLeft: Radius.circular(15.0),
                  ),
                ),
                child: Form(
                  key: _formRegisterSecondPart,
                  child: Align(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.85,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 30.0,
                          ),
                          Text(
                            "¡Cuéntanos un poco más de ti!",
                            style: Theme.of(context).textTheme.headline2,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            children: [
                              Text(
                                'Selecciona tu país',
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 2.0,
                          ),
                          DropdownButton<String>(
                            value: _selectedValue,
                            isExpanded: true,
                            icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            onChanged: (String? newValue) {
                              setState(() {
                                _selectedValue = newValue as String;
                                if (newValue == "Canada") {
                                  states = CustomFunctions.canadaStates()
                                      as List<String>;
                                  _selectedState = states[0];
                                } else if (newValue == "Estados Unidos") {
                                  states = CustomFunctions.unitedStates()
                                      as List<String>;
                                  _selectedState = states[0];
                                } else {
                                  states = CustomFunctions.mexicanStates()
                                      as List<String>;
                                  _selectedState = states[0];
                                }
                              });
                              print('TV - Selected new value $_selectedValue');
                            },
                            items: <String>[
                              'Estados Unidos',
                              'México',
                              'Canada'
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            children: [
                              Text(
                                'Selecciona tu Estado',
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 2.0,
                          ),
                          DropdownButton<String>(
                            value: _selectedState,
                            isExpanded: true,
                            icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            onChanged: (String? newValue) {
                              setState(() {
                                _selectedState = newValue as String;
                              });
                              print('TV - Selected state $_selectedState');
                            },
                            items: states
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                          TextFormField(
                            controller: _cityController,
                            keyboardType: TextInputType.streetAddress,
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: "Ciudad",
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "* Debes ingresar una ciudad";
                              }
                              if (value.length < 4) {
                                return "* Intenta con un valor más largo";
                              }
                              return null;
                            },
                          ),
                          /* TextFormField(
                            //controller: _addressController,
                            //keyboardType: TextInputType.streetAddress,
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: "Calle y número",
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "* Debes ingresar una dirección";
                              }
                              if (value.length < 5) {
                                return "* Intenta con un valor más largo";
                              }
                              return null;
                            },
                          ),
                          TextFormField(
                            controller: _postcodeController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: "Código Postal",
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "* Debes ingresar un código";
                              }
                              if (value.length < 2) {
                                return "* Intenta con un valor más largo";
                              }
                              return null;
                            },
                          ), */
                          SizedBox(
                            height: 15.0,
                          ),
                          TextButton(
                            style: TextButton.styleFrom(
                              backgroundColor: Theme.of(context)
                                  .buttonTheme
                                  .colorScheme!
                                  .secondary,
                            ),
                            child: Text(
                              "Selecciona un vecindario",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            onPressed: () async {
                              /******************************************/
                              // Create a new Neighborhood
                              /******************************************/
                              if (_formRegisterSecondPart.currentState!
                                  .validate()) {
                                print("TV - Form Register Second Part Valid");
                                /******************************************/
                                // Save the preferences
                                // of the next of registered user
                                /******************************************/
                                preferences =
                                    await SharedPreferences.getInstance();
                                await preferences.setString(
                                    'country', _selectedValue);
                                await preferences.setString(
                                    'state', _selectedState);
                                await preferences.setString(
                                    'city', _cityController.text);

                                ///* String fullAddress = _addressController.text +
                                //    " " + */
                                String fullAddress = "N/A";
                                await preferences.setString(
                                    'address', fullAddress);
                                /******************************************/
                                // Lets get to page to register a new
                                // neighborhood
                                /******************************************/
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            RegisterZipCode()));
                                /* Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            RegisterCreateNeighborhood())); */
                              } else {
                                print(
                                    "TV - Error Something wrong with the Form");
                              }
                            },
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          /* TextButton(
                            child: Text(
                              "Quiero unirme a uno",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            onPressed: () async {
                              /******************************************/
                              // Register form validation
                              /******************************************/
                              /******************************************/
                              // Select Neighborhood
                              /******************************************/
                              if (_formRegisterSecondPart.currentState
                                  .validate()) {
                                print("TV - Form Register Second Part Valid");
                                /******************************************/
                                // Save the preferences
                                // of the next of registered user
                                /******************************************/
                                preferences =
                                    await SharedPreferences.getInstance();
                                await preferences.setString(
                                    'country', _selectedValue);
                                await preferences.setString(
                                    'state', _selectedState);
                                await preferences.setString(
                                    'city', _cityController.text);
                                String fullAddress = "N/A";
                                await preferences.setString(
                                    'address', fullAddress);
                                /******************************************/
                                // Lets get to page to select
                                // neighborhood
                                /******************************************/
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            RegisterSelectNeighborhood()));
                              } else {
                                print(
                                    "TV - Error Something wrong with the Form");
                              }
                            },
                          ), */
                          SizedBox(
                            height: 25.0,
                          ),
                          GestureDetector(
                            onTap: () async {
                              await _instance.signOut();
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginPage()));
                            },
                            child: Text(
                              "Cerrar sesión",
                              style: TextStyle(
                                fontSize: 16.0,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
