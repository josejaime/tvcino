import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/classes/neighborhood.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/home.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart' as auth;

class RegisterSelectNeighborhood extends StatefulWidget {
  @override
  _RegisterSelectNeighborhoodState createState() =>
      _RegisterSelectNeighborhoodState();
}

class _RegisterSelectNeighborhoodState
    extends State<RegisterSelectNeighborhood> {
  final _registerForm = GlobalKey<FormState>();
  late Timer timer;
  bool _isLoading = true;
  late Dio dio;
  List _data = [];
  //Map<String, dynamic> _data;
  List<Neighbordhood> _listOfNeighborhoods =
      List<Neighbordhood>.empty(growable: true);
  //===============================*/
  // Messages
  //===============================*/
  String _message = "";
  bool _error = false;
  //===============================*/
  // User Information to send
  //===============================*/
  String name = "";
  String username = "";
  String email = "";
  String uuid = "";
  String birthdate = "";
  String country = "";
  String state = "";
  String city = "";
  String address = "";
  //===============================*/
  // Firebase
  //===============================*/
  late auth.FirebaseAuth _instance;
  late auth.User _user;

  @override
  void initState() {
    _instance = auth.FirebaseAuth.instance;
    _user = _instance.currentUser as auth.User;
    _makeLoading();
    _getPreferences();
    super.initState();
  }

  _getPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print("TV Retrived data");
    print("TV User Name = ${preferences.getString('name')}");
    //print("TV User userame = ${preferences.getString('username')}");
    print("TV User email  = ${preferences.getString('email')}");
    print("TV User UUID  = ${preferences.getString('uuid')}");
    //print("TV User birthdate  = ${preferences.getString('birthdate')}");
    print("TV User country  = ${preferences.getString('country')}");
    print("TV User state  = ${preferences.getString('state')}");
    print("TV User city  = ${preferences.getString('city')}");
    print("TV User address  = ${preferences.getString('address')}");
  }

  @override
  void dispose() {
    if (timer != null && timer.isActive) {
      timer.cancel();
    }
    super.dispose();
  }

  _makeLoading() async {
    setState(() {
      _isLoading = true;
    });
    /* timer = new Timer(Duration(seconds: 3), () {
      setState(() {
        _isLoading = false;
      });
      print("Se Acabó el tiempo 10 segundos");
    }); */

    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    Response response = await dio.get('/neighborhoods?start=0&limit=10');
    if (response.statusCode != 200) {
      print("TV DIO ERROR - Status Code ${response.statusCode}");
      print("TV DIO ERROR - Status Message ${response.statusMessage}");

      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor regresa e intenta nuevamente";
      });
    } else {
      print("DIO Status Code ${response.statusCode}");
      print("DIO - Response RAW Data");
      print(response.data['items']);
      print("DIO Trying to convert data");
      //_data = jsonDecode(response.data.toString());
      _data = response.data['items'];

      print("CONVERTED DATA ¿?");
      print("DATA is: ");
      //print(_data[0]);

      print("MAPPING is: ");
      for (int i = 0; i < _data.length; i++) {
        print(_data[i]['name']);
        //_listOfUsers.add(InterUser.fromJson(_data[i]));
        _listOfNeighborhoods.add(Neighbordhood.fromJson(_data[i]));
      }
      print("TV THE LIST OF NEIGHBORHOODS IS :");
      print(_listOfNeighborhoods);
      //print("THE FIRST ONE IS: ${_listOfUsers[0].name}");

      setState(() {
        _isLoading = false;
        _error = false;
        _message = "";
      });
    }
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addUser() async {
    print("TV - ADD USER TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    //Random random = new Random();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    name = preferences.getString('name') as String;
    //int number = random.nextInt(100000);
    //username = preferences.getString('username');
    //username = "username\_$number";
    email = preferences.getString('email') as String;
    uuid = preferences.getString('uuid') as String;
    birthdate = preferences.getString('birthdate') as String;
    country = preferences.getString('country') as String;
    state = preferences.getString('state') as String;
    city = preferences.getString('city') as String;
    address = preferences.getString('address') as String;

    try {
      print("TV - ADD USER TRY");

      Map<String, dynamic> data = Map<String, dynamic>();
      Map<String, dynamic> user = Map<String, dynamic>();

      user['uid'] = uuid;
      user['name'] = name;
      //user['username'] = "username";
      user['email'] = email;
      //user['birthdate'] = '06-11-1965';
      user['phone_number'] = '-';
      //FIRST ROLE ID
      user['role_id'] = "462cbf38-0442-4829-97ca-78462960675d";
      //FIRST PACKAGE ID
      user['package_id'] = "40f91e35-f930-4d60-bbd1-052f0ba03181";
      user['country'] = country;
      user['active'] = true;
      user['state'] = state;
      user['city'] = city;
      user['address'] = address;

      data['data'] = user;

      print("TV - USER DATA :");
      print(user);
      print("TV - BODY DATA :");
      print(data);

      /* print("TV - ==================");
      //print(json.encode("data": json.encode(user)));
      print(jsonEncode(
          "data: {'uid': '12fsdfdsfds','name': 'Poncho','username': 'Poncho','birthdate': '06-11-1965','phone_number': '123456789','email': 'Poncho','address': 'Poncho','active': 'true','role_id': '462cbf38-0442-4829-97ca-78462960675d','package_id': '40f91e35-f930-4d60-bbd1-052f0ba03181','country': 'Poncho','state': 'Poncho','city': 'Poncho'}"));
      print("TV - =================="); */

      Map<String, String> datos = Map<String, String>();

      /* datos["data"] =
          '{"uid": "$uuid","name": "$name","username": "$username","birthdate": "06-11-1965","phone_number": "null","email": "$email","address": "$address","active": "true","role_id": "462cbf38-0442-4829-97ca-78462960675d","package_id": "40f91e35-f930-4d60-bbd1-052f0ba03181","country": "$country","state": "$state","city": "$city"}'; */
      datos["data"] =
          '{"uid": "$uuid","name": "$name","email": "$email", "phone_number": "-", "address": "$address","active": true,"role_id": "462cbf38-0442-4829-97ca-78462960675d","package_id": "40f91e35-f930-4d60-bbd1-052f0ba03181","country": "$country","state": "$state","city": "$city"}';

      String token = await _user.getIdToken();

      var url = CustomFunctions.url + '/users/';
      print("TV - ADD USER URL $url");

      var response = await http.post(Uri.parse(url),
          //headers: {"Content-Type": "multipart/form-data"},
          //headers: {"Content-Type": "application/json"},
          headers: {"Authorization": "$token"},
          body: datos);

      print('TV - ADD USER - Response status: ${response.statusCode}');
      print('TV - ADD USER - Response status: ${response.headers}');
      print('TV - ADD USER - Response status: ${response.body}');

      if (response.statusCode != 200) {
        print("TV HTTP ERROR - Status Code ${response.statusCode}");
        setState(() {
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _error = true;
          _isLoading = false;
        });
      } else {
        //============================================/
        /// 200 is everything ok
        //============================================/
        InterUser user;
        user = InterUser.fromJson(jsonDecode(response.body));
        await preferences.setBool('userCreated', true);
        await preferences.setString('userId', user.id);

        setState(() {
          _isLoading = false;
          _error = false;
          _message = "";
        });

        print("TV - USER CREATED ALL OK");
        print("TV - NOW LETS ADD THE NEIGHBORHOOD");
        return true;
      }
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = false;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = false;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = false;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    }
    return false;
  }

  /// =============================================
  /// Function
  /// Add user to a Neighborhood in the TVCINO DB
  /// =============================================
  _addUserToNeighborhood(String neighborhoodId) async {
    print("TV - ADD USER TO NEIGHBORHOOD ID $neighborhoodId");

    bool added = await _addUser();

    if (added == false) {
      print("TV - ADDED FAILED");
      return false;
    } else {
      print("TV - ADDED WENT TRUE");

      SharedPreferences preferences = await SharedPreferences.getInstance();
      String userId = preferences.getString('userId') as String;
      setState(() {
        _error = false;
        _isLoading = true;
        _message = "";
      });

      try {
        var url = CustomFunctions.url +
            '/neighborhoods/$neighborhoodId/users/$userId';

        print("TV - URL - $url");
        print("TV - Neighborhood ID - $neighborhoodId");
        print("TV - User ID - $userId");
        String token = await _user.getIdToken();

        var response = await http
            .post(Uri.parse(url), headers: {'Authorization': '$token'});

        print("Response headers" + response.headers.toString());
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');

        if (response.statusCode != 200) {
          print("TV HTTP ERROR - Status Code ${response.statusCode}");
          setState(() {
            _message =
                "Hubo un error en el servidor regresa e intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        } else {
          /******************************************/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          /******************************************/
          await preferences.setString('neighborhoodId', neighborhoodId);
          await preferences.setBool('neighborhoodSet', true);
          /* _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Vecindario creado, vamos a la página principal',
            ),
          ),
        );
        */
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        }
      } on SocketException {
        print('TV EXCEPTION - No Internet connection');
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - No Internet connection";
        });
      } on HttpException {
        print("TV EXCEPTION - Couldn't find the post");
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - Couldn't find the post";
        });
      } on FormatException {
        print("TV EXCEPTION - Bad response format");
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - Bad response format";
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: _isLoading
            ? Center(
                child: TvcinoCircularProgressBar(
                  color: Colors.white,
                ),
              )
            : _error
                ? Center(
                    child: Column(
                      children: [
                        Text(_message),
                      ],
                    ),
                  )
                : SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: double.infinity,
                          height: 70,
                          color: Colors.white,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: IconButton(
                                  icon: Icon(Icons.arrow_back),
                                  color: Theme.of(context).primaryColor,
                                  iconSize: 28.0,
                                  onPressed: () => Navigator.pop(context),
                                ),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "tuvcino",
                                  style: Theme.of(context).textTheme.headline1,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: (MediaQuery.of(context).size.height * 0.20) -
                              (MediaQuery.of(context).padding.top + 70),
                        ),
                        Container(
                          width: double.infinity,
                          //height: MediaQuery.of(context).size.height * 0.80,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(15.0),
                              topLeft: Radius.circular(15.0),
                            ),
                          ),
                          child: Form(
                            key: _registerForm,
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 35.0,
                                ),
                                Text(
                                  "¡Selecciona tu vecindario!",
                                  style: Theme.of(context).textTheme.headline2,
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                _listOfNeighborhoods.isEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          'No hay vecindarios',
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline3,
                                        ),
                                      )
                                    : Offstage(),
                                _listOfNeighborhoods != null
                                    ? SingleChildScrollView(
                                        child: buildNeighborhoods(
                                            _listOfNeighborhoods))
                                    : Row(),
                                SizedBox(
                                  height: 15.0,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
      ),
    );
  }

  /* Widget buildUsers(List<InterUser> interUsers) {
    return Column(
      children: interUsers.map((e) {
        //return Text("${e.name}");
        //return buildNeighborhoodContainer(context, e);
      }).toList(),
    );
  } */

  Widget buildNeighborhoods(List<Neighbordhood> neighborhoods) {
    return Column(
      children: neighborhoods.map((e) {
        //return Text("${e.name}");
        return buildNeighborhoodContainer(context, e);
      }).toList(),
    );
  }

  Widget buildNeighborhoodContainer(
      BuildContext context, Neighbordhood neighborhood) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.9,
          child: TextButton(
            onPressed: () {
              print("TV - SELECT NEIGHBORHOOD");
              _showEnterConfirmation(context, neighborhood.id);
            },
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                        child: Container(
                          width: double.infinity,
                          child: Text(
                            neighborhood.name,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .headline4!
                                  .fontSize,
                              fontFamily: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .fontFamily,
                            ),
                          ),
                        ),
                      ),
                      Divider(),
                      Text(
                        "${neighborhood.country} \n${neighborhood.state}",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize:
                              Theme.of(context).textTheme.headline6!.fontSize,
                          fontFamily:
                              Theme.of(context).textTheme.headline6!.fontFamily,
                        ),
                        //style: Theme.of(context).textTheme.headline5.fontFamily,
                      ),
                    ],
                  ),
                ),
                Icon(Icons.chevron_right),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 2.5,
        )
      ],
    );
  }

  void _showEnterConfirmation(BuildContext context, String id) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Salir"),
          content: Text("¿Quieres unirte a este vecindario?"),
          actions: [
            TextButton(
              child: Text('Cancelar'),
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
              ),
              onPressed: () {
                //Just dismiss the alert
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Sí, Continuar'),
              onPressed: () async {
                _addUserToNeighborhood(id);
              },
            ),
          ],
        );
      },
    );
  }
}
