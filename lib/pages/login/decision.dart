import 'package:flutter/material.dart';
import 'package:tvcino/pages/login/create_neighborhood.dart';
import 'package:tvcino/pages/login/select_neighborhood.dart';

class DecisionPage extends StatefulWidget {
  @override
  _DecisionPageState createState() => _DecisionPageState();
}

class _DecisionPageState extends State<DecisionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: double.infinity,
              height: 70,
              color: Colors.white,
              child: Text(
                "tuvcino",
                style: Theme.of(context).textTheme.headline1,
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: (MediaQuery.of(context).size.height * 0.30) -
                  (MediaQuery.of(context).padding.top + 70),
              /* child: Center(
                child: Text(
                  "¿Ya tienes vecindario?",
                  style: TextStyle(color: Colors.white, fontSize: 28.0),
                  textAlign: TextAlign.center,
                ),
              ), */
            ),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.70,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(15.0),
                  topLeft: Radius.circular(15.0),
                ),
              ),
              child: Form(
                //key: _registerForm,
                child: Column(
                  children: [
                    SizedBox(
                      height: 35.0,
                    ),
                    Text(
                      "¿Ya tienes vecindario?",
                      style: Theme.of(context).textTheme.headline2,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    TextButton(
                      child: Text(
                        "Crear un vecindario",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: () async {
                        /******************************************/
                        // Create a new Neighborhood
                        /******************************************/
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    RegisterCreateNeighborhood()));
                      },
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    TextButton(
                      child: Text(
                        "Quiero unirme a uno",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      onPressed: () async {
                        /******************************************/
                        // Register form validation
                        /******************************************/
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    RegisterSelectNeighborhood()));
                      },
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
