import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/classes/neighborhood.dart';
import 'package:tvcino/pages/home.dart';
import 'package:tvcino/pages/login/login.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:geocoding/geocoding.dart' as geo;
import 'package:http/http.dart' as http;

class RegisterZipCode extends StatefulWidget {
  @override
  _RegisterZipCodeState createState() => _RegisterZipCodeState();
}

class _RegisterZipCodeState extends State<RegisterZipCode> {
  final GlobalKey<FormState> _formRegisterZipCode =
      GlobalKey<FormState>(debugLabel: 'formRegisterZipCode');
  final _zipController = TextEditingController();
  late auth.FirebaseAuth _instance;
  late auth.User _user;
  String _selectedValue = "México";
  String _calculatedZipCode = "";
  List data = [];
  List<Neighbordhood> _listOfNeighborhoods =
      List<Neighbordhood>.empty(growable: true);
  String _message = "";
  bool _isLoading = false;
  bool _error = false;
  late Dio dio;

  @override
  void initState() {
    _instance = auth.FirebaseAuth.instance;
    _zipController.text = _calculatedZipCode;
    _user = _instance.currentUser as auth.User;
    _getCurrentPosition();
    //_getNeighborhoods();
    super.initState();
  }

  _getCurrentPosition() async {
    Position position = await _determinePosition();
    print('Lat = ${position.latitude}');
    print('Long = ${position.longitude}');
    List<geo.Placemark> placemarks = await geo.placemarkFromCoordinates(
        position.latitude, position.longitude,
        localeIdentifier: 'es_MX');
    print('TV - _getCurrentPosition GEOLOCATION');
    print(placemarks);
    if (placemarks.isNotEmpty) {
      setState(() {
        if (placemarks.first.isoCountryCode == 'MX') {
          _selectedValue = 'México';
        } else if (placemarks.first.isoCountryCode == 'US') {
          _selectedValue = 'Estados Unidos';
        }
        _calculatedZipCode = placemarks.first.postalCode as String;
        _zipController.text = _calculatedZipCode;
      });
      SharedPreferences preferences = await SharedPreferences.getInstance();
      // await preferences.setString('country', placemarks.first.country);
      // await preferences.setString('state', placemarks.first.locality);
      // await preferences.setString('city', placemarks.first.administrativeArea);
      await preferences.setString('address', 'N/A');
      _getNeighborhoods(_calculatedZipCode);
    }
  }

  _getNeighborhoods(String zipcode) async {
    setState(() {
      _isLoading = true;
      _error = false;
      _listOfNeighborhoods.clear();
    });
    dio = CustomFunctions.getDio();
    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    try {
      Response response = await dio.get(
          '/neighborhoods?country=$_selectedValue&zipcode=$zipcode&start=0&limit=100');
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        //_data = jsonDecode(response.data.toString());
        data = response.data['items'];

        print("CONVERTED DATA ¿?");
        print("DATA is: ");
        //print(data[0]);

        print("MAPPING is: ");
        for (int i = 0; i < data.length; i++) {
          print(data[i]['name']);
          //_listOfUsers.add(InterUser.fromJson(data[i]));
          _listOfNeighborhoods.add(Neighbordhood.fromJson(data[i]));
        }
        print("TV THE LIST OF NEIGHBORHOODS IS :");
        print(_listOfNeighborhoods);
        //print("THE FIRST ONE IS: ${_listOfUsers[0].name}");

        setState(() {
          _isLoading = false;
          _error = false;
          _message = "";
        });
      }
    } on DioError catch (error) {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = error.toString();
      });
    }
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                height: 70,
                color: Colors.white,
                child: Text(
                  "tuvcino",
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: (MediaQuery.of(context).size.height * 0.20) -
                    (MediaQuery.of(context).padding.top + 70),
              ),
              Container(
                width: double.infinity,
                //height: MediaQuery.of(context).size.height * 0.70,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0),
                    topLeft: Radius.circular(15.0),
                    bottomLeft: Radius.circular(15.0),
                  ),
                ),
                child: Form(
                  key: _formRegisterZipCode,
                  child: Align(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.85,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 30.0,
                          ),
                          Text(
                            "Primero necesitamos tu ZIPCODE",
                            style: Theme.of(context).textTheme.headline2,
                            textAlign: TextAlign.center,
                          ),
                          DropdownButton<String>(
                            value: _selectedValue,
                            isExpanded: true,
                            icon: Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            onChanged: (String? newValue) {
                              print('TV - Selected new value $newValue');
                              setState(() {
                                _selectedValue = newValue as String;
                              });
                            },
                            items: <String>[
                              'Estados Unidos',
                              'México',
                              'Canada'
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                          TextFormField(
                            controller: _zipController,
                            keyboardType: TextInputType.streetAddress,
                            decoration: InputDecoration(
                              isDense: true,
                              labelText: "ZIPCODE",
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "* Debes ingresar una ciudad";
                              }
                              if (value.length < 3) {
                                return "* Intenta con un valor más largo";
                              }
                              return null;
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          TextButton(
                            child: Text(
                              "Busca un vecindario",
                              style: TextStyle(fontSize: 20.0),
                            ),
                            onPressed: () async {
                              /******************************************/
                              // Search ZIPCODE Neighborhoods
                              /******************************************/
                              _getNeighborhoods(_zipController.text);
                            },
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Row(
                            children: [
                              Text(
                                'Selecciona tu vecindario',
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                          _listOfNeighborhoods.isEmpty
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'No hay vecindarios',
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline3,
                                  ),
                                )
                              : Offstage(),
                          _isLoading
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        CircularProgressIndicator(),
                                        SizedBox(
                                          height: 15.0,
                                        ),
                                        Text("Cargando..."),
                                      ],
                                    ),
                                  ],
                                )
                              : Offstage(),
                          //_error ? Text(_message) : Row(),
                          _listOfNeighborhoods != null
                              ? SingleChildScrollView(
                                  child:
                                      buildNeighborhoods(_listOfNeighborhoods))
                              : Row(),
                          SizedBox(
                            height: 15.0,
                          ),
                          GestureDetector(
                            onTap: () async {
                              await _instance.signOut();
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => LoginPage()));
                            },
                            child: Text(
                              "Cerrar sesión",
                              style: TextStyle(
                                fontSize: 16.0,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 25.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildNeighborhoods(List<Neighbordhood> neighborhoods) {
    int i = -1;
    return Column(
      children: neighborhoods.map((e) {
        //return Text("${e.name}");
        i++;
        return buildNeighborhoodContainer(context, e, i);
      }).toList(),
    );
  }

  Widget buildNeighborhoodContainer(
      BuildContext context, Neighbordhood neighborhood, int index) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            _showEnterConfirmation(context, neighborhood.id, neighborhood);
          },
          child: Material(
            borderRadius: BorderRadius.circular(5),
            elevation: 2.0,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              padding: EdgeInsets.all(7.5),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Align(
                          child: Container(
                            width: double.infinity,
                            child: Text(
                              neighborhood.name,
                              style: TextStyle(
                                //color: Colors.white,
                                fontSize: Theme.of(context)
                                    .textTheme
                                    .headline4!
                                    .fontSize,
                                fontFamily: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .fontFamily,
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          endIndent: 20,
                        ),
                        Text(
                          "${neighborhood.country} \n${neighborhood.state}",
                          style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize:
                                Theme.of(context).textTheme.headline6!.fontSize,
                            fontFamily: Theme.of(context)
                                .textTheme
                                .headline6!
                                .fontFamily,
                          ),
                          //style: Theme.of(context).textTheme.headline5.fontFamily,
                        ),
                      ],
                    ),
                  ),
                  Icon(Icons.chevron_right),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        )
      ],
    );
  }

  void _showEnterConfirmation(
      BuildContext context, String id, Neighbordhood neighbordhood) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Confirmar"),
          content: Text("¿Quieres unirte a este vecindario?"),
          actions: [
            TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.grey,
              ),
              child: Text('Cancelar'),
              //color: Theme.of(context).buttonTheme.colorScheme.secondary,
              onPressed: () {
                //Just dismiss the alert
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Sí, Continuar'),
              onPressed: () async {
                bool result = await _addUserToNeighborhood(id, neighbordhood);

                if (result) {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => HomePage()));
                } else {
                  showBottomMessage(Icons.dangerous,
                      "Hubo un error al registrar tu usuario en el vecindario ¡intenta nuevamente!");
                }
              },
            ),
          ],
        );
      },
    );
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addUser(String neighborhoodId) async {
    print("TV - ADD USER TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });
    //Random random = new Random();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String name = preferences.getString('name') as String;
    //int number = random.nextInt(100000);
    //username = preferences.getString('username');
    //username = "username\_$number";
    String email = preferences.getString('email') as String;
    String uuid = preferences.getString('uuid') as String;
    //String birthdate = preferences.getString('birthdate');
    String country = preferences.getString('country') as String;
    String state = preferences.getString('state') as String;
    String city = preferences.getString('city') as String;
    String address = preferences.getString('address') as String;

    try {
      print("TV - ADD USER TRY");

      Map<String, dynamic> data = Map<String, dynamic>();
      Map<String, dynamic> user = Map<String, dynamic>();

      user['uid'] = uuid;
      user['name'] = name;
      //user['username'] = "username";
      user['email'] = email;
      //user['birthdate'] = '06-11-1965';
      user['phone_number'] = '-';
      //FIRST ROLE ID
      user['role_id'] = "462cbf38-0442-4829-97ca-78462960675d";
      //FIRST PACKAGE ID
      user['package_id'] = "40f91e35-f930-4d60-bbd1-052f0ba03181";
      user['country'] = country;
      user['active'] = true;
      user['state'] = state;
      user['city'] = city;
      user['address'] = address;

      data['data'] = user;

      print("TV - USER DATA :");
      print(user);
      print("TV - BODY DATA :");
      print(data);

      Map<String, String> datos = Map<String, String>();

      datos["data"] =
          '{"uid": "$uuid","name": "$name","email": "$email", "phone_number": "-", "address": "$address","active": true,"role_id": "462cbf38-0442-4829-97ca-78462960675d","package_id": "40f91e35-f930-4d60-bbd1-052f0ba03181","country": "$country","state": "$state","city": "$city", "neighborhood_id": "$neighborhoodId"}';

      String token = await _user.getIdToken();

      var url = CustomFunctions.url + '/users/';
      print("TV - ADD USER URL $url");

      var response = await http.post(Uri.parse(url),
          //headers: {"Content-Type": "multipart/form-data"},
          //headers: {"Content-Type": "application/json"},
          headers: {"Authorization": "$token"},
          body: datos);

      print('TV - ADD USER - Response status: ${response.statusCode}');
      print('TV - ADD USER - Response status: ${response.headers}');
      print('TV - ADD USER - Response status: ${response.body}');

      if (response.statusCode != 200) {
        print("TV HTTP ERROR - Status Code ${response.statusCode}");
        setState(() {
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
          _error = true;
          _isLoading = false;
        });
      } else {
        //============================================/
        /// 200 is everything ok
        //============================================/
        InterUser user;
        user = InterUser.fromJson(jsonDecode(response.body));
        await preferences.setBool('userCreated', true);
        await preferences.setString('userId', user.id);

        setState(() {
          _isLoading = false;
          _error = false;
          _message = "";
        });

        print("TV - USER CREATED ALL OK");
        print("TV - NOW LETS ADD THE NEIGHBORHOOD");
        return true;
      }
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = false;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
        showBottomMessage(Icons.dangerous, _message);
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = false;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the server action";
        showBottomMessage(Icons.dangerous, _message);
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = false;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
        showBottomMessage(Icons.dangerous, _message);
      });
      return false;
    }
    return false;
  }

  /// =============================================
  /// Function
  /// Add user to a Neighborhood in the TVCINO DB
  /// =============================================
  _addUserToNeighborhood(
      String neighborhoodId, Neighbordhood neighbordhood) async {
    print("TV - ADD USER TO NEIGHBORHOOD ID $neighborhoodId");

    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('country', neighbordhood.country);
    await preferences.setString('state', neighbordhood.state);
    await preferences.setString('city', neighbordhood.city);

    bool added = await _addUser(neighborhoodId);

    if (added == false) {
      print("TV - ADDED FAILED");
      return false;
    } else {
      print("TV - ADDED WENT TRUE");

      String userId = preferences.getString('userId') as String;
      setState(() {
        _error = false;
        _isLoading = true;
        _message = "";
      });

      try {
        var url = CustomFunctions.url +
            '/neighborhoods/$neighborhoodId/users/$userId';

        print("TV - URL - $url");
        print("TV - Neighborhood ID - $neighborhoodId");
        print("TV - User ID - $userId");
        String token = await _user.getIdToken();

        var response = await http
            .post(Uri.parse(url), headers: {'Authorization': '$token'});

        print("Response headers" + response.headers.toString());
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');

        if (response.statusCode != 200) {
          print("TV HTTP ERROR - Status Code ${response.statusCode}");
          setState(() {
            _message =
                "Hubo un error en el servidor regresa e intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        } else {
          /******************************************/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          /******************************************/
          print(
              "TV - Saving Preferences Country ${neighbordhood.country}, state ${neighbordhood.state}, city ${neighbordhood.city}");
          await preferences.setString('neighborhoodId', neighborhoodId);
          await preferences.setBool('neighborhoodSet', true);

          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          /* Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage())); */
          return true;
        }
      } on SocketException {
        print('TV EXCEPTION - No Internet connection');
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - No Internet connection";
        });
        return false;
      } on HttpException {
        print("TV EXCEPTION - Couldn't find the post");
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - Couldn't find the post";
        });
        return false;
      } on FormatException {
        print("TV EXCEPTION - Bad response format");
        setState(() {
          _error = false;
          _isLoading = false;
          _message = "TV EXCEPTION - Bad response format";
        });
        return false;
      }
    }
  }

  showBottomMessage(IconData icon, String message) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(25.0),
          color: Theme.of(context).accentColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
