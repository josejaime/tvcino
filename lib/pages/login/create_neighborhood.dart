import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/pages/home.dart';
import 'package:tvcino/utils/custom_functions.dart';

class RegisterCreateNeighborhood extends StatefulWidget {
  @override
  _RegisterCreateNeighborhoodState createState() =>
      _RegisterCreateNeighborhoodState();
}

class _RegisterCreateNeighborhoodState
    extends State<RegisterCreateNeighborhood> {
  //////////////////////////////////////////////////
  /// Form key and text controllers
  //////////////////////////////////////////////////
  final _createNeighborhoodForm = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _stateController = TextEditingController();
  final _cityController = TextEditingController();
  final _countryController = TextEditingController();
  // Scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  // Select value
  String _selectedValue = "México";

  ///===============================================
  /// DIO Variables
  ///===============================================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late InterUser user;

  ///===============================================
  /// DIO Variables
  ///===============================================
  late Dio dio;
  bool _isLoading = false;
  //bool _error = false;
  //String _message = "";

  ///===============================================
  /// TO SEND USER
  ///===============================================
  String name = "";
  String username = "";
  String email = "";
  String uuid = "";
  String birthdate = "";
  String country = "";
  String state = "";
  String city = "";
  String address = "";

  @override
  void initState() {
    _getPreferences();
    _user = instance.currentUser as auth.User;
    super.initState();
  }

  _getPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    print("TV Retrived data");
    print("TV User Name = ${preferences.getString('name')}");
    //print("TV User userame = ${preferences.getString('username')}");
    print("TV User uuid = ${preferences.getString('uuid')}");
    print("TV User email  = ${preferences.getString('email')}");
    //print("TV User birthdate  = ${preferences.getString('birthdate')}");
    print("TV User country  = ${preferences.getString('country')}");
    print("TV User state  = ${preferences.getString('state')}");
    print("TV User city  = ${preferences.getString('city')}");
    print("TV User address  = ${preferences.getString('address')}");

    setState(() {
      _cityController.text = preferences.getString('city') as String;
      _stateController.text = preferences.getString('state') as String;
      _countryController.text = preferences.getString('country') as String;
      _selectedValue = preferences.getString('country') as String;
    });
  }

  _postNeighborhood() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    name = preferences.getString('name') as String;
    uuid = preferences.getString('uuid') as String;
    setState(() {
      //_error = false;
      _isLoading = true;
      //_message = "";
    });

    bool addedUser = await _addUser();
    //bool addedUser = true;

    if (addedUser) {
      String userId = preferences.getString('userId') as String;

      try {
        Map<String, dynamic> neighborhood = Map<String, dynamic>();

        neighborhood["name"] = _nameController.text;
        //neighborhood["user_id"] = user.id;
        neighborhood["user_id"] = preferences.getString('userId') as String;
        neighborhood["country"] = _selectedValue;
        neighborhood["state"] = _stateController.text;
        neighborhood["city"] = _cityController.text;
        neighborhood["active"] = true;

        print("TV - THE NEIGHBORHOOD IS:");
        print(neighborhood);
        String token = await _user.getIdToken();

        var url = CustomFunctions.url + '/neighborhoods/';
        Uri uri = Uri(
          path: url,
        );
        var response = await http.post(uri,
            //headers: {"Content-Type": "application/json"},
            headers: {"Authorization": "$token"},
            body: jsonEncode(neighborhood));

        print("Response headers" + response.headers.toString());
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');

        if (response.statusCode != 200) {
          print("TV HTTP ERROR - Status Code ${response.statusCode}");
          setState(() {
            //_message =
            //"Hubo un error en el servidor regresa e intenta nuevamente";
            //_error = true;
            _isLoading = false;
          });
        } else {
          Map<String, dynamic> result = jsonDecode(response.body);

          print('THE RESULT IS:');
          print(result);
          print('THE name is :');
          print(result['name']);
          print('THE id is :');
          print(result['id']);

          //_scaffoldKey.currentState.showSnackBar(
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(
                'Vecindario creado, vamos a la página principal',
              ),
            ),
          );

          /******************************************/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          /******************************************/
          await preferences.setString('neighborhoodId', result['id']);
          //preferences.setBool('neighborhoodSet', true);

          /******************************************/
          /// ADD USER TO NEIGHBORHOOD
          /// Set preferences neighborhoodSet = true
          /******************************************/
          var url = CustomFunctions.url +
              '/neighborhoods/${result["id"]}/users/$userId';

          print("TV - URL - $url");
          print("TV - Neighborhood ID - ${result["id"]}");
          print("TV - User ID - $userId");
          var response2 = await http
              .post(Uri.parse(url), headers: {'Authorization': '$token'});

          print("Response2 headers" + response2.headers.toString());
          print('Response2 status: ${response2.statusCode}');
          print('Response2 body: ${response2.body}');

          if (response2.statusCode != 200) {
            print("TV HTTP ERROR - Status Code ${response2.statusCode}");
            setState(() {
              //_message =
              //"Hubo un error en el servidor regresa e intenta nuevamente";
              //_error = true;
              _isLoading = false;
            });
          } else {
            /******************************************/
            /// 200 is everything ok
            /// Set preferences neighborhoodSet = true
            /******************************************/
            await preferences.setString('neighborhoodId', result["id"]);
            await preferences.setBool('neighborhoodSet', true);
            setState(() {
              _isLoading = false;
              //_error = false;
              //_message = "";
            });

            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => HomePage()));
          }
        }
      } on SocketException {
        print('TV EXCEPTION - No Internet connection');
        setState(() {
          //_error = false;
          _isLoading = false;
          //_message = "TV EXCEPTION - No Internet connection";
        });
      } on HttpException {
        print("TV EXCEPTION - Couldn't find the post");
        setState(() {
          //_error = false;
          _isLoading = false;
          //_message = "TV EXCEPTION - Couldn't find the post";
        });
      } on FormatException {
        print("TV EXCEPTION - Bad response format");
        setState(() {
          //_error = false;
          _isLoading = false;
          //_message = "TV EXCEPTION - Bad response format";
        });
      }
    } else {
      print("TV - USER NOT ADDED, SKIPING CREATE NEIGHBORHOOD");
    }
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addUser() async {
    print("TV - ADD USER TO DB");

    setState(() {
      //_error = false;
      _isLoading = true;
      //_message = "";
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    Random random = new Random();
    name = preferences.getString('name') as String;
    //username = preferences.getString('username');
    int number = random.nextInt(1000000);
    username = 'username_$number';
    email = preferences.getString('email') as String;
    uuid = preferences.getString('uuid') as String;
    //birthdate = preferences.getString('birthdate');
    birthdate = '06-11-1965';
    country = preferences.getString('country') as String;
    state = preferences.getString('state') as String;
    city = preferences.getString('city') as String;
    address = preferences.getString('address') as String;

    try {
      print("TV - ADD USER TRY");

      print("TV - ==================");
      Map<String, String> datos = Map<String, String>();

      /* datos["data"] =
          '{"uid": "124fsdfdsfds","name": "Poncho","username": "Poncho3","birthdate": "06-11-1965","phone_number": "123456789","email": "Poncho","address": "Poncho","active": "true","role_id": "462cbf38-0442-4829-97ca-78462960675d","package_id": "40f91e35-f930-4d60-bbd1-052f0ba03181","country": "Poncho","state": "Poncho","city": "Poncho"}'; */

      /* datos["data"] =
          '{"uid": "$uuid","name": "$name","username": "$username","birthdate": "null","phone_number": "null","email": "$email","address": "$address","active": "true","role_id": "462cbf38-0442-4829-97ca-78462960675d","package_id": "40f91e35-f930-4d60-bbd1-052f0ba03181","country": "$country","state": "$state","city": "$city"}'; */

      datos["data"] =
          '{"uid": "$uuid","name": "$name","email": "$email","phone_number":"","address": "$address","active": true,"role_id": "462cbf38-0442-4829-97ca-78462960675d","package_id": "40f91e35-f930-4d60-bbd1-052f0ba03181","country": "$country","state": "$state","city": "$city"}';

      String token = await _user.getIdToken();

      var url = CustomFunctions.url + '/users/';
      print("TV - ADD USER URL $url");
      var response = await http.post(Uri.parse(url),
          //headers: {"Content-Type": "multipart/form-data"},
          //headers: {"Content-Type": "application/json"},
          headers: {"Authorization": "$token"},
          body: datos);

      print('TV - ADD USER - Response status: ${response.statusCode}');
      print('TV - ADD USER - Response status: ${response.headers}');
      print('TV - ADD USER - Response status: ${response.body}');

      if (response.statusCode != 200) {
        print("TV HTTP ERROR - Status Code ${response.statusCode}");
        setState(() {
          //_message =
          //"Hubo un error en el servidor regresa e intenta nuevamente";
          //_error = true;
          _isLoading = false;
        });
      } else {
        //============================================/
        /// 200 is everything ok
        /// Set preferences neighborhoodSet = true
        //============================================/
        user = InterUser.fromJson(jsonDecode(response.body));
        await preferences.setBool('userCreated', true);
        await preferences.setString('userId', user.id);

        setState(() {
          _isLoading = false;
          //_error = false;
          //_message = "";
        });

        print("TV - USER CREATED ALL OK");
        print("TV - NOW LETS ADD THE NEIGHBORHOOD");
        return true;
      }
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        //_error = false;
        _isLoading = false;
        //_message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        //_error = false;
        _isLoading = false;
        //_message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        //_error = false;
        _isLoading = false;
        //_message = "TV EXCEPTION - Bad response format";
      });
      return false;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      key: _scaffoldKey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                height: 70,
                color: Colors.white,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: IconButton(
                        icon: Icon(Icons.arrow_back),
                        color: Theme.of(context).primaryColor,
                        iconSize: 28.0,
                        onPressed: () => Navigator.pop(context),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        "tuvcino",
                        style: Theme.of(context).textTheme.headline1,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: (MediaQuery.of(context).size.height * 0.20) -
                    (MediaQuery.of(context).padding.top + 70),
              ),
              Container(
                width: double.infinity,
                //height: MediaQuery.of(context).size.height * 0.70,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15.0),
                    topLeft: Radius.circular(15.0),
                  ),
                ),
                child: Form(
                  key: _createNeighborhoodForm,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 35.0,
                      ),
                      Text(
                        "¡Crea aquí tu vecindario!",
                        style: Theme.of(context).primaryTextTheme.headline3,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          children: [
                            Divider(),
                            Row(
                              children: [
                                Text(
                                  'Cómo identificas a tu vecindario',
                                  textAlign: TextAlign.left,
                                  style: Theme.of(context).textTheme.headline5,
                                ),
                              ],
                            ),
                            TextFormField(
                              controller: _nameController,
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                labelText: "Nombre del vecindario",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar un nombre";
                                }
                                if (value.length < 4) {
                                  return "* Intenta con un nombre más largo";
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Row(
                              children: [
                                Text(
                                  'Selecciona tu país',
                                  textAlign: TextAlign.left,
                                  style: Theme.of(context).textTheme.headline5,
                                ),
                              ],
                            ),
                            DropdownButton<String>(
                              value: _selectedValue,
                              isExpanded: true,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              //style: TextStyle(color: Colors.deepPurple),
                              /*underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),*/
                              onChanged: (String? newValue) {
                                setState(() {
                                  _selectedValue = newValue as String;
                                });
                              },
                              items: <String>[
                                'Estados Unidos',
                                'México',
                                'Canada'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            TextFormField(
                              controller: _stateController,
                              decoration: InputDecoration(
                                labelText: "Estado",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar tu estado";
                                }
                                if (value.length < 3) {
                                  return "* Intenta con mínimo 3 caracteres";
                                }

                                return null;
                              },
                            ),
                            TextFormField(
                              controller: _cityController,
                              decoration: InputDecoration(
                                labelText: "Ciudad",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar una ciudad";
                                }
                                if (value.length < 3) {
                                  return "* Intenta con mínimo 3 caracteres";
                                }
                                return null;
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      TextButton(
                        child: Text(
                          "¡Comenzar!",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        onPressed: _isLoading
                            ? null
                            : () async {
                                /******************************************/
                                // Register form validation
                                /******************************************/
                                print(
                                    'TV - Create Neighborhood Form Validation State = ${_createNeighborhoodForm.currentState!.validate()}');
                                if (_createNeighborhoodForm.currentState!
                                    .validate()) {
                                  print(
                                      "TV - Ready to send Create Neighborhood POST");
                                  if (_isLoading == false) {
                                    _postNeighborhood();
                                  }
                                } else {
                                  print("TV - NOT Ready to send");
                                }
                              },
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
