import 'package:firebase_auth/firebase_auth.dart' as auth;
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/pages/home.dart';
import 'package:tvcino/pages/login/forgotten_password.dart';
import 'package:tvcino/pages/login/register.dart';
import 'package:dio/dio.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _loginForm = GlobalKey<FormState>();
  final _scaffold = GlobalKey<ScaffoldState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool _sendingUser = false;
  bool _viewPassword = false;

  /// Firebase
  //FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  late Dio dio;

  @override
  void initState() {
    _setSplashSeen();
    super.initState();
    // _firebaseMessaging.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     setState(() {
    //       print("Message on Message");
    //       //_message = '$message';
    //     });
    //   },
    //   onLaunch: (message) async {
    //     setState(() {
    //       print("Message on Launch");
    //       //_message = '$message';
    //     });
    //   },
    //   onResume: (message) async {
    //     setState(() {
    //       print("Message on Resume");
    //       //_message = '$message';
    //     });
    //   },
    // );
    // _firebaseMessaging.getToken().then((String token) {
    //   assert(token != null);
    //   setState(() {
    //     print(token);
    //   });
    // });
  }

  _setSplashSeen() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('splashSeen', true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                //height: 70,
                color: Colors.white,
                child: Text(
                  "tuvcino",
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.center,
                ),
                /*Image.asset(
                  'assets/splash/tuvcino_logo_720_M.png',
                  height: 50.0,
                ),*/
              ),
              SizedBox(
                height: (MediaQuery.of(context).size.height * 0.20) -
                    (MediaQuery.of(context).padding.top + 70),
              ),
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.80,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15.0),
                        topLeft: Radius.circular(15.0))),
                child: Form(
                  key: _loginForm,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 35.0,
                      ),
                      Text(
                        "¡Hola vecino!",
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                labelText: "Correo",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar tu correo electrónico";
                                }

                                Pattern pattern =
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                RegExp regex = RegExp(pattern.toString());

                                if (!regex.hasMatch(value)) {
                                  return "Asegurate de colocar un email válido";
                                }

                                return null;
                              },
                            ),
                            TextFormField(
                              controller: _passwordController,
                              obscureText: _viewPassword ? false : true,
                              decoration: InputDecoration(
                                labelText: "Contraseña",
                                suffixIcon: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      _viewPassword = !_viewPassword;
                                    });
                                  },
                                  icon: Icon(_viewPassword
                                      ? FontAwesome.eye_off
                                      : FontAwesome.eye),
                                ),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar tu contraseña";
                                }
                                if (value.length < 3) {
                                  return "* Intenta con mínimo 3 caracteres";
                                }
                                return null;
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      TextButton(
                        /* padding:
                            EdgeInsets.symmetric(horizontal: 35, vertical: 12), */
                        onPressed: _sendingUser
                            ? null
                            : () async {
                                /*****************************************/
                                // Check current state of the login form
                                /*****************************************/
                                print(
                                    'Login Form Validation State = ${_loginForm.currentState!.validate()}');
                                if (_loginForm.currentState!.validate()) {
                                  // ScaffoldMessenger.of(context).showSnackBar(
                                  //     SnackBar(
                                  //         content:
                                  //             Text('Iniciando sesión...')));

                                  auth.FirebaseAuth instance =
                                      auth.FirebaseAuth.instance;
                                  try {
                                    setState(() {
                                      _sendingUser = true;
                                    });
                                    await instance
                                        .signInWithEmailAndPassword(
                                            email: _emailController.text.trim(),
                                            password:
                                                _passwordController.text.trim())
                                        .then((user) => {
                                              Navigator.pushReplacement(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          HomePage()))
                                            });
                                  } on auth.FirebaseAuthException catch (e) {
                                    print('Failed with error code: ${e.code}');
                                    print(e.message);
                                    String authError =
                                        "Contraseña o usuario equivocado";
                                    if (e.code == "user-disabled") {
                                      authError =
                                          "Usuario desabilitado por un administrador";
                                    }
                                    showBottomMessage(Icons.dangerous,
                                        "Error al Iniciar sesión \n $authError");
                                    setState(() {
                                      _sendingUser = false;
                                    });
                                  }
                                }
                              },
                        child: Text(
                          "Inicia Sesión",
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ForgottenPassword()));
                        },
                        child: Text("¿Olvidaste tu contraseña?"),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      /* Wrap(
                        spacing: 20.0,
                        children: [
                          GestureDetector(
                            onTap: () {},
                            child: Image(
                              image:
                                  AssetImage('assets/images/google_logo.png'),
                              width: 35.0,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Image(
                              image:
                                  AssetImage('assets/images/facebook_logo.png'),
                              width: 35.0,
                            ),
                          ),
                        ],
                      ), 
                      SizedBox(
                        height: 15.0,
                      ), */
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Theme.of(context)
                              .buttonTheme
                              .colorScheme!
                              .secondaryVariant,
                        ),
                        onPressed: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RegisterPage()));
                        },
                        child: Text(
                          "Crear cuenta",
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showBottomMessage(IconData icon, String message) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(25.0),
          color: Theme.of(context).accentColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
