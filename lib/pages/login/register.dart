import 'package:flutter/material.dart';
/* import 'package:flutter_holo_date_picker/date_picker.dart';
import 'package:flutter_holo_date_picker/i18n/date_picker_i18n.dart'; */
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:tvcino/pages/login/register_zip_code.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  //////////////////////////////////////////////////
  /// Form key and text controllers
  //////////////////////////////////////////////////
  final _registerForm = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  /* final _usernameController = TextEditingController(); */
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  // Scaffold key
  final _scaffoldKey = GlobalKey<ScaffoldState>(debugLabel: '_registerForm');
  bool _sendingUser = false;
  bool _obscurePassword = true;

  /// Which holds the selected date
  /// Defaults to today's date.
  /* String _birthdate = "Selecciona fecha";
  final _birthdateController = TextEditingController(); */
  //////////////////////////////////////////////////
  /// Shared Preferences
  //////////////////////////////////////////////////
  late SharedPreferences preferences;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      key: _scaffoldKey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                height: 70,
                color: Colors.white,
                child: Text(
                  "tuvcino",
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: (MediaQuery.of(context).size.height * 0.25) -
                    (MediaQuery.of(context).padding.top + 70),
              ),
              Container(
                width: double.infinity,
                //height: MediaQuery.of(context).size.height * 0.85,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15.0),
                    topLeft: Radius.circular(15.0),
                    bottomRight: Radius.circular(15.0),
                    bottomLeft: Radius.circular(15.0),
                  ),
                ),
                child: Form(
                  key: _registerForm,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        "¡Únete a la comunidad!",
                        style: Theme.of(context).textTheme.headline2,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.85,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: _nameController,
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                isDense: true,
                                labelText: "Nombre",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar tu nombre";
                                }
                                if (value.length < 4) {
                                  return "* Intenta con un nombre más largo";
                                }
                                return null;
                              },
                            ),
                            /* TextFormField(
                              controller: _usernameController,
                              keyboardType: TextInputType.name,
                              decoration: InputDecoration(
                                isDense: true,
                                labelText: "Username",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar un username";
                                }
                                if (value.length < 4 || value.length > 14) {
                                  return "* Intenta con un username entre 4 a 14 dígitos";
                                }
                                Pattern pattern =
                                    r'^(?=[a-zA-Z0-9._]{4,14}$)(?!.*[_.]{2})[^_.].*[^_.]$';
                                RegExp regex = RegExp(pattern);

                                if (!regex.hasMatch(value)) {
                                  return "Asegurate de colocar un username válido";
                                }

                                return null;
                              },
                            ), */
                            TextFormField(
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                isDense: true,
                                labelText: "Correo",
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar tu correo";
                                }

                                Pattern pattern =
                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                RegExp regex = RegExp(pattern.toString());

                                if (!regex.hasMatch(value)) {
                                  return "Asegurate de colocar un correo válido";
                                }

                                return null;
                              },
                            ),
                            /* TextFormField(
                              controller: _birthdateController,
                              keyboardType: TextInputType.datetime,
                              readOnly: true,
                              decoration: InputDecoration(
                                  isDense: true,
                                  labelText: "Fecha de Nacimiento",
                                  suffixIcon: IconButton(
                                    icon: Icon(Icons.date_range),
                                    onPressed: () async {
                                      var datePicked =
                                          await DatePicker.showSimpleDatePicker(
                                        context,
                                        titleText: 'Selecciona fecha',
                                        initialDate: DateTime(1996),
                                        firstDate: DateTime(1960),
                                        lastDate: DateTime(2012),
                                        dateFormat: "dd-MMMM-yyyy",
                                        locale: DateTimePickerLocale.es,
                                        looping: true,
                                      );
                                      setState(() {
                                        _birthdate = datePicked
                                            .toString()
                                            .substring(0, 11);
                                        _birthdateController.text = _birthdate;
                                      });
                                    },
                                  )),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar una fecha";
                                }
                                return null;
                              },
                            ), */
                            TextFormField(
                              controller: _passwordController,
                              obscureText: _obscurePassword,
                              decoration: InputDecoration(
                                  labelText: "Contraseña",
                                  suffixIcon: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        _obscurePassword = !_obscurePassword;
                                      });
                                    },
                                    /* icon: Icon(Icons.remove_red_eye), */
                                    icon: Icon(_obscurePassword
                                        ? FontAwesome.eye
                                        : FontAwesome.eye_off),
                                  )),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "* Debes ingresar una contraseña";
                                }
                                if (value.length < 6) {
                                  return "* Intenta con mínimo 6 caracteres";
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      TextButton(
                        child: Text(
                          "Continuar",
                          style: TextStyle(fontSize: 20.0),
                        ),
                        onPressed: _sendingUser
                            ? null
                            : () async {
                                /******************************************/
                                // Register form validation
                                /******************************************/
                                print(
                                    'TV - Register Form Validation State = ${_registerForm.currentState!.validate()}');
                                if (_registerForm.currentState!.validate()) {
                                  /******************************************/
                                  // Save the preferences
                                  // of the next of registered user
                                  /******************************************/
                                  preferences =
                                      await SharedPreferences.getInstance();
                                  await preferences.setString(
                                      'name', _nameController.text);
                                  await preferences.setString(
                                      'email', _emailController.text);
                                  /* await preferences.setString(
                                        'username', _usernameController.text); */
                                  /* await preferences.setString(
                                        'birthdate', _birthdateController.text); */
                                  await preferences.setBool(
                                      'neighborhoodSet', false);

                                  auth.FirebaseAuth instance =
                                      auth.FirebaseAuth.instance;
                                  auth.UserCredential userCredentials;

                                  setState(() {
                                    _sendingUser = true;
                                  });

                                  /***************************************/
                                  // Lets try to register this user
                                  /***************************************/
                                  try {
                                    setState(() {
                                      _sendingUser = false;
                                    });

                                    userCredentials = await instance
                                        .createUserWithEmailAndPassword(
                                            email: _emailController.text,
                                            password: _passwordController.text);

                                    if (userCredentials.user != null) {
                                      /***************************************/
                                      // Just for control display info
                                      /***************************************/
                                      preferences.setString(
                                          'uuid', userCredentials.user!.uid);
                                      // await userCredentials.user.updateProfile(
                                      //     displayName: _nameController.text);
                                      await userCredentials.user!
                                          .updateDisplayName(
                                              _nameController.text);
                                      // print('TV - User: ' +
                                      //     userCredentials.user!.email);
                                      print('TV - User to String: ' +
                                          userCredentials.user.toString());
                                      print('TV - Credentials: ' +
                                          userCredentials.credential
                                              .toString());
                                      print(
                                          'TV - User UUID - ${userCredentials.user!.uid}');

                                      /***************************************/
                                      // Lets get to second register page
                                      /***************************************/
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  RegisterZipCode()));
                                    } else {
                                      print(
                                          'TVERR - Error on the User Credential');
                                    }
                                  } on auth.FirebaseAuthException catch (e) {
                                    print(
                                        'TVEX - Error on saving user = ${e.message}');
                                    /* _scaffoldKey.currentState
                                          .removeCurrentSnackBar();
                                      _scaffoldKey.currentState
                                          .showSnackBar(SnackBar(
                                        content: Text(
                                            "Hubo un error al crear el usuario"),
                                      )); */
                                    String messageError =
                                        getMessageFromErrorCode(e);
                                    showBottomMessage(Icons.dangerous,
                                        "Hubo un error al iniciar sesión\n $messageError");
                                    setState(() {
                                      _sendingUser = false;
                                      _passwordController.text = "";
                                    });
                                  }
                                }
                              },
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      //Text("O registrate con"),
                      SizedBox(
                        height: 25.0,
                      ),
                      /* Wrap(
                        spacing: 20.0,
                        children: [
                          GestureDetector(
                            onTap: () {},
                            child: Image(
                              image:
                                  AssetImage('assets/images/google_logo.png'),
                              width: 35.0,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {},
                            child: Image(
                              image:
                                  AssetImage('assets/images/facebook_logo.png'),
                              width: 35.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30.0,
                      ), */
                      /*FlatButton(
                        padding: EdgeInsets.symmetric(horizontal: 35, vertical: 12),
                        color: Theme.of(context).buttonTheme.colorScheme.secondaryVariant,
                        onPressed: (){
                        }, 
                        child: Text("Crear cuenta", style: TextStyle( fontSize: 20.0),),
                      ),*/
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String getMessageFromErrorCode(auth.FirebaseAuthException error) {
    switch (error.code) {
      case "ERROR_EMAIL_ALREADY_IN_USE":
      case "account-exists-with-different-credential":
      case "email-already-in-use":
        return "El correo que ingresaste ya está registrado.";
      //break;
      case "ERROR_WRONG_PASSWORD":
      case "wrong-password":
        return "Correo o password incorrectos.";
      //break;
      case "ERROR_USER_NOT_FOUND":
      case "user-not-found":
        return "No existe un usuario asociado a este correo.";
      //break;
      case "ERROR_USER_DISABLED":
      case "user-disabled":
        return "El usuario ha sido desactivado.";
      //break;
      case "ERROR_TOO_MANY_REQUESTS":
      case "too-many-requests":
      case "operation-not-allowed":
        return "Demasiados intentos de inicio de sesión, por favor intenta tras unos minutos.";
      //break;
      case "ERROR_OPERATION_NOT_ALLOWED":
      case "operation-not-allowed":
        return "Error del servidor, intenta nuevamente.";
      //break;
      case "ERROR_INVALID_EMAIL":
      case "invalid-email":
        return "El correo es inválido.";
      //break;
      default:
        return "Fallo en la operación. Por favor intenta nuevamente.";
      //break;
    }
  }

  showBottomMessage(IconData icon, String message) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(25.0),
          color: Theme.of(context).accentColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
