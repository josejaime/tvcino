import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/fee.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/fees/show_fee.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class AllFeesPage extends StatefulWidget {
  @override
  _AllFeesPageState createState() => _AllFeesPageState();
}

class _AllFeesPageState extends State<AllFeesPage> {
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  late Dio dio;
  List<Fee> _listOfFees = List<Fee>.empty(growable: true);
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  @override
  initState() {
    _user = instance.currentUser as auth.User;
    getNeighborhoodFees();
    initializeDateFormatting();
    super.initState();
  }

  /// ==============================
  /// Get assigned tasks
  /// from a particular user
  /// ==============================
  getNeighborhoodFees() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken(true);
    dio.options.headers['authorization'] = "$token";
    bool safeCall = true;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String neighborhoodId = preferences.getString('neighborhoodId') as String;

    Response response = await dio
        .get('/fees?neighborhood_id=$neighborhoodId')
        .catchError((onError) {
      print("ERROR DOING THE CALL");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            _listOfFees.add(Fee.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(),
      //backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SingleChildScrollView(
              child: Column(
              children: [
                Align(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12.5),
                    child: Text(
                      "Lista de cuotas creadas",
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.headline2,
                    ),
                  ),
                ),
                _error
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: 28.0,
                            color: Colors.black,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(_message, style: TextStyle(color: Colors.black)),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: _listOfFees.isNotEmpty
                              ? _listOfFees.map((e) {
                                  return buildTask(e);
                                }).toList()
                              : [
                                  Text(
                                    "No hay nada para mostrar aún",
                                    style: TextStyle(color: Colors.black),
                                  )
                                ],
                        ),
                      ),
              ],
            )),
    );
  }

  GestureDetector buildTask(Fee fee) {
    String finalDate = "";
    DateFormat format = new DateFormat('dd-MM-yyy', 'es');
    DateTime something = format.parse(fee.createdAt);
    finalDate = DateFormat.yMMMMd('es').format(something);
    //print(DateFormat.yMMMMd('es').format(something));

    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => FeePage(fee: fee)));
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        elevation: 2.0,
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            children: [
              Expanded(
                /* padding: EdgeInsets.all(15.0),
                width: MediaQuery.of(context).size.width * 0.5, */
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              fee.name,
                              style:
                                  Theme.of(context).primaryTextTheme.headline4,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text(
                      "\$${fee.total}",
                      style: Theme.of(context).textTheme.headline2,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      "$finalDate",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          "Creada por: ${fee.createdBy.name}",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Theme.of(context).primaryColor),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Icon(
                Icons.navigate_next,
              )
            ],
          ),
        ),
      ),
    );
  }
}
