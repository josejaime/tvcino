import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:http/http.dart' as http;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/fees/all_fees.dart';
import 'package:tvcino/utils/custom_functions.dart';

class AddFeePage extends StatefulWidget {
  @override
  _AddFeePageState createState() => _AddFeePageState();
}

class _AddFeePageState extends State<AddFeePage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  //===============================*/
  // Controlls
  //===============================*/
  final GlobalKey<FormState> _feeAddKey =
      GlobalKey<FormState>(debugLabel: 'feeAddKey');
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _shortController = TextEditingController();

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getUserInformation();
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
    //print("The user Token is = $token");
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addFee() async {
    print("TV - ADD FEE TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String name = _titleController.text;
    String total = _shortController.text;
    String neighborhoodId = preferences.getString('neighborhoodId') as String;
    String userId = preferences.getString('userId') as String;

    String url = CustomFunctions.url + "/fees/";

    try {
      String data =
          '{"name": "$name", "total": $total, "neighborhood_id": "$neighborhoodId", "created_by": "$userId"}';
      print("TV - DATA");
      print(data);
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      Map<String, dynamic> fee = new Map<String, dynamic>();
      fee['name'] = name;
      fee['total'] = total;
      fee['neighborhood_id'] = neighborhoodId;

      var postUri = Uri.parse(url);
      await http
          .post(postUri,
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
                'authorization': '$token'
              },
              body: jsonEncode(<String, String>{
                'name': name,
                'total': total,
                'neighborhood_id': neighborhoodId,
                'created_by': userId,
              }))
          .then((response) {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          //Fee fee = Fee.fromJson(jsonDecode(response.body));
          Navigator.pop(context);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AllFeesPage()));
          print("TV - FEE CREATED ALL OK");
          /* Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false); */
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV HTTP ERROR - Status Code ${response.body}");
          print("TV HTTP ERROR - Status Code ${response.headers}");
          /* String strResponse = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $strResponse"); */
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    /***********/
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Nueva Cuota",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información general",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Form(
                              key: _feeAddKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: _titleController,
                                    decoration: InputDecoration(
                                        labelText: "Nombre de la cuota"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar el nombre";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con un nombre más largo";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  TextFormField(
                                      keyboardType: TextInputType.number,
                                      controller: _shortController,
                                      decoration: InputDecoration(
                                        hintText: "Total de la cuota",
                                      ),
                                      validator: (value) {
                                        if (value!.isEmpty) {
                                          return "* Debes poner un valor";
                                        }
                                        if (double.tryParse(value) == null) {
                                          return "Debes ingresar un número entero";
                                        }
                                        double valueScore = double.parse(value);
                                        if (valueScore < 0) {
                                          return "Intenta con un número mayor a cero";
                                        }
                                        return null;
                                      }),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Guardar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    onPressed: () {
                                      if (_feeAddKey.currentState!.validate()) {
                                        _addFee();
                                      } else {
                                        print("TV - VALIDATE FALSE");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
