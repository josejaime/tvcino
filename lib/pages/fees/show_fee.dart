import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:tvcino/classes/fee.dart';
import 'package:tvcino/classes/payment.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/payments/add_payment.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class FeePage extends StatefulWidget {
  final Fee fee;

  FeePage({
    required this.fee,
  });

  @override
  _FeePageState createState() => _FeePageState();
}

class _FeePageState extends State<FeePage> {
  /// ==============================
  /// DIO GET THE POSTS
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = false;
  bool _error = false;
  late Fee _fee;
  List<Payment> listPayments = List<Payment>.empty(growable: true);

  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    _getFee();
    super.initState();
  }

  _getFee() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers['authorization'] = "$token";
    bool safeCall = true;
    print("ID- ${widget.fee.id}");
    Response response =
        await dio.get('/fees/${widget.fee.id}').catchError((onError) {
      print("ERRORORORORORO ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response);
        print("DIO Trying to convert data");
        //List data = response.data['items'];
        setState(() {
          _fee = Fee.fromJson(response.data);
          _isLoading = false;
          _error = false;
          _message = "";
        });

        ///==============================
        /// Now Lets get the comments
        ///==============================
        bool safeCallComments = true;

        Response responseComments = await dio
            .get('/payments?fee_id=${widget.fee.id}&active=true')
            .catchError((onError) {
          print("TV - ERROR MAKING CALL TO COMMENTS ${onError.toString()}");
          safeCallComments = false;
        });

        if (safeCallComments) {
          List data = responseComments.data['items'];
          if (data.length > 0) {
            for (int i = 0; i < data.length; i++) {
              setState(() {
                listPayments.add(Payment.fromJson(data[i]));
              });
            }
          } else {
            /// There is not comments
            print("TV - MSG Fee WITH NO Payments");
          }
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        width: width,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "¡Hola Vecino!",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontFamily: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .fontFamily),
                            ),
                            Divider(
                              color: Colors.white24,
                            ),
                            Text(
                              "Revisa aquí la gestión de la cuota",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17.0,
                                fontFamily: Theme.of(context)
                                    .textTheme
                                    .headline6!
                                    .fontFamily,
                              ),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Container(
                                width: width * 0.8,
                                padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0),
                                decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .textTheme
                                      .headline6!
                                      .color,
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          Text(
                                            "\$${_fee.total}",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 28.0,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          Text(
                                            "Costo de la cuota",
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 1.5,
                                    ),
                                    Container(
                                      width: width * 0.8,
                                      padding: EdgeInsets.all(15.0),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(15.0),
                                              bottomRight:
                                                  Radius.circular(15.0))),
                                      child: Text(_fee.name),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Container(
                        width: width * 0.75,
                        child: TextButton(
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.symmetric(vertical: 15.0),
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddPaymentPage(
                                          fee: _fee,
                                        )));
                          },
                          child: Text("Registrar pago"),
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      // Here is my git change for ammend
                      listPayments.isNotEmpty
                          ? buildPayments(listPayments, context)
                          : Text("No hay pagos aún para esta cuota"),
                    ],
                  ),
                ),
    );
  }

  Widget buildPayments(List<Payment> payments, BuildContext context) {
    return Column(
      children: payments.map((e) {
        //return Text("${e.name}");
        return buildPayment(context, e);
      }).toList(),
    );
  }

  Column buildPayment(BuildContext context, Payment e) {
    return Column(
      children: [
        Divider(),
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: CircleAvatar(
                      radius: 15.0,
                      backgroundColor: const Color(0xFF778899),
                      child: Text(''),
                    ),
                  ),
                  Expanded(
                      flex: 5,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Pago por : ${e.user.name}',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          Text(e.createdAt,
                              style: TextStyle(
                                  fontWeight: FontWeight.w200,
                                  fontSize: 12.0,
                                  color: Colors.black45)),
                        ],
                      )),
                ],
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 17.5, bottom: 2.0, right: 2.0, left: 2.0),
                  child: Text(
                    "Monto del pago " + e.payment.toString(),
                    textAlign: TextAlign.left,
                    style: Theme.of(context).primaryTextTheme.headline5,
                  ),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
            ],
          ),
        ),
        Divider(
          color: Colors.black38,
          height: 1,
        ),
      ],
    );
  }
}
