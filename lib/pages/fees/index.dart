import 'package:flutter/material.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/pages/fees/add_fee.dart';
import 'package:tvcino/pages/fees/all_fees.dart';

class FeesIndexPage extends StatefulWidget {
  @override
  _FeesIndexPageState createState() => _FeesIndexPageState();
}

class _FeesIndexPageState extends State<FeesIndexPage> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: width,
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "¡Hola Vecino!",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontFamily: Theme.of(context)
                                .textTheme
                                .headline6!
                                .fontFamily),
                      ),
                      /* Text(
                        "Fraccionamiento del bosque",
                        style: TextStyle(color: Colors.white),
                      ), */
                      Divider(
                        color: Colors.white24,
                      ),
                      Text(
                        "Revisa aquí la gestión de tu vecindario",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17.0,
                          fontFamily:
                              Theme.of(context).textTheme.headline6!.fontFamily,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      /* Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: width * 0.8,
                          padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0),
                          decoration: BoxDecoration(
                            color: Theme.of(context).textTheme.headline6.color,
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Text(
                                      "\$4,999",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 28.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    Text(
                                      "Recaudado hasta el momento...",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 1.5,
                              ),
                              Container(
                                width: width * 0.8,
                                padding: EdgeInsets.all(15.0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(15.0),
                                        bottomRight: Radius.circular(15.0))),
                                child: Text("Total del vecindario \$9,999"),
                              ),
                            ],
                          ),
                        ),
                      ), */
                    ],
                  ),
                ),
                /* Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      height: 100.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      height: 100.0,
                      width: 100.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.blue,
                      ),
                    ),
                  ],
                ), */
                /* Container(
                  width: width * 0.8,
                  padding: EdgeInsets.fromLTRB(0.0, 25.0, 0.0, 0),
                  decoration: BoxDecoration(
                    color: Theme.of(context).textTheme.headline6.color,
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              "\$4,999",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 28.0,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              "Recaudado hasta el momento...",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: width * 0.8,
                        padding: EdgeInsets.all(15.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(15.0),
                                bottomRight: Radius.circular(15.0))),
                        child: Text("Total de..."),
                      ),
                    ],
                  ),
                ), */
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 25.0, horizontal: 25.0),
                  child: Column(
                    children: [
                      /* RaisedButton(
                        color:
                            Theme.of(context).buttonTheme.colorScheme.surface,
                        padding: EdgeInsets.symmetric(
                            horizontal: 20, vertical: 17.5),
                        onPressed: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Text(
                                "Cuotas Pagadas",
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ),
                            Text(
                              "30/30",
                              style:
                                  Theme.of(context).primaryTextTheme.headline5,
                            ),
                            Icon(Icons.arrow_downward),
                          ],
                        ),
                      ),
                      RaisedButton(
                        color:
                            Theme.of(context).buttonTheme.colorScheme.surface,
                        padding: EdgeInsets.symmetric(
                            horizontal: 20, vertical: 17.5),
                        onPressed: () {},
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 12.0),
                              child: Text(
                                "Cuotas Restantes",
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ),
                            Text(
                              "30/30",
                              style:
                                  Theme.of(context).primaryTextTheme.headline5,
                            ),
                            Icon(Icons.arrow_downward),
                          ],
                        ),
                      ), */
                    ],
                  ),
                ),
                Container(
                  width: width * 0.75,
                  child: TextButton(
                    /* padding:
                        EdgeInsets.symmetric(vertical: 17.5, horizontal: 30.0), */
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 17.5),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AllFeesPage()));
                    },
                    child: Text("Ver cuotas registradas"),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  width: width * 0.75,
                  child: TextButton(
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.symmetric(vertical: 15.0),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddFeePage()));
                    },
                    child: Text("Registrar Cuota"),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
