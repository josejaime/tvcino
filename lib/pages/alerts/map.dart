import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
//import 'package:latlong/latlong.dart';
import 'package:geocoding/geocoding.dart' as geo;

class AlertMap extends StatefulWidget {
  final double latitude;
  final double longitude;

  AlertMap({
    required this.latitude,
    required this.longitude,
  });

  @override
  _AlertMapState createState() => _AlertMapState();
}

class _AlertMapState extends State<AlertMap> {
  String address = "";
  String city = "";
  String state = "";
  late LatLng myPosition; //= LatLng(widget.latitude, widget.longitude);

  @override
  void initState() {
    myPosition = LatLng(widget.latitude, widget.longitude);
    getPosition();
    super.initState();
  }

  getPosition() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      myPosition.latitude =
          preferences.getDouble('latitude') ?? widget.latitude;
      myPosition.longitude =
          preferences.getDouble('longitude') ?? widget.longitude;
    });

    List<geo.Placemark> placemarks = await geo.placemarkFromCoordinates(
        widget.latitude, widget.longitude,
        localeIdentifier: 'es_MX');
    print(placemarks);
    if (placemarks.isNotEmpty) {
      debugPrint('TV - Trying to update address');
      setState(() {
        address = placemarks.first.street as String;
        state = placemarks.first.administrativeArea as String;
        city = placemarks.first.locality as String;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;

    var points = <LatLng>[
      LatLng(widget.latitude, widget.longitude),
      LatLng(myPosition.latitude, myPosition.longitude),
    ];

    // var pointsGradient = <LatLng>[
    //   LatLng(21.879610, -102.295227),
    //   LatLng(20.659698, -103.349609),
    // ];

    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      body: Container(
        width: width,
        child: Column(
          children: [
            Flexible(
              child: FlutterMap(
                options: MapOptions(
                  //center: LatLng(51.5, -0.09),
                  center: LatLng(widget.latitude, widget.longitude),
                  zoom: 15.0,
                ),
                layers: [
                  TileLayerOptions(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c'],
                  ),
                  PolylineLayerOptions(
                    polylines: [
                      Polyline(
                          points: points,
                          strokeWidth: 4.0,
                          color: Colors.purple),
                    ],
                  ),
                  // PolylineLayerOptions(
                  //   polylines: [
                  //     Polyline(
                  //       points: pointsGradient,
                  //       strokeWidth: 4.0,
                  //       gradientColors: [
                  //         Color(0xffE40203),
                  //       ],
                  //     ),
                  //   ],
                  // ),
                  MarkerLayerOptions(
                    markers: [
                      Marker(
                        width: 60.0,
                        height: 60.0,
                        point: LatLng(widget.latitude, widget.longitude),
                        builder: (ctx) => Container(
                            child: Image(
                                image: ExactAssetImage(
                                    'assets/iconos/atraco.png'))),
                      ),
                      Marker(
                        width: 60.0,
                        height: 60.0,
                        point:
                            LatLng(myPosition.latitude, myPosition.longitude),
                        builder: (ctx) => Container(
                            child: Image(
                                image: ExactAssetImage(
                                    'assets/iconos/ubicacion.png'))),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: width,
              //color: Theme.of(context).primaryColor,
              padding: EdgeInsets.all(8.0),
              //height: height * 0.35 - 95,
              child: Column(
                children: [
                  // Text("Alerta enviada en la siguiente dirección:",
                  //     style: TextStyle(color: Theme.of(context).primaryColor)),
                  Text("Alerta enviada en la siguiente dirección:",
                      style: Theme.of(context).textTheme.headline5),
                  Text("Estado: $state",
                      //style: TextStyle(color: Theme.of(context).primaryColor)),
                      style: Theme.of(context).primaryTextTheme.headline5),
                  Text("Ciudad: $city",
                      style: Theme.of(context).primaryTextTheme.headline5),
                  Text("Calle: $address",
                      style: Theme.of(context).primaryTextTheme.headline5),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // String route = 'polyline';

  // @override
  // Widget build(BuildContext context) {
  //   var points = <LatLng>[
  //     LatLng(21.879610, -102.295227),
  //     LatLng(20.659698, -103.349609),
  //   ];

  //   var pointsGradient = <LatLng>[
  //     LatLng(21.879610, -102.295227),
  //     LatLng(20.659698, -103.349609),
  //   ];

  //   return Scaffold(
  //     appBar: AppBar(title: Text('Polylines')),
  //     //drawer: buildDrawer(context, PolylinePage.route),
  //     body: Padding(
  //       padding: EdgeInsets.all(8.0),
  //       child: Column(
  //         children: [
  //           Padding(
  //             padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
  //             child: Text('Polylines'),
  //           ),
  //           Flexible(
  //             child: FlutterMap(
  //               options: MapOptions(
  //                 center: LatLng(51.5, -0.09),
  //                 zoom: 5.0,
  //               ),
  //               layers: [
  //                 TileLayerOptions(
  //                     urlTemplate:
  //                         'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
  //                     subdomains: ['a', 'b', 'c']),
  //                 PolylineLayerOptions(
  //                   polylines: [
  //                     Polyline(
  //                         points: points,
  //                         strokeWidth: 4.0,
  //                         color: Colors.purple),
  //                   ],
  //                 ),
  //                 PolylineLayerOptions(
  //                   polylines: [
  //                     Polyline(
  //                       points: pointsGradient,
  //                       strokeWidth: 4.0,
  //                       gradientColors: [
  //                         Color(0xffE40203),
  //                       ],
  //                     ),
  //                   ],
  //                 ),
  //               ],
  //             ),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
}
