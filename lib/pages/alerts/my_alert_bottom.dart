import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:http/http.dart' as http;

class MyBottomSheet extends StatefulWidget {
  @override
  _MyBottomSheetState createState() => _MyBottomSheetState();
}

class _MyBottomSheetState extends State<MyBottomSheet> {
  late Timer _timer;
  int _start = 10;
  bool running = true;

  bool _error = true;
  bool _isLoading = false;
  String _message = "";

  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  void startTimer() {
    print("Starting timer");
    setState(() {
      _start = 10;
    });
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            print("Canceling timer");
            print("Create Alert");
            _addAlert();
            running = false;
            timer.cancel();
          });
        } else {
          setState(() {
            print("$_start time");
            _start--;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    print("Disposing timer from inneer stateful widget");
    if (_timer.isActive) {
      _timer.cancel();
    }
    print("Not create an alert");
    super.dispose();
  }

  @override
  void initState() {
    startTimer();
    _user = instance.currentUser as auth.User;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 25.0, vertical: 25.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: running
                  ? [
                      Text("Se lanzará una alerta en:",
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(fontSize: 24.0, color: Colors.white)),
                      SizedBox(
                        height: 20,
                      ),
                      Text(_start.toString(),
                          style:
                              TextStyle(fontSize: 44.0, color: Colors.white)),
                      SizedBox(
                        height: 10,
                      ),
                      Text("Pulsa el botón o baja esta pantalla para cancelar",
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(fontSize: 20.0, color: Colors.white)),
                      SizedBox(
                        height: 45,
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          primary: Colors.black,
                        ),
                        //onPressed: () => setState(() => _flag = !_flag),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Cancelar Alerta',
                            style: TextStyle(fontSize: 20.0)),
                      )
                    ]
                  : [
                      Icon(
                        Icons.warning_rounded,
                        size: 45.0,
                        color: Colors.red.shade800,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text("¡Alerta Enviada!",
                          style: TextStyle(fontSize: 24.0, color: Colors.white))
                    ],
            ),
          ),
        ),
      ),
    );
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addAlert() async {
    print("TV - ADD GROUP TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userId = preferences.getString('userId') as String;

    String url = CustomFunctions.url + "/alerts/";
    String name = "Nueva Alerta";
    double latitude = preferences.getDouble('latitude') ?? 0.0;
    double longitude = preferences.getDouble('longitude') ?? 0.0;
    String content = "LAT AND LONG";
    name = "$latitude,$longitude";

    try {
      String data =
          '{"name": "$name", "content": "$content", "expiration_time": "14-01-2024T13:28:51", "type": 2, "risk_rate": 0, "user_id": "$userId", "active": true}';

      print("TV - DATA");
      print(data.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      http
          .post(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
        body: data,
      )
          .then((response) {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          print("TV - Alert CREATED ALL OK");
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV something went wrong ${response.request}");
          print("TV HTTP ERROR - Status Code ${response.body}");
          setState(() {
            _error = true;
            _isLoading = false;
            _message = "Hubo un error en el servicio intenta más tarde";
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
      });
      return false;
    }

    return false;
  }
}
