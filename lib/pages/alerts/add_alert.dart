import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/alerts/index_alerts.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:http/http.dart' as http;

class AddAlertPage extends StatefulWidget {
  final int? alertType;

  AddAlertPage({
    this.alertType,
  });

  @override
  _AddAlertPageState createState() => _AddAlertPageState();
}

class _AddAlertPageState extends State<AddAlertPage> {
  bool _high = false;
  bool _low = false;

  TextEditingController _contentController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  int type = 1;

  /// ==============================
  /// DIO and more
  /// ==============================
  late Dio dio;
  bool _isLoading = false;
  bool _error = false;
  String _message = "";
  late SharedPreferences preferences;

  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    setState(() {
      widget.alertType == 1 ? _low = true : _high = true;
    });
    getUserInformation();
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    //token = await _user.getIdToken();
    //print("The user Token is = $token");
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addAlert() async {
    print("TV - ADD GROUP TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    preferences = await SharedPreferences.getInstance();
    String content =
        _contentController.text == "" ? "none" : _contentController.text;
    String name = _nameController.text;
    String userId = preferences.getString('userId') as String;

    String url = CustomFunctions.url + "/alerts/";

    try {
      if (_high) {
        type = 2;
      }
      if (_low) {
        type = 1;
      }

      String data =
          '{"name": "$name", "content": "$content", "expiration_time": "14-01-2024T13:28:51", "type": $type, "risk_rate": 0, "user_id": "$userId", "active": true}';

      print("TV - DATA");
      print(data.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      http
          .post(
        postUri,
        headers: {
          HttpHeaders.authorizationHeader: "$token",
          "Content-Type": "application/json"
        },
        body: data,
      )
          .then((response) {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          print("TV - Alert CREATED ALL OK");
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => IndexAlertsPage()));
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV something went wrong ${response.request}");
          print("TV HTTP ERROR - Status Code ${response.body}");
          setState(() {
            _error = true;
            _isLoading = false;
            _message = "Hubo un error en el servicio intenta más tarde";
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
      });
      return false;
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : SingleChildScrollView(
                  child: Container(
                    width: width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 3.5,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.97,
                          padding: EdgeInsets.all(20.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Text(
                            "Alerta",
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.97,
                          padding: EdgeInsets.all(20.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Column(
                            children: [
                              Form(
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 5.0,
                                    ),
                                    TextFormField(
                                      controller: _nameController,
                                      decoration: InputDecoration(
                                          labelText: "Nombre de la Alerta",
                                          hintText: "Alerta..."),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    TextField(
                                      maxLines: 4,
                                      controller: _contentController,
                                      decoration: InputDecoration.collapsed(
                                          hintText: "¿Tienes algún mensaje?"),
                                    ),
                                    SizedBox(
                                      height: 20.0,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'Tipo',
                                          textAlign: TextAlign.left,
                                        ),
                                      ],
                                    ),
                                    Container(
                                      height: 85.0,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: [
                                          ChoiceChip(
                                            selectedColor: Colors.red,
                                            label: Text("Atraco"),
                                            selected: _high,
                                            onSelected: (bool value) {
                                              setState(() {
                                                _high = value;
                                                _low = false;
                                              });
                                            },
                                          ),
                                          SizedBox(
                                            width: 2.0,
                                          ),
                                          ChoiceChip(
                                              label: Text("Aviso"),
                                              selectedColor: Colors.green,
                                              selected: _low,
                                              onSelected: (bool selected) {
                                                print("Hello there $selected");
                                                setState(() {
                                                  _high = false;
                                                  _low = selected;
                                                });
                                              }),
                                          SizedBox(
                                            width: 2.0,
                                          ),
                                        ],
                                      ),
                                    ),
                                    TextButton(
                                      child: Text(
                                        "Mandar",
                                        style: TextStyle(fontSize: 20.0),
                                      ),
                                      onPressed: () {
                                        _addAlert();
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
    );
  }
}
