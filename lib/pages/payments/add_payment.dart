import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/fee.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/fees/show_fee.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart' as auth;

class AddPaymentPage extends StatefulWidget {
  final Fee fee;

  AddPaymentPage({
    required this.fee,
  });

  @override
  _AddPaymentPageState createState() => _AddPaymentPageState();
}

class _AddPaymentPageState extends State<AddPaymentPage> {
  List<InterUser> contacts = List<InterUser>.empty(growable: true);

  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  /// ==============================
  /// DIO and more
  /// ==============================
  late Dio dio;
  bool _isLoading = false;
  bool _error = false;
  String _message = "";
  late SharedPreferences preferences;
  String _selectedValue = "";
  final TextEditingController _paymentController = TextEditingController();
  final GlobalKey<FormState> _paymentAddKey =
      GlobalKey<FormState>(debugLabel: 'paymentAddKey');

  /// =============================================
  /// Function
  /// Add payment to a fee in TVCINO DB
  /// =============================================
  Future<bool> _addPayment() async {
    print("TV - ADD FEE TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    String url = CustomFunctions.url + "/payments/";

    try {
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      double total = double.parse(_paymentController.text);
      double change = 0;
      change = widget.fee.total - total;

      var postUri = Uri.parse(url);
      await http
          .post(postUri,
              headers: <String, String>{
                'Content-Type': 'application/json; charset=UTF-8',
                'authorization': '$token'
              },
              body: jsonEncode(<String, dynamic>{
                'fee_id': widget.fee.id,
                'payment': total,
                'user_id': _selectedValue,
                'change': change
              }))
          .then((response) {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - FEE CREATED ALL OK");
          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FeePage(
                fee: widget.fee,
              ),
            ),
          );
          /* Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false); */
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV HTTP ERROR - Status Code ${response.body}");
          print("TV HTTP ERROR - Status Code ${response.headers}");
          /* String strResponse = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $strResponse"); */
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }

    return false;
  }

  /// =================================
  /// Get users fomr the neighborhood
  /// =================================
  getNeighborhoodUsers() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers['authorization'] = "$token";
    bool safeCall = true;

    preferences = await SharedPreferences.getInstance();
    String neighborhoodId = preferences.getString('neighborhoodId') as String;
    print("TV Neighborhood Id is : $neighborhoodId");

    Response response = await dio
        .get('/neighborhoods/$neighborhoodId/users')
        .catchError((onError) {
      print("ERRORORORORORO");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        //List data = response.data;
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print(data[i]['name']);
            //contacts.add(InterUser.fromJson(_data[i]));
            contacts.add(InterUser.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
            _selectedValue = contacts[0].id;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            _error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
    //print("The user Token is = $token");
  }

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getNeighborhoodUsers();
    getUserInformation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Cuota \$${widget.fee.total}",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información del pago",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Form(
                              key: _paymentAddKey,
                              child: Column(
                                children: [
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      "Usuario",
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline4,
                                    ),
                                  ),
                                  contacts.isNotEmpty
                                      ? Material(
                                          child: DropdownButton<String>(
                                            value: _selectedValue,
                                            isExpanded: true,
                                            isDense: false,
                                            icon: Icon(Icons.arrow_downward),
                                            iconSize: 24,
                                            elevation: 0,
                                            onChanged: (String? newValue) {
                                              setState(() {
                                                _selectedValue =
                                                    newValue as String;
                                              });
                                              print(
                                                  'TV - Selected new value $_selectedValue');
                                            },
                                            items: contacts
                                                .map<DropdownMenuItem<String>>(
                                                    (InterUser user) {
                                              return DropdownMenuItem<String>(
                                                value: user.id,
                                                child: Text(user.name),
                                              );
                                            }).toList(),
                                          ),
                                        )
                                      : Text('No hay usuarios disponibles'),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  TextFormField(
                                      keyboardType: TextInputType.number,
                                      controller: _paymentController,
                                      decoration: InputDecoration(
                                        hintText: "Pagado",
                                      ),
                                      validator: (value) {
                                        if (value!.isEmpty) {
                                          return "* Debes poner un valor";
                                        }
                                        if (double.tryParse(value) == null) {
                                          return "Debes ingresar un número entero";
                                        }
                                        double valueScore = double.parse(value);
                                        if (valueScore < 0) {
                                          return "Intenta con un número mayor a cero";
                                        }
                                        return null;
                                      }),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Guardar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    onPressed: () {
                                      if (_paymentAddKey.currentState!
                                          .validate()) {
                                        _addPayment();
                                      } else {
                                        print("TV - VALIDATE FALSE");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
