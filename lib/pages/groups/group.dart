import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/classes/posts.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/events/event.dart';
import 'package:tvcino/pages/groups/group_members.dart';
import 'package:tvcino/pages/posts/add_post_group.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class GroupPage extends StatefulWidget {
  final Group group;

  GroupPage({
    required this.group,
  });

  @override
  _GroupPageState createState() => _GroupPageState();
}

class _GroupPageState extends State<GroupPage> {
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  late SharedPreferences preferences;
  late String userId;

  /// ==============================
  /// DIO GET THE POSTS
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = true;
  //bool _error = false;

  /// ==============================
  /// The Posts
  /// ==============================
  List<Post> _listOfPosts = List<Post>.empty(growable: true);

  /// ==============================
  /// Get posts from server
  /// ==============================
  getGroupPosts() async {
    setState(() {
      _isLoading = true;
      _message = "";
      //_error = false;
    });

    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;

    Response response =
        await dio.get('/posts?group_id=${widget.group.id}&start=0&limit=200')
            //.get('/posts?start=0&limit=200')
            .catchError((onError) {
      print("TV - Error getting posts ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          //_error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print(data[i]['title']);
            //_listOfUsers.add(InterUser.fromJson(_data[i]));
            _listOfPosts.add(Post.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            //_error = false;
          });
        } else {
          setState(() {
            _message =
                "Lo sentimos, no hay publicaciones que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        //_error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getGroupPosts();
    getUserInformation();
    print('TV - THE GROUP ID IS = ${widget.group.id}');
    super.initState();
  }

  getUserInformation() async {
    preferences = await SharedPreferences.getInstance();
    setState(() {
      userId = preferences.getString('userId') as String;
    });
    print("USERID: $userId == ${widget.group.authorId}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
        title: Image(
          image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
          height: 21.0,
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height / 3.8,
                decoration: BoxDecoration(
                    color: Theme.of(context).accentColor.withOpacity(0.7),
                    image: DecorationImage(
                      image: NetworkImage(widget.group.image),
                      fit: BoxFit.cover,
                    )),
                child: Stack(
                  fit: StackFit.expand,
                  children: [
                    /*Positioned(
                      bottom: 0,
                      right: 0,
                      child: RawMaterialButton(
                        onPressed: () {
                          print("Icon Share Button");
                        },
                        shape: CircleBorder(),
                        fillColor: Colors.white,
                        child: Icon( Icons.share , color: Theme.of(context).primaryColor,),
                      ),
                    ),*/
                    /* Positioned(
                      bottom: 0,
                      right: -15,
                      child: Column(
                        children: [
                          RawMaterialButton(
                            onPressed: () {
                              print("Icon Share Button");
                            },
                            shape: CircleBorder(),
                            fillColor: Colors.white,
                            //child: Icon( Icons.share , color: Theme.of(context).primaryColor,),
                            child: Icon(FontAwesome.forward,
                                color: Theme.of(context).primaryColor),
                          ),
                          RawMaterialButton(
                            onPressed: () {
                              print("Icon Share Button");
                            },
                            shape: CircleBorder(),
                            fillColor: Colors.white,
                            //child: Icon( Icons.thumb_up , color: Theme.of(context).primaryColor,),
                            child: Icon(FontAwesome.heart_empty,
                                color: Theme.of(context).primaryColor),
                          ),
                        ],
                      ),
                    ), */
                  ],
                ),
              ),
              /*====================================*/
              /* Event title */
              /*====================================*/
              Container(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.group.name,
                    style: Theme.of(context).textTheme.headline1,
                    textScaleFactor: 0.7,
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              /*====================================*/
              /* Event Place and Time */
              /*====================================*/
              Divider(),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 1.0, horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Chip(
                          label: Text('Afiliado'),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: userId == widget.group.authorId
                              ? () {
                                  debugPrint("TV - SHOW MEMBERS");
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => GroupUsers(
                                        group: widget.group,
                                      ),
                                    ),
                                  );
                                }
                              : null,
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.fromLTRB(
                                    8.0, 4.0, 8.0, 4.0),
                                child: Icon(
                                  Icons.people,
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 3.0),
                                child:
                                    Text(widget.group.totalMembers.toString()),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              /*====================================*/
              /* Event Info */
              /*====================================*/
              Divider(),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Más Información',
                      style: Theme.of(context).textTheme.headline4,
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('${widget.group.description}'),
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextButton(
                          onPressed: () {
                            //_settingModalBottomSheet(context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AddGrupPostPage(
                                  group: widget.group,
                                ),
                              ),
                            );
                          },
                          child: Text("Crea una publicación para el grupo")),
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Publicaciones',
                      style: Theme.of(context).textTheme.headline4,
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
              _listOfPosts.isNotEmpty
                  ? Column(
                      children: _listOfPosts
                          .map((e) => buildCardWithImage(context, e))
                          .toList())
                  : _isLoading
                      ? TvcinoCircularProgressBar(
                          color: Colors.white,
                          textColor: Theme.of(context).primaryColor,
                        )
                      : Text(_message),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  Widget buildCardWithImage(BuildContext context, Post post) {
    return Column(
      children: [
        SizedBox(
          height: 10.0,
        ),
        Align(
          child: GestureDetector(
            onTap: () {
              print("Hello new gesture detector");
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EventPage(
                            id: post.id,
                            tile: post.title,
                            image: post.image,
                            type: 1,
                          )));
            },
            child: Container(
              height: 275,
              width: MediaQuery.of(context).size.width / 1.1,
              decoration: BoxDecoration(
                image: post.image != null
                    ? DecorationImage(
                        //image: ExactAssetImage(image),
                        image: NetworkImage(post.image),
                        fit: BoxFit.cover,
                        alignment: Alignment.topCenter)
                    : null,
                borderRadius: BorderRadius.circular(15.0),
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.25),
                    spreadRadius: 3,
                    blurRadius: 3,
                    offset: Offset(2, 4), // changes position of shadow
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Positioned(
                    bottom: 0,
                    width: MediaQuery.of(context).size.width / 1.1,
                    child: Container(
                      //color: Colors.white,
                      padding: EdgeInsets.all(10.0),
                      height: 120,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.5),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Flexible(
                                child: RichText(
                                  overflow: TextOverflow.ellipsis,
                                  strutStyle: StrutStyle(fontSize: 12.0),
                                  text: TextSpan(
                                      style:
                                          Theme.of(context).textTheme.subtitle1,
                                      text: post.title),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Text((post.shortDescription!.length > 125
                              ? post.shortDescription!.substring(0, 123) + "..."
                              : post.shortDescription) as String),
                          SizedBox(
                            height: 10.0,
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                      right: 2.5,
                      bottom: 100,
                      //top: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          /*border: Border.all(
                                color: Colors.black,
                                width: 0.1
                              ),*/
                          borderRadius: BorderRadius.circular(50.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.25),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset:
                                  Offset(1, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: IconButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => EventPage(
                                          id: post.id,
                                          tile: post.title,
                                          image: post.image,
                                          type: 1,
                                        )));
                          },
                          icon: Icon(Icons.send),
                          //icon: Image.asset('assets/iconos/enviar.png'),
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
      ],
    );
  }
}
