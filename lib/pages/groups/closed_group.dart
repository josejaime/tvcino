import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/groups/groups.dart';
import 'package:tvcino/pages/groups/update_group.dart';
import 'package:http/http.dart' as http;
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class ClosedGroup extends StatefulWidget {
  final int? id;
  final String? title;
  final String? image;
  final String? members;
  final Group? group;

  ClosedGroup({
    this.id,
    this.title,
    this.image,
    this.members,
    this.group,
  });

  @override
  _ClosedGroupState createState() => _ClosedGroupState();
}

class _ClosedGroupState extends State<ClosedGroup> {
  late String _userId;
  late SharedPreferences preferences;
  late String token;
  //===============================*/
  // Messages
  //===============================*/
  bool _isLoading = false;
  String _message = "";
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;

  /// =============================================
  /// Function
  /// Add user to the group
  /// =============================================
  Future<bool> _joinGroup() async {
    print("TV - ADD FEE TO DB");

    setState(() {
      _isLoading = true;
      _message = "";
    });

    String url =
        CustomFunctions.url + "/groups/${widget.group!.id}/users/$_userId";
    token = await _user.getIdToken();

    try {
      var postUri = Uri.parse(url);
      print("TV - ADD GROUP URL $url");
      await http.post(postUri, headers: <String, String>{
        //'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': '$token',
      }).then((response) {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// User was added to the group members
          //============================================/
          setState(() {
            _isLoading = false;
            _message = "";
          });

          print("TV - ADDED TO GROUP");
          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => GroupsPage()));
          /*Navigator.of(context).pushNamedAndRemoveUntil(
              '/groups', (Route<dynamic> route) => false);*/
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV HTTP ERROR - Status Code ${response.body}");
          print("TV HTTP ERROR - Status Code ${response.headers}");
          /* String strResponse = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $strResponse"); */
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _isLoading = false;
            _showDialogWithMessage(context, _message);
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
        _showDialogWithMessage(context, _message);
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
        _showDialogWithMessage(context, _message);
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
        _showDialogWithMessage(context, _message);
      });
      return false;
    }

    return false;
  }

  void _showDialogWithMessage(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("tuvcino"),
          content: Text(message),
          actions: [
            TextButton(
              child: Text('Aceptar'),
              //color: Theme.of(context).buttonTheme.colorScheme.secondary,
              onPressed: () {
                //Just dismiss the alert
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getPreferences();
    super.initState();
  }

  Future<void> getPreferences() async {
    preferences = await SharedPreferences.getInstance();
    setState(() {
      _userId = preferences.getString('userId') as String;
    });
    debugPrint("TV - DEBUG $_userId == ${widget.group!.authorId}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        title: Image(
          image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
          height: 21.0,
        ),
      ),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: MediaQuery.of(context).size.height / 3.8,
                      decoration: BoxDecoration(
                          color: Theme.of(context).accentColor.withOpacity(0.7),
                          image: DecorationImage(
                            image: NetworkImage(widget.group!.image),
                            fit: BoxFit.cover,
                          )),
                      child: Stack(
                        fit: StackFit.expand,
                        children: [
                          Positioned(
                            bottom: 5,
                            right: -15,
                            child: Column(
                              children: [
                                widget.group!.authorId == _userId
                                    ? RawMaterialButton(
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      UpdateGroupPage(
                                                        group: widget.group
                                                            as Group,
                                                      )));
                                        },
                                        shape: CircleBorder(),
                                        fillColor: Colors.white,
                                        //child: Icon( Icons.share , color: Theme.of(context).primaryColor,),
                                        child: Icon(Icons.settings,
                                            semanticLabel: 'Actualizar',
                                            color:
                                                Theme.of(context).primaryColor),
                                      )
                                    : Offstage(),
                                /* RawMaterialButton(
                                  onPressed: () {
                                    print("Icon Share Button");
                                  },
                                  shape: CircleBorder(),
                                  fillColor: Colors.white,
                                  //child: Icon( Icons.share , color: Theme.of(context).primaryColor,),
                                  child: Icon(FontAwesome.forward,
                                      color: Theme.of(context).primaryColor),
                                ),
                                RawMaterialButton(
                                  onPressed: () {
                                    print("Icon Love Button");
                                  },
                                  shape: CircleBorder(),
                                  fillColor: Colors.white,
                                  //child: Icon( Icons.thumb_up , color: Theme.of(context).primaryColor,),
                                  child: Icon(FontAwesome.heart_empty,
                                      color: Theme.of(context).primaryColor),
                                ), */
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    /*====================================*/
                    /* Event title */
                    /*====================================*/
                    Container(
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          widget.title as String,
                          style: Theme.of(context).textTheme.headline1,
                          textScaleFactor: 0.7,
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    /*====================================*/
                    /* Event Place and Time */
                    /*====================================*/
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2.0),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 4.0, 8.0, 4.0),
                                  child: Icon(
                                    Icons.people,
                                    color: Theme.of(context)
                                        .colorScheme
                                        .secondaryVariant,
                                  ),
                                ),
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 5.0),
                                        child: Text(widget.members as String),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    /*====================================*/
                    /* Event Info */
                    /*====================================*/
                    Divider(),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Más Información',
                            style: Theme.of(context).textTheme.headline4,
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(widget.group!.description)),
                    Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 15.0, vertical: 8.0),
                            child: TextButton(
                                onPressed: () {
                                  _showDialog();
                                },
                                child: Text("Unete".toUpperCase())),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(),
    );
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Unirse al grupo"),
          content: Text("¿Te quieres unir?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                primary: Colors.white,
              ),
              child: Text("Cerrar"),
              //color: Theme.of(context).buttonTheme.colorScheme.primary,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text("Unirse"),
              onPressed: () async {
                print("Hello There!");
                Navigator.pop(context);
                await _joinGroup();
              },
            ),
          ],
        );
      },
    );
  }

  Column buildComment(BuildContext context, String name, String initials) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: CircleAvatar(
                      radius: 30.0,
                      backgroundColor: const Color(0xFF778899),
                      child: Text(initials),
                    ),
                  ),
                  Expanded(
                      flex: 5,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            name,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          Text("Fraccionamiento Arboledas",
                              style: TextStyle(
                                  fontWeight: FontWeight.w200,
                                  fontSize: 12.0,
                                  color: Colors.black45)),
                        ],
                      )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 17.5, bottom: 2.0, right: 2.0, left: 2.0),
                child: Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit mi quam, at suscipit erat consequat at. Pellentesque mauris justo, tincidunt vitae gravida vel.',
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                children: [
                  Expanded(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        FontAwesome.heart_empty,
                        color: Theme.of(context).primaryColor,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text("Me gusta"),
                      )
                    ],
                  )),
                  Expanded(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        FontAwesome.commenting_o,
                        color: Theme.of(context).primaryColor,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text("Comentar"),
                      )
                    ],
                  )),
                ],
              )
            ],
          ),
        ),
        Divider(
          color: Colors.black38,
          height: 1,
        ),
      ],
    );
  }
}
