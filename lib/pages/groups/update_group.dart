import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:http/http.dart' as http;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/pages/groups/closed_group.dart';
import 'package:tvcino/utils/custom_functions.dart';

class UpdateGroupPage extends StatefulWidget {
  final Group group;

  UpdateGroupPage({
    required this.group,
  });

  @override
  _UpdateGroupPageState createState() => _UpdateGroupPageState();
}

class _UpdateGroupPageState extends State<UpdateGroupPage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  /// ==============================
  /// Images
  /// ==============================
  late File _image;
  final picker = ImagePicker();
  TextEditingController _imageTextController = TextEditingController();
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  //===============================*/
  // Controlls
  //===============================*/
  final GlobalKey<FormState> _groupUpdateKey =
      GlobalKey<FormState>(debugLabel: 'groupUpdateKey');
  late TextEditingController _titleController;
  late TextEditingController _shortController;
  bool _errorText = false;
  //
  String uuid = "";
  String title = "";
  String shortDescription = "";
  String description = "";
  String userId = "";
  String neighborhoodId = "";

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getUserInformation();
    _titleController = TextEditingController(text: widget.group.name);
    _shortController = TextEditingController(text: widget.group.description);
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
    //print("The user Token is = $token");
  }

  /// =============================================
  /// Function
  /// UPDATE GROUP IN THE TVCINO DB
  /// =============================================
  Future<bool> _updateGroup() async {
    print("TV - ADD GROUP TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    uuid = preferences.getString('uuid') as String;
    title = _titleController.text;
    shortDescription = _shortController.text;
    description = _shortController.text;
    userId = preferences.getString('userId') as String;
    neighborhoodId = preferences.getString('neighborhoodId') as String;

    String _fileName = _imageTextController.text.split('/').last;

    String url = CustomFunctions.url + "/groups/${widget.group.id}";

    try {
      Map<String, String> datos = Map<String, String>();

      shortDescription = shortDescription.replaceAll(RegExp('"'), '\'');
      description = description.replaceAll(RegExp('"'), '\'');

      datos["data"] =
          '{"name": "$title", "short_description": "$shortDescription","description": "$description"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("PUT", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;
      if (_image != null) {
        //============================================/
        /// CHECK IF THE IMAGE IS AVAILABLE
        //============================================/
        request.files.add(http.MultipartFile.fromBytes(
            'image', File(_image.path).readAsBytesSync(),
            filename: _fileName));
      }
      request.send().then((response) async {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - GROUP Updated OK");
          String stringResponse = await response.stream.bytesToString();
          Group updatedGroup = Group.fromJson(jsonDecode(stringResponse));
          Navigator.popUntil(context, ModalRoute.withName('/groups'));
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ClosedGroup(
                        group: updatedGroup,
                        id: 1,
                        image: updatedGroup.image,
                        members: "0",
                        title: updatedGroup.name,
                      )));
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV HTTP ERROR - Status Code ${response.statusCode}");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }

    return false;
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print("TV: Image selected");
        print(_image);
        //_imageTextController.text = _image.path;
        _imageTextController.text = _image.path.split('/').last;
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /***********/
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Actualizar Grupo",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información general",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            Form(
                              key: _groupUpdateKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: _titleController,
                                    decoration:
                                        InputDecoration(labelText: "Nombre"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar tu nombre";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con un nombre más largo";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  TextField(
                                    maxLines: 6,
                                    controller: _shortController,
                                    decoration: InputDecoration(
                                      hintText: "Agrega una descripción aquí",
                                      errorText: _errorText
                                          ? "* No olvides llenar este campo"
                                          : null,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  TextFormField(
                                    decoration: InputDecoration(
                                      labelText: "Imagen",
                                      suffixIcon: IconButton(
                                          icon: Icon(Icons.file_upload),
                                          onPressed: () {
                                            getImage();
                                          }),
                                    ),
                                    controller: _imageTextController,
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Actualizar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    onPressed: () {
                                      if (_groupUpdateKey.currentState!
                                          .validate()) {
                                        if (_shortController.text.isNotEmpty) {
                                          print("TV - VALIDATE TRUE");
                                          setState(() {
                                            _errorText = false;
                                          });
                                          _updateGroup();
                                        } else {
                                          setState(() {
                                            _errorText = true;
                                          });
                                          print("TV - VALIDATE MISS CONTENT");
                                        }
                                      } else {
                                        print("TV - VALIDATE FALSE");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 2.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Borrar el Grupo",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                backgroundColor: Colors.red,
                              ),
                              child: Text(
                                "Borrar",
                                style: TextStyle(fontSize: 20.0),
                              ),
                              onPressed: () {
                                _showDialog();
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Borrar"),
          content: Text("¿Realmente quieres borrar el grupo?"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            TextButton(
              child: Text("Cerrar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text("Aceptar"),
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
                primary: Colors.white,
              ),
              onPressed: () async {
                Navigator.pop(context);
                await _deleteGroup();
              },
            ),
          ],
        );
      },
    );
  }

  /// =============================================
  /// Function
  /// update post in the TVCINO DB
  /// =============================================
  Future<bool> _deleteGroup() async {
    print("TV - ADD POST TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    String url = CustomFunctions.url + "/groups/${widget.group.id}";

    try {
      token = await _user.getIdToken();
      //print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("DELETE", postUri);
      request.headers['authorization'] = "$token";

      request.send().then((response) async {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - POST DELETED ALL OK");
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/home', (Route<dynamic> route) => false);
          Navigator.pushNamed(context, '/groups');
          return true;
        } else {
          //============================================/
          /// SOMETHING WENT WRONG
          //============================================/
          print("TV something went wrong ${response.statusCode}");
          String responseStr = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $responseStr");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV EXCEPTION - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - No Internet connection";
      });
      return false;
    } on HttpException {
      print("TV EXCEPTION - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Couldn't find the post";
      });
      return false;
    } on FormatException {
      print("TV EXCEPTION - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "TV EXCEPTION - Bad response format";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }
    return false;
  }
}
