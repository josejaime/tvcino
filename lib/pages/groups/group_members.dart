import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class GroupUsers extends StatefulWidget {
  final Group group;

  GroupUsers({
    required this.group,
  });

  @override
  _GroupUsersState createState() => _GroupUsersState();
}

class _GroupUsersState extends State<GroupUsers> {
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  late Dio dio;
  List<InterUser> _listOfInterUsers = List<InterUser>.empty(growable: true);
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;

  late SharedPreferences preferences;
  late String token;

  @override
  initState() {
    _user = instance.currentUser as auth.User;
    getMembers();
    super.initState();
  }

  /// ==============================
  /// Get posts from server
  /// ==============================
  getMembers() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    bool safeCall = true;

    String token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";

    Response response = await dio
        .get('/groups/${widget.group.id}/members')
        .catchError((onError) {
      print("ERROR DOING THE CALL ${onError.toString()}");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            _listOfInterUsers.add(InterUser.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  /// =============================================
  /// Function
  /// Add user to the group
  /// =============================================
  Future<bool> _removeFromGroup(String userId, int index) async {
    print("TV - Remove user $userId at index $index from group");

    setState(() {
      _isLoading = true;
      _message = "";
    });

    String url =
        CustomFunctions.url + "/groups/${widget.group.id}/users/$userId";
    token = await _user.getIdToken();

    try {
      var postUri = Uri.parse(url);
      print("TV - REMOVE FROM GROUP URL $url");
      await http.delete(postUri, headers: <String, String>{
        //'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': '$token',
      }).then((response) {
        if (response.statusCode == 200) {
          //============================================/
          /// 200 is everything ok
          /// User was added to the group members
          //============================================/
          setState(() {
            _showDialogWithMessage(
                context, "El usuario fue removido del grupo");
            _isLoading = false;
            _message = "";
            _listOfInterUsers.removeAt(index);
          });

          print("TV - REMOVED TO GROUP");
          /*Navigator.of(context).pushNamedAndRemoveUntil(
              '/groups', (Route<dynamic> route) => false);*/
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV HTTP ERROR - Status Code ${response.body}");
          print("TV HTTP ERROR - Status Code ${response.headers}");
          /* String strResponse = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $strResponse"); */
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _isLoading = false;
            _showDialogWithMessage(context, _message);
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
        _showDialogWithMessage(context, _message);
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
        _showDialogWithMessage(context, _message);
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
        _showDialogWithMessage(context, _message);
      });
      return false;
    }

    return false;
  }

  void _showDialogWithMessage(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("tuvcino"),
          content: Text(message),
          actions: [
            TextButton(
              child: Text('Aceptar'),
              //color: Theme.of(context).buttonTheme.colorScheme.secondary,
              onPressed: () {
                //Just dismiss the alert
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      //backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
              ),
            )
          : SingleChildScrollView(
              child: Column(
              children: [
                Align(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 12.5, horizontal: 8.5),
                    child: Column(
                      children: [
                        Text(
                          "Miembros del grupo",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .headline4!
                                  .fontSize,
                              fontFamily: Theme.of(context)
                                  .textTheme
                                  .headline4!
                                  .fontFamily),
                        ),
                        Text(
                          "${widget.group.name}",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              //color: Theme.of(context).primaryColor,
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .headline3!
                                  .fontSize,
                              fontFamily: Theme.of(context)
                                  .textTheme
                                  .headline3!
                                  .fontFamily),
                        ),
                      ],
                    ),
                  ),
                ),
                _error
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: 28.0,
                            color: Colors.black,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(_message, style: TextStyle(color: Colors.black)),
                        ],
                      )
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: _listOfInterUsers.isNotEmpty
                              ? _listOfInterUsers.map((e) {
                                  int idx = _listOfInterUsers.indexOf(e);
                                  return buildUser(e, idx);
                                }).toList()
                              : [
                                  Text(
                                    "No hay nada para mostrar aún",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                        ),
                      ),
              ],
            )),
    );
  }

  GestureDetector buildUser(InterUser user, int index) {
    return GestureDetector(
      onTap: () {
        print("Gesture detector");
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10), // if you need this
          /* side: BorderSide(
                    color: Colors.green,
                    width: 1,
                  ), */
        ),
        elevation: 2.0,
        child: Container(
          padding: EdgeInsets.all(15.0),
          width: MediaQuery.of(context).size.width * 0.9,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        user.name,
                        style: Theme.of(context).primaryTextTheme.headline4,
                      ),
                    ),
                    IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () async {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("tuvcino"),
                                content: Text(
                                    "¿Realmente quieres quitar a este usuario del grupo?"),
                                actions: [
                                  TextButton(
                                    child: Text('Cancelar'),
                                    onPressed: () {
                                      //Just dismiss the alert
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  TextButton(
                                    child: Text('Borrar'),
                                    style: TextButton.styleFrom(
                                      primary: Colors.white,
                                      backgroundColor: Colors.red,
                                    ),
                                    onPressed: () async {
                                      //Just dismiss the alert
                                      print('remove user at index $index');
                                      Navigator.of(context).pop();
                                      await _removeFromGroup(user.id, index);
                                    },
                                  ),
                                ],
                              );
                            },
                          );
                        }),
                  ],
                ),
              ),
              SizedBox(
                child: Divider(
                  color: Theme.of(context).colorScheme.secondary,
                ),
                height: 10.0,
              ),
              user.phoneNumber == null || user.phoneNumber == "null"
                  ? buildRowUser('Teléfono:', 'N/A')
                  : buildRowUser('Teléfono:', user.phoneNumber),
              user.email != null
                  ? buildRowUser('Email:', user.email as String)
                  : buildRowUser('Email:', 'N/A'),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildRowUser(String label, String text) {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              label,
              style: Theme.of(context).textTheme.headline6,
            ),
            Text(text),
          ],
        ),
      ],
    );
  }
}
