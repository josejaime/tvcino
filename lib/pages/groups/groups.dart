import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/components/tcvino_bottom_bar.dart';
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/components/tvcino_floating_button.dart';
import 'package:tvcino/pages/alerts/map.dart';
import 'package:tvcino/pages/alerts/my_alert_bottom.dart';
import 'package:tvcino/pages/groups/add_group.dart';
import 'package:tvcino/pages/groups/closed_group.dart';
import 'package:tvcino/pages/groups/group.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;

class GroupsPage extends StatefulWidget {
  @override
  _GroupsPageState createState() => _GroupsPageState();
}

class _GroupsPageState extends State<GroupsPage> {
  /// ==============================
  /// DIO GET THE POSTS
  /// ==============================
  late Dio dio;
  String _message = "";
  bool _isLoading = false;
  bool _error = false;

  /// ==============================
  /// List Of The Groups
  /// ==============================
  List<Group> _listOfGroups = List<Group>.empty(growable: true);
  List<Group> _listOfMyGroups = List<Group>.empty(growable: true);
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;
  String inAppMessage = "";
  String inAppTitle = "";
  late FirebaseMessaging messaging;
  bool _showNotification = false;
  List<String> choices = <String>[
    "Atraco",
    "Aviso",
  ];
  late String dataTitle;
  late String dataBody;
  late double latitude;
  late double longitude;

  /// ==============================
  /// Get groups from server
  /// ==============================
  _getGroups() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String neighborhoodId = preferences.getString('neighborhoodId') as String;
    String userId = preferences.getString('userId') as String;

    Response response = await dio
        .get(
            '/groups?start=0&limit=100&user_id=$userId&member=false&neighborhood_id=$neighborhoodId')
        .catchError((onError) {
      print("ERRORORORORORO");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            //print(data[i]);
            //_listOfUsers.add(InterUser.fromJson(_data[i]));
            _listOfGroups.add(Group.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            //_error = true;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  /// ==============================
  /// Get groups from server
  /// ==============================
  _getMyGroups() async {
    setState(() {
      _isLoading = true;
      _message = "";
      _error = false;
    });

    /// ==============================
    /// Later maybe empty the post list ¿?
    /// ==============================

    dio = CustomFunctions.getDio();
    token = await _user.getIdToken();
    dio.options.headers["authorization"] = "$token";
    bool safeCall = true;
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String userId = preferences.getString('userId') as String;

    Response response = await dio
        .get('/groups?start=0&limit=100&user_id=$userId&member=true')
        .catchError((onError) {
      print("ERRORORORORORO");
      safeCall = false;
    });

    if (safeCall) {
      if (response.statusCode != 200) {
        print("TV DIO ERROR - Status Code ${response.statusCode}");
        print("TV DIO ERROR - Status Message ${response.statusMessage}");

        setState(() {
          _isLoading = false;
          _error = true;
          _message =
              "Hubo un error en el servidor regresa e intenta nuevamente";
        });
      } else {
        print("DIO Status Code ${response.statusCode}");
        print("DIO - Response RAW Data");
        print(response.data['items']);
        print("DIO Trying to convert data");
        List data = response.data['items'];
        print("DIO SIZE RESPONSE ${data.length}");
        if (data.length > 0) {
          for (int i = 0; i < data.length; i++) {
            print('TV - My Groups ${data[i]['name']}');
            //_listOfUsers.add(InterUser.fromJson(_data[i]));
            _listOfMyGroups.add(Group.fromJson(data[i]));
          }
          setState(() {
            _isLoading = false;
            _error = false;
          });
        } else {
          setState(() {
            _message = "Lo sentimos, no hay nada que ver aquí por ahora";
            _isLoading = false;
            _error = false;
          });
        }
      }
    } else {
      setState(() {
        _isLoading = false;
        _error = true;
        _message = "Hubo un error en el servidor, intenta más tarde";
      });
    }
  }

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    initFirebase();
    _getGroups();
    _getMyGroups();
    super.initState();
  }

  void initFirebase() {
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value) {
      print(value);
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      handleNotification(event);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('On Background push notification clicked!');
      handleNotification(message);
    });
  }

  handleNotification(RemoteMessage event) {
    setState(() {
      print("Message recieved");
      Map<String, dynamic> data = event.data;
      print(event.notification!.title);
      dataTitle = data['title'] as String;
      dataBody = data['body'] as String;
      print("The data body is");
      print(dataBody);
      print("The data title is");
      print(dataTitle);

      List<String> stringData = dataBody.split(',');
      latitude = double.parse(stringData[0]);
      longitude = double.parse(stringData[1]);

      print('Latitude: $latitude');
      print('Longitude: $longitude');
      inAppMessage = "Mira la ubicación dando click en el botón";
      inAppTitle = "Alguien lanzó una alerta";
      _showNotification = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
          title: Image(
            image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
            height: 21.0,
          ),
          centerTitle: true,
          //backgroundColor: Colors.white,
          backgroundColor: Theme.of(context).primaryColor,
          //iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
          actions: <Widget>[
            IconButton(
              iconSize: 45.0,
              onPressed: () {
                showModalBottomSheet(
                  backgroundColor: Theme.of(context).primaryColor,
                  isDismissible: true,
                  isScrollControlled: true,
                  context: context,
                  builder: (_) => MyBottomSheet(),
                );
              },
              icon: Image.asset('assets/iconos/emergencia_blanco.png'),
            ),
          ]),
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Theme.of(context).primaryColor,
              ),
            )
          : _error
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.error_outline,
                        size: 28.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(_message),
                    ],
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    _showNotification
                        ? Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            elevation: 2.0,
                            margin: EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 10.0),
                            child: Container(
                              padding: EdgeInsets.all(15.0),
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            inAppTitle,
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .headline4,
                                          ),
                                        ),
                                        IconButton(
                                            icon: Icon(Icons.close),
                                            onPressed: () {
                                              setState(() {
                                                _showNotification = false;
                                              });
                                            })
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 8.0,
                                  ),
                                  Text(inAppMessage),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: TextButton(
                                      onPressed: () {
                                        print('Latitude: $latitude');
                                        print('Longitude: $longitude');
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => AlertMap(
                                                      latitude: latitude,
                                                      longitude: longitude,
                                                    )));
                                      },
                                      child:
                                          Text('Revisar alerta'.toUpperCase()),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Offstage(),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: Text(
                        "Tus grupos",
                        style: Theme.of(context).textTheme.headline3,
                      ),
                    ),
                    _listOfMyGroups.isNotEmpty
                        ? Container(
                            height: 200,
                            padding: EdgeInsets.symmetric(
                                horizontal: 7.5, vertical: 5.0),
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: _listOfMyGroups.map((e) {
                                return buildMyGroup(
                                  context,
                                  e,
                                );
                              }).toList(),
                              /* children: [
                          _listOfMyGroups.map((e) { return Text("dsfsd");});
                          buildMyGroup(context, 'Grupo A', '65 Miembros',
                              'assets/images/tvcino_grupo_6.jpg'),
                          SizedBox(
                            width: 10.0,
                          ),
                          buildMyGroup(context, 'Grupo B', '25 Miembros',
                              'assets/images/tvcino_grupo_1.jpg'),
                          SizedBox(
                            width: 10.0,
                          ),
                          buildMyGroup(context, 'Grupo C', '5 Miembros',
                              'assets/images/tvcino_grupo_4.jpg'),
                          SizedBox(
                            width: 10.0,
                          ),
                          buildMyGroup(context, 'Grupo D', '45 Miembros',
                              'assets/images/tvcino_grupo_6.jpg'),
                          SizedBox(
                            width: 10.0,
                          ),
                        ], */
                            ),
                          )
                        : Container(
                            child: Text(
                              '¡No te has unido a ningun grupo todavía!',
                              style:
                                  Theme.of(context).primaryTextTheme.headline3,
                              textAlign: TextAlign.center,
                            ),
                          ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Flexible(
                      child: ListView(
                        children: [
                          Align(
                            child: Material(
                              elevation: 2.0,
                              borderRadius: BorderRadius.circular(15),
                              child: Container(
                                width: MediaQuery.of(context).size.width / 1.1,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Container(
                                  margin: EdgeInsets.all(8.0),
                                  padding: EdgeInsets.all(5.0),
                                  width:
                                      MediaQuery.of(context).size.width / 1.1,
                                  height: 155,
                                  decoration: BoxDecoration(
                                      color: Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                          image: ExactAssetImage(
                                              "assets/images/tuvcino-ilustracion-servicios.png"),
                                          alignment: Alignment.centerRight)),
                                  child: Stack(
                                    children: [
                                      Positioned(
                                        top: 15,
                                        left: 5,
                                        //child: Text("Novedades", style: Theme.of(context).textTheme.headline2 ),
                                        child: Text("¡Conoce los \nGrupos!",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 26)),
                                      ),
                                      Positioned(
                                          bottom: 1,
                                          left: 25,
                                          child: TextButton(
                                              style: TextButton.styleFrom(
                                                backgroundColor: Colors.white,
                                                primary: Colors.black,
                                              ),
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            AddGroupPage()));
                                              },
                                              child: Text(
                                                'Crear ahora',
                                              )))
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          _listOfGroups.isNotEmpty
                              ? _buildGroupFromGet(_listOfGroups)
                              : Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      children: [
                                        Icon(
                                          Icons.error_outline,
                                          size: 28.0,
                                        ),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        Text(
                                            "No hay grupos aquí por el momento"),
                                        SizedBox(
                                          height: 10.0,
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                          /* buildGroup(context, 'Nombre del grupo', 30,
                              'assets/images/tvcino_grupo_5.jpg'),
                          buildGroup(context, 'Nombre del grupo', 42,
                              'assets/images/tvcino_grupo_4.jpg'),
                          buildGroup(context, 'Nombre del grupo', 20,
                              'assets/images/tvcino_grupo_2.jpg'),
                          buildGroup(context, 'Nombre del grupo', 35,
                              'assets/images/tvcino_grupo_3.jpg'), */
                        ],
                      ),
                    ),
                  ],
                ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: TvcinoFloatingActionButton(),
      bottomNavigationBar: TvcinoBottomAppBar(
        disabledButton: 2,
      ),
    );
  }

  Widget _buildGroupFromGet(List<Group> groups) {
    return Column(
      children: groups.map((e) {
        return buildGroup(context, e);
      }).toList(),
    );
  }

  Widget buildGroup(BuildContext context, Group element) {
    return Align(
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              /*Navigator.push(context, MaterialPageRoute(builder: (context) => GroupPage(
                id: 1,
                image: image,
                title: title,
              )));*/
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ClosedGroup(
                            id: 1,
                            image: element.image,
                            title: element.name,
                            members: element.totalMembers.toString(),
                            group: element,
                          )));
            },
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              clipBehavior: Clip.hardEdge,
              padding: EdgeInsets.all(2.0),
              decoration: BoxDecoration(
                  //color: Colors.indigo.withOpacity(0.5),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15.0)),
              child: Row(
                children: [
                  Container(
                    width: 145,
                    height: 135,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        //image: AssetImage(image),
                        image: NetworkImage(element.image),
                        fit: element.image != null
                            ? BoxFit.cover
                            : BoxFit.contain,
                        alignment: Alignment.center,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 3.0, horizontal: 10.0),
                          child: Text(element.name,
                              style: Theme.of(context).textTheme.headline5),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 10.0),
                          child: Row(
                            children: [
                              Icon(
                                Icons.people,
                                size: 16.0,
                                color: Theme.of(context).primaryColor,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: Text(element.totalMembers.toString()),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ClosedGroup(
                                                id: 1,
                                                image: element.image,
                                                title: element.name,
                                                members: element.totalMembers
                                                    .toString(),
                                                group: element,
                                              )));
                                },
                                child: Text("Saber más")),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10.0,
          )
        ],
      ),
    );
  }

  Align buildMyGroup(BuildContext context, Group group) {
    return Align(
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => GroupPage(
                group: group,
              ),
            ),
          );
        },
        child: Container(
          height: 180,
          margin: EdgeInsets.only(right: 10.0),
          width: MediaQuery.of(context).size.width / 2,
          decoration: BoxDecoration(
            /* image: DecorationImage(
                image: ExactAssetImage(image),
                fit: BoxFit.contain,
                alignment: Alignment.topCenter), */
            image: DecorationImage(
              image: NetworkImage(group.image),
              fit: group.image != null ? BoxFit.cover : BoxFit.contain,
              alignment: Alignment.topCenter,
            ),
            borderRadius: BorderRadius.circular(15.0),
            color: Theme.of(context).primaryColor,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.25),
                spreadRadius: 3,
                blurRadius: 3,
                offset: Offset(2, 4),
              ),
            ],
          ),
          child: Stack(
            children: [
              Positioned(
                  bottom: 0,
                  width: MediaQuery.of(context).size.width / 2,
                  child: Container(
                      padding: EdgeInsets.all(10.0),
                      height: 80,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.5),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 3.5),
                            child: Text(
                              group.name,
                              style: Theme.of(context).textTheme.subtitle1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          //Text(group.totalMembers.toString()),
                        ],
                      ))),
              Positioned(
                  right: 2.5,
                  bottom: 58,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(50.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.25),
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: Offset(1, 2),
                        ),
                      ],
                    ),
                    child: IconButton(
                      color: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => GroupPage(
                              group: group,
                            ),
                          ),
                        );
                      },
                      icon: Icon(Icons.send),
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
