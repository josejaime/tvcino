import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/group.dart';
import 'package:tvcino/components/tvcino_app_bar.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:http/http.dart' as http;
import 'package:tvcino/components/tvcino_circular_loading.dart';
import 'package:tvcino/utils/custom_functions.dart';

class AddGroupPage extends StatefulWidget {
  @override
  _AddGroupPageState createState() => _AddGroupPageState();
}

class _AddGroupPageState extends State<AddGroupPage> {
  /// ==============================
  /// FIREBASE AUTH VARIABLES
  /// ==============================
  auth.FirebaseAuth instance = auth.FirebaseAuth.instance;
  late auth.User _user;
  late String token;

  /// ==============================
  /// Images
  /// ==============================
  File? _image;
  final picker = ImagePicker();
  TextEditingController _imageTextController = TextEditingController();
  //===============================*/
  // Messages
  //===============================*/
  bool _error = false;
  bool _isLoading = false;
  String _message = "";
  //===============================*/
  // Controlls
  //===============================*/
  final GlobalKey<FormState> _groupAddKey =
      GlobalKey<FormState>(debugLabel: 'groupAddKey');
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _shortController = TextEditingController();
  bool _errorText = false;
  //
  String uuid = "";
  String title = "";
  String shortDescription = "";
  String description = "";
  String userId = "";
  String neighborhoodId = "";

  @override
  void initState() {
    _user = instance.currentUser as auth.User;
    getUserInformation();
    super.initState();
  }

  /// ==============================
  /// Get current user from firebase
  /// ==============================
  getUserInformation() async {
    token = await _user.getIdToken();
    print("The user Token is = $token");
  }

  /// =============================================
  /// Function
  /// Add user to the TVCINO DB
  /// =============================================
  Future<bool> _addGroup() async {
    print("TV - ADD GROUP TO DB");

    setState(() {
      _error = false;
      _isLoading = true;
      _message = "";
    });

    SharedPreferences preferences = await SharedPreferences.getInstance();
    uuid = preferences.getString('uuid') as String;
    title = _titleController.text;
    shortDescription = _shortController.text;
    description = _shortController.text;
    userId = preferences.getString('userId') as String;
    neighborhoodId = preferences.getString('neighborhoodId') as String;

    String _fileName = _imageTextController.text.split('/').last;

    String url = CustomFunctions.url + "/groups/";

    try {
      Map<String, String> datos = Map<String, String>();

      shortDescription = shortDescription.replaceAll(RegExp('"'), '\'');
      description = description.replaceAll(RegExp('"'), '\'');

      datos["data"] =
          '{"name": "$title", "short_description": "$shortDescription","description": "$description", "author_id": "$userId", "main_neighborhood_id": "$neighborhoodId"}';
      print("TV - DATA");
      print(datos.toString());
      print("TV - TOKEN");
      token = await _user.getIdToken(true);
      print(token);

      var postUri = Uri.parse(url);
      var request = http.MultipartRequest("POST", postUri);
      request.headers['authorization'] = "$token";
      request.fields['data'] = datos['data'] as String;
      if (_image != null) {
        //============================================/
        /// CHECK IF THE IMAGE IS AVAILABLE
        //============================================/
        request.files.add(http.MultipartFile.fromBytes(
            'image', File(_image!.path).readAsBytesSync(),
            filename: _fileName));
      }
      request.send().then((response) async {
        if (response.statusCode == 200) {
          print("Uploaded!");
          //============================================/
          /// 200 is everything ok
          /// Set preferences neighborhoodSet = true
          //============================================/
          setState(() {
            _isLoading = false;
            _error = false;
            _message = "";
          });

          print("TV - GROUP CREATED ALL OK");

          ///============================================
          /// Add user to the group
          ///============================================
          final responseString = await response.stream.bytesToString();
          Group createdGroup = Group.fromJson(jsonDecode(responseString));

          String joinGroup =
              CustomFunctions.url + "/groups/${createdGroup.id}/users/$userId";

          var postUriJoinGroup = Uri.parse(joinGroup);
          print("TV - ADD GROUP URL $joinGroup");
          await http.post(postUriJoinGroup, headers: <String, String>{
            //'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': '$token',
          }).then((response) {
            if (response.statusCode == 200) {
              //============================================/
              /// 200 is everything ok
              /// User was added to the group members
              //============================================/
              setState(() {
                _isLoading = false;
                _message = "";
              });

              print("TV - ADDED TO GROUP");
              /*Navigator.of(context).pushNamedAndRemoveUntil(
              '/groups', (Route<dynamic> route) => false);*/
              return true;
            } else {
              print("TV something went wrong ${response.statusCode}");
              print("TV HTTP ERROR - Status Code ${response.body}");
              print("TV HTTP ERROR - Status Code ${response.headers}");
              /* String strResponse = await response.stream.bytesToString();
          print("TV HTTP ERROR - Status Code $strResponse"); */
              setState(() {
                /* _message = "Hubo un error en el servidor intenta nuevamente";
                _isLoading = false; */
              });
            }
          });

          ///============================================
          /// End add user to the group
          ///============================================

          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.pushNamed(context, '/groups');
          /*Navigator.of(context).pushNamedAndRemoveUntil(
              '/groups', (Route<dynamic> route) => false);*/
          return true;
        } else {
          print("TV something went wrong ${response.statusCode}");
          print("TV HTTP ERROR - Status Code ${response.statusCode}");
          setState(() {
            _message = "Hubo un error en el servidor intenta nuevamente";
            _error = true;
            _isLoading = false;
          });
        }
      });
    } on SocketException {
      print('TV SocketException - No Internet connection');
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "Parece que falta la conexión a internet, verifica tu conexión";
      });
      return false;
    } on HttpException {
      print("TV HttpException - Couldn't find the post");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "Lo sentimos no podemos conectarnos con el servidor";
      });
      return false;
    } on FormatException {
      print("TV FormatException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message = "El formato de envio no es el correcto, intenta más tarde";
      });
      return false;
    } on FileSystemException {
      print("TV FileSystemException - Bad response format");
      setState(() {
        _error = true;
        _isLoading = false;
        _message =
            "El archivo de imagen no se puede cargar, intenta con uno distinto";
      });
      return false;
    }

    return false;
  }

  Future getImage() async {
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print("TV: Image selected");
        print(_image);
        //_imageTextController.text = _image.path;
        _imageTextController.text = _image!.path.split('/').last;
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /***********/
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: TvcinoAppBar(
        color: Theme.of(context).primaryColor,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: _isLoading
          ? Center(
              child: TvcinoCircularProgressBar(
                color: Colors.white,
                textColor: Colors.white,
              ),
            )
          : SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 1.5,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Text(
                          "Nuevo Grupo",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      _error
                          ? Container(
                              child: Text(
                                _message,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: Theme.of(context)
                                        .textTheme
                                        .headline5!
                                        .fontSize),
                              ),
                            )
                          : Container(),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.97,
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: Column(
                          children: [
                            Text(
                              "Información general",
                              style: Theme.of(context).textTheme.headline4,
                            ),
                            _image != null
                                ? Column(
                                    children: [
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.95,
                                        height: 150,
                                        child: Image.file(
                                          _image!,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          "* Puede cambiar la forma de verse al guardar",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w100,
                                              fontSize: 10.0,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                    ],
                                  )
                                : Column(
                                    children: [
                                      GestureDetector(
                                        onTap: getImage,
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.95,
                                          height: 150,
                                          child: Image.asset(
                                              'assets/images/no_image_placeholder.png'),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          "* Puede cambiar la forma de verse al guardar",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w100,
                                              fontSize: 10.0,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                    ],
                                  ),
                            Form(
                              key: _groupAddKey,
                              child: Column(
                                children: [
                                  TextFormField(
                                    controller: _titleController,
                                    decoration:
                                        InputDecoration(labelText: "Nombre"),
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes ingresar tu nombre";
                                      }
                                      if (value.length < 4) {
                                        return "* Intenta con un nombre más largo";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  TextField(
                                    maxLines: 6,
                                    controller: _shortController,
                                    decoration: InputDecoration(
                                      hintText: "Agrega una descripción aquí",
                                      errorText: _errorText
                                          ? "* No olvides llenar este campo"
                                          : null,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20.0,
                                  ),
                                  TextFormField(
                                    onTap: () {
                                      getImage();
                                    },
                                    decoration: InputDecoration(
                                      labelText: "Imagen",
                                      suffixIcon: Icon(Icons.file_upload),
                                    ),
                                    controller: _imageTextController,
                                    readOnly: true,
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return "* Debes poner una imagen";
                                      }
                                      return null;
                                    },
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TextButton(
                                    child: Text(
                                      "Publicar",
                                      style: TextStyle(fontSize: 20.0),
                                    ),
                                    onPressed: () {
                                      if (_groupAddKey.currentState!
                                          .validate()) {
                                        if (_shortController.text.isNotEmpty) {
                                          print("TV - VALIDATE TRUE");
                                          setState(() {
                                            _errorText = false;
                                          });
                                          _addGroup();
                                        } else {
                                          setState(() {
                                            _errorText = true;
                                          });
                                          print("TV - VALIDATE MISS CONTENT");
                                        }
                                      } else {
                                        print("TV - VALIDATE FALSE");
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
