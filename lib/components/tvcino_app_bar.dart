import 'package:flutter/material.dart';
//import 'package:tvcino/utils/theme.dart';

List<String> choices = <String>[
  "Atraco",
  "Aviso",
];

class TvcinoAppBar extends AppBar {
  TvcinoAppBar({
    Key? key,
    Widget? title,
    actions,
    Color? color,
  }) : super(
          key: key,
          title: Image(
            image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
            height: 21.0,
          ),
          centerTitle: true,
          elevation: 2.0,
          /* actions: [
            PopupMenuButton(
                icon: Image.asset('assets/iconos/emergencia_blanco.png'),
                onSelected: (String choice) {
                  print("Selected == $choice");
                },
                padding: EdgeInsets.zero,
                itemBuilder: (BuildContext context) {
                  return choices.map((String choice) {
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Row(
                        children: [
                          Icon(Icons.settings, color: Colors.black),
                          Text(choice),
                        ],
                      ),
                    );
                  }).toList();
                }),
          ], */
          backgroundColor: color,
          brightness: Brightness.dark,
        );
}
