import 'package:flutter/material.dart';

class TvcinoCircularProgressBar extends StatelessWidget {
  final Color? color;
  final Color? textColor;

  const TvcinoCircularProgressBar({Key? key, this.color, this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircularProgressIndicator(
            backgroundColor: color,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            "Cargando",
            style: TextStyle(
              color: textColor,
            ),
          ),
        ]);
  }
}
