import 'package:flutter/material.dart';
import 'package:tvcino/pages/posts/add_post.dart';

class TvcinoFloatingActionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => AddPostPage()));
        /* Navigator.push(context,
            MaterialPageRoute(builder: (context) => RegisterZipCode())); */
      },
      /*child: Icon(
          Icons.search
        ),*/
      child: Icon(Icons.add),
      elevation: 3.0,
    );
  }
}
