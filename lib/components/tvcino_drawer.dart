import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
//import 'package:tvcino/pages/alerts/index_alerts.dart';
import 'package:tvcino/pages/neighborhoods/view_neighborhood.dart';
import 'package:tvcino/pages/posts/user_posts.dart';
//import 'package:tvcino/pages/user/admin_user.dart';
//import 'package:tvcino/pages/user/assigned_tasks.dart';
import 'package:tvcino/pages/user/created_groups.dart';
import 'package:tvcino/pages/user/favorites.dart';
import 'package:tvcino/pages/user/profile.dart';
import 'package:tvcino/utils/custom_functions.dart';
import 'package:tvcino/utils/soon.dart';
import 'package:http/http.dart' as http;

class TvcinoDrawer extends StatelessWidget {
  final auth.User user;

  TvcinoDrawer({required this.user});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(children: [
        /*Container(
              //margin: EdgeInsets.all(20.0),
              padding: 
                EdgeInsets.only( 
                  top: 15.0, left: 15.0, right: 125.0, bottom: 5.0),
              child: Image(
                //image: Image.asset("assets/splash/tuvcino_splash_logotipo_1024_M copia.png"),
                //image: ExactAssetImage('assets/splash/tuvcino_splash_logotipo_1024_M copia.png'),
                image: ExactAssetImage('assets/splash/tuvcino_logo_720_B.png'),
                alignment: Alignment.topLeft,
              ),
              color: Theme.of(context).primaryColor,
            ),*/
        UserAccountsDrawerHeader(
          currentAccountPicture: user.photoURL != null
              ? CircleAvatar(
                  radius: 50.0,
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(user.photoURL as String))
              : CircleAvatar(
                  radius: 50.0,
                  backgroundColor: Colors.white,
                  backgroundImage: ExactAssetImage('assets/user_2.png'),
                ),
          accountName: Text(
              (user.displayName != null ? user.displayName : "") as String,
              style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600)),
          accountEmail: user.email != null
              ? Text(user.email as String)
              : Text("jondoe@live.com"),
        ),
        //config
        /* ListTile(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SoonPage()));
            print("click drawer button");
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/config.png'),
          ),
          title: Text("Configuración"),
        ), */
        //servicios publicos
        /* ListTile(
          onTap: () {
            print("click drawer button");
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SoonPage()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/servicios_publicos.png'),
          ),
          title: Text("Servicios Públicos"),
        ), */
        //mi cuenta
        ListTile(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => UserProfilePage()));
          },
          leading: IconButton(
            onPressed: null,
            //icon: Image.asset('assets/iconos/usuario.png'),
            icon: Image.asset('assets/iconos/config.png'),
          ),
          title: Text("Mi cuenta"),
        ),
        ListTile(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => ViewNeighborhood()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/usuario.png'),
          ),
          title: Text("Mi Vecindario"),
        ),
        // favoritos
        ListTile(
          onTap: () {
            print("click drawer button");
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => UserFavorites()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/like.png'),
          ),
          title: Text("Favoritos"),
        ),
        // notificaciones
        /* ListTile(
          onTap: () {
            print("click drawer button");
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SoonPage()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/notificacion.png'),
          ),
          title: Text("Notificaciones"),
        ), */
        ListTile(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => UserCreatedGroups()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/grupos.png'),
          ),
          title: Text("Grupos Creados"),
        ),
        ListTile(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => UsersPosts()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/grupos.png'),
          ),
          title: Text("Posts Creados"),
        ),
        // ListTile(
        //   onTap: () {
        //     Navigator.push(context,
        //         MaterialPageRoute(builder: (context) => AssignedUserTasks()));
        //   },
        //   leading: IconButton(
        //     color: Theme.of(context).primaryColor,
        //     onPressed: () {},
        //     icon: Icon(Icons.format_list_numbered),
        //   ),
        //   title: Text("Tus tareas"),
        // ),
        // ListTile(
        //   onTap: () {
        //     Navigator.push(context,
        //         MaterialPageRoute(builder: (context) => IndexAlertsPage()));
        //   },
        //   leading: IconButton(
        //     onPressed: null,
        //     icon: Image.asset('assets/iconos/emergencia.png'),
        //   ),
        //   title: Text("Alertas"),
        // ),
        Divider(),
        // admnin
        ListTile(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SoonPage()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/administrador.png'),
          ),
          title: Text("Administrar"),
        ),
        // aiuda
        ListTile(
          onTap: () {
            print("click drawer button");
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => SoonPage()));
          },
          leading: IconButton(
            onPressed: null,
            icon: Image.asset('assets/iconos/ayuda.png'),
          ),
          title: Text("Ayuda"),
        ),
        //
        ListTile(
          onTap: () async {
            /*****************************************/
            // Call the funtion to sign out the
            // current user
            /*****************************************/
            /*bool signOut = await CustomFunctions().singOutUser();
            if (signOut) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/', (Route<dynamic> route) => false);
            }*/
            _showLogOutConfirmation(context);
          },
          leading: IconButton(
              onPressed: null,
              icon: Icon(
                Icons.exit_to_app,
                color: Theme.of(context).colorScheme.primary,
              )),
          title: Text("Salir"),
        ),
      ]),
    );
  }
}

void _showLogOutConfirmation(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text("Salir"),
        content: Text("¿Realmente quieres salir?"),
        actions: [
          TextButton(
            child: Text('Regresar'),
            //color: Theme.of(context).buttonTheme.colorScheme.secondary,
            onPressed: () {
              //Just dismiss the alert
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            style: TextButton.styleFrom(
              primary: Colors.white,
              backgroundColor: Colors.red,
            ),
            child: Text('Sí, Salir'),
            onPressed: () async {
              bool signOut = await CustomFunctions().singOutUser();
              if (signOut) {
                SharedPreferences preferences =
                    await SharedPreferences.getInstance();
                await preferences.clear();
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/', (Route<dynamic> route) => false);
              }
            },
          ),
        ],
      );
    },
  );
}

updateUser(InterUser user, User firebaseUser) async {
  print("TV - UPDATE USER IN THE DB");
  String url = CustomFunctions.url + "/users/${user.id}";

  try {
    Map<String, String> datos = Map<String, String>();

    datos["data"] =
        '{"name": "${user.name}", "role_id": "${user.roleId}", "device_token": null}';
    print("TV - DATA");
    print(datos.toString());
    print("TV - TOKEN");
    String token = await firebaseUser.getIdToken();
    print(token);

    var postUri = Uri.parse(url);
    var request = http.MultipartRequest("PUT", postUri);
    request.headers['authorization'] = "$token";
    request.fields['data'] = datos['data'] as String;

    await request.send().then((response) async {
      if (response.statusCode == 200) {
        //============================================/
        /// 200 is everything ok
        /// Set preferences neighborhoodSet = true
        //============================================/
        String value = await response.stream.bytesToString();
        InterUser updatedUser = InterUser.fromJson(jsonDecode(value));
        print("TV - UPDATED USER =====");
        print(updatedUser.name);
        print("TV - UPDATED USER =====");
        return true;
      } else {
        //============================================/
        /// SOMETHING WENT WRONG
        //============================================/
        print(
            "TV something went wrong updating user in the home ${response.statusCode}");
        String responseStr = await response.stream.bytesToString();
        print("TV HTTP ERROR - Status Code $responseStr");
      }
    });
  } on SocketException {
    print('TV EXCEPTION On updating user in the home - No Internet connection');
    return false;
  } on HttpException {
    print("TV EXCEPTION On updating user in the home - Couldn't find the post");
    return false;
  } on FormatException {
    print("TV EXCEPTION On updating user in the home - Bad response format");
    return false;
  } on FileSystemException {
    print(
        "TV FileSystemException On updating user in the home - Bad response format");
    return false;
  }
  return false;
}
