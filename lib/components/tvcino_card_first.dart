import 'package:flutter/material.dart';

class TvcinoExtendedCard extends StatelessWidget {
  final String? title;
  final String? image;
  final String? subtitle;
  final VoidCallback? funct;

  TvcinoExtendedCard({this.funct, this.image, this.title, this.subtitle});

  @override
  Widget build(BuildContext context) {
    return Align(
      child: GestureDetector(
        /*onTap: () {
          print("Hello from stl widget new gesture detector");
          Navigator.push(
            context, 
            MaterialPageRoute(
              builder: (context) => EventPage( id: 1, tile: title, image: image,)
            )
          );
        },*/
        onTap: funct,
        child: Column(
          children: [
            Divider(),
            Container(
              height: 245,
              width: MediaQuery.of(context).size.width / 1.1,
              decoration: BoxDecoration(
                image: DecorationImage(
                    //image: ExactAssetImage(image),
                    image: NetworkImage(image ?? ''),
                    fit: BoxFit.cover,
                    alignment: Alignment.center),
                borderRadius: BorderRadius.circular(15.0),
                color: Theme.of(context).primaryColor,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.25),
                    spreadRadius: 3,
                    blurRadius: 3,
                    offset: Offset(2, 4), // changes position of shadow
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Positioned(
                      bottom: 0,
                      width: MediaQuery.of(context).size.width / 1.1,
                      child: Container(
                          //color: Colors.white,
                          padding: EdgeInsets.all(10.0),
                          height: 95,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(12.5),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(bottom: 3.5),
                                child: Text(title as String,
                                    style:
                                        Theme.of(context).textTheme.subtitle1),
                              ),
                              Text((subtitle!.length > 74
                                  ? subtitle!.substring(0, 70) + "..."
                                  : subtitle) as String),
                            ],
                          ))),
                  Positioned(
                      right: 2.5,
                      bottom: 68,
                      //top: 5,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          /*border: Border.all(
                            color: Colors.black,
                            width: 0.1
                          ),*/
                          borderRadius: BorderRadius.circular(50.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.25),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset:
                                  Offset(1, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: IconButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: funct,
                          icon: Icon(Icons.send),
                          //icon: Image.asset('assets/iconos/enviar.png'),
                        ),
                      )),
                ],
              ),
            ),
            SizedBox(
              height: 15.0,
            )
          ],
        ),
      ),
    );
  }
}
