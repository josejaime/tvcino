import 'package:flutter/material.dart';
import 'package:tvcino/pages/search/filters.dart';
import 'package:tvcino/utils/soon.dart';

class TvcinoBottomAppBar extends StatelessWidget {
  final int? disabledButton;
  final bool? hideMiddle;

  TvcinoBottomAppBar({this.disabledButton, this.hideMiddle});

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      notchMargin: 3.0,
      elevation: 8,
      shape: CircularNotchedRectangle(),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            width: 0.1,
          ),
          IconButton(
              icon: Image.asset('assets/iconos/home.png'),
              onPressed: disabledButton == 1
                  ? null
                  : () {
                      /*Navigator.push(context,
                          MaterialPageRoute(builder: (context) => HomePage()));*/
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/home', (Route<dynamic> route) => false);
                    }),
          IconButton(
              icon: Image.asset('assets/iconos/grupos.png'),
              onPressed: disabledButton == 2
                  ? null
                  : () {
                      /* Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => GroupsPage())); */
                      Navigator.pushNamed(context, '/groups');
                    }),
          hideMiddle == true
              ? Offstage()
              : SizedBox(
                  width: 35.0,
                ),
          IconButton(
              icon: Image.asset('assets/iconos/directorio.png'),
              onPressed: disabledButton == 3
                  ? null
                  : () {
                      /* Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DirectoriesPage())); */
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => FiltersPage()));
                    }),
          IconButton(
            icon: Image.asset('assets/iconos/administrador.png'),
            onPressed: disabledButton == 4
                ? null
                : () {
                    /* Navigator.pushNamed(context, '/admin'); */
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => SoonPage()));
                  },
          ),
          SizedBox(
            width: 0.1,
          ),
        ],
      ),
    );
  }
}
