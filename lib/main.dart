import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:tvcino/pages/directories/directories.dart';
import 'package:tvcino/pages/fees/index.dart';
import 'package:tvcino/pages/groups/groups.dart';
import 'package:tvcino/pages/home.dart';
import 'package:tvcino/pages/search/filters.dart';
import 'package:tvcino/pages/tasks/index_tasks.dart';
import 'package:tvcino/pages/user/admin_user.dart';
import 'package:tvcino/pages/user/profile.dart';
import 'package:tvcino/splash/main_splash.dart';
import 'package:tvcino/utils/theme.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> _messageHandler(RemoteMessage message) async {
  print('background message ${message.notification!.body}');
}

//91562100-9ff7-4396-b422-8605492e3825 yo
//72f9bc88-cb20-4549-99e7-d02bc4f730f5 ivan
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp().whenComplete(() {
    print("TV - Firebase App Initialized");
  });
  FirebaseMessaging.onBackgroundMessage(_messageHandler);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'tuvcino',
      theme: myTheme,
      debugShowCheckedModeBanner: false,
      //home: EventPage(),
      //home: LoginPage(),
      //home: HomePage(),
      home: MainSplashPage(),
      //T9vc1n0_#4pp
      //fyx7Iz94QAGL553y6p6JyI:APA91bEJ7oU3iaWtV1OeeyZ0TZY3tftBdSXT1hklNOsEbguTPEPeVMyAem-LWGEQbafJRB7cc52XELpP0QyDpwec6UWaXIBT2iby2_0pwHn4X3Q6fQpxJyBeOXlNkoQBqrseKSl7500o
      //eXyDXXLHTUiUpnwIaoY7QH:APA91bGGkAzzFX3j-KvwNuoW4AeEqA5F1mc9vWONLf14IZkNrIEyDCVj0B6xVXXcVOHmF2sniRRVXVHXZAIWsUkEnbgue6fc4_rmSgNtvKXK-3Ea2yqd--vgM5c4sN_KVedHIKIMevvJ
      initialRoute: '/',
      routes: {
        '/home': (context) => HomePage(),
        '/groups': (context) => GroupsPage(),
        '/places': (context) => DirectoriesPage(),
        '/filters': (context) => FiltersPage(),
        '/tasks': (context) => IndexTasksPage(),
        '/fees': (context) => FeesIndexPage(),
        '/profile': (context) => UserProfilePage(),
        '/admin': (context) => AdminiNeighborhoodMain(),
      },
    );
  }
}
