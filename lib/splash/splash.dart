import 'package:flutter/material.dart';
import 'package:tvcino/pages/login/login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  PageController _myPageController = PageController();
  /* var _currentPageValue = 0.0;
  AnimationController _animationController;
  Animation _animation; */

  @override
  void initState() {
    _myPageController.addListener(() {
      //print("This is my page ${_myPageController.page}");
      setState(() {
        //_currentPageValue = _myPageController.page;
      });
    });
    /* _animationController =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    _animationController.repeat(reverse: true);
    _animation = Tween(begin: 2.0, end: 10.0).animate(_animationController)
      ..addListener(() {
        setState(() {});
      }); */
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PageView(
        controller: _myPageController,
        onPageChanged: (int page) {
          print("Estamos en la página $page");
        },
        children: [
          buildInnerPage(
              context,
              'assets/splash/tuvcino_splash_ilustracion_1.png',
              "¡Hola vecino!",
              true,
              false),
          buildInnerPage(
              context,
              'assets/splash/tuvcino_splash_ilustracion_2.png',
              "Creamos lazos afectivos",
              false,
              false),
          buildInnerPage(
              context,
              'assets/splash/tuvcino_splash_ilustracion_3.png',
              "Vive en un entorno más colaborativo",
              false,
              false),
          buildInnerPage(
              context,
              'assets/splash/tuvcino_splash_ilustracion_4.png',
              "Tu comunidad más segura",
              false,
              true),
          /*Container(
            color: Colors.white,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Page 2", style: Theme.of(context).textTheme.headline2),
                  Text("$_currentPageValue", style: Theme.of(context).textTheme.headline4),
                ],
              )
            ),
          ),
          Container(
            color: Colors.white,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Page 3", style: Theme.of(context).textTheme.headline2),
                  Text("$_currentPageValue", style: Theme.of(context).textTheme.headline4),
                ],
              )
            ),
          ),*/
        ],
      ),
    );
  }

  Container buildInnerPage(
      BuildContext context, String image, String title, bool first, bool end) {
    return Container(
      color: Colors.white,
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image(
                width: MediaQuery.of(context).size.width / 1.6,
                image: AssetImage(
                    'assets/splash/tuvcino_splash_logotipo_1024_M.png'),
              ),
            ),
          ),
          SizedBox(
            height: 25.0,
          ),
          Image(
            image: AssetImage(image),
            width: MediaQuery.of(context).size.width * 0.75,
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
              width: MediaQuery.of(context).size.width * 0.9,
              child: Text(
                title,
                style: Theme.of(context).primaryTextTheme.headline2,
                textAlign: TextAlign.center,
              )),
          SizedBox(
            height: 15.0,
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.25,
            child: Text(
                'Tuvcino es una plataforma amigable y confiable la cual te ayuda a crear lazos afectivos con tus vecinos',
                style: Theme.of(context).textTheme.bodyText1),
          ),
          SizedBox(
            height: 35.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              first
                  ? GestureDetector(
                      onTap: () {
                        _myPageController.nextPage(
                            duration: Duration(milliseconds: 375),
                            curve: Curves.linear);
                      },
                      child: Container(
                        /* decoration: BoxDecoration(shape: BoxShape.circle,
                            //color: Color.fromARGB(255, 27, 28, 30),
                            boxShadow: [
                              BoxShadow(
                                  color: Theme.of(context).primaryColor,
                                  blurRadius: _animation.value,
                                  spreadRadius: (_animation.value - 5))
                            ]), */
                        child: Image(
                          image: AssetImage('assets/iconos/flecha_derecha.png'),
                          width: 55.0,
                        ),
                      ),
                    )
                  : Offstage(),
              !end && !first
                  ? Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          /* GestureDetector(
                            onTap: () {
                              _myPageController.previousPage(
                                  duration: Duration(milliseconds: 375),
                                  curve: Curves.linear);
                            },
                            child: Container(
                              /*decoration: BoxDecoration(shape: BoxShape.circle,
                                  //color: Color.fromARGB(255, 27, 28, 30),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Theme.of(context).primaryColor,
                                        blurRadius: _animation.value,
                                        spreadRadius: (_animation.value - 5))
                                  ]),*/
                              child: Image(
                                image: AssetImage(
                                    'assets/iconos/flecha_izquierda.png'),
                                width: 55.0,
                              ),
                            ),
                          ), */
                          GestureDetector(
                            onTap: () {
                              _myPageController.nextPage(
                                  duration: Duration(milliseconds: 375),
                                  curve: Curves.linear);
                            },
                            child: Container(
                              /* decoration: BoxDecoration(shape: BoxShape.circle,
                                  //color: Color.fromARGB(255, 27, 28, 30),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Theme.of(context).primaryColor,
                                        blurRadius: _animation.value,
                                        spreadRadius: (_animation.value - 5))
                                  ]), */
                              child: Image(
                                image: AssetImage(
                                    'assets/iconos/flecha_derecha.png'),
                                width: 55.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Offstage(),
            ],
          ),
          end
              ? TextButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                  child: Text('¡Comencemos!',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.8)),
                )
              : Row()
          //Text("$_currentPageValue", style: Theme.of(context).textTheme.headline4),
        ],
      )),
    );
  }
}
