import 'dart:async';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tvcino/classes/interuser.dart';
import 'package:tvcino/pages/login/login.dart';
import 'package:tvcino/pages/login/register_zip_code.dart';
import 'package:tvcino/splash/splash.dart';
import 'package:tvcino/utils/custom_functions.dart';

///===========================================================///
/// Class Main Splah, just the tuvcino logo in front
/// check if is the first time to show the splash
/// or
/// If its already loged in go to home directly
/// or get to the splash or login
///===========================================================///

class MainSplashPage extends StatefulWidget {
  @override
  _MainSplashPageState createState() => _MainSplashPageState();
}

class _MainSplashPageState extends State<MainSplashPage> {
  late SharedPreferences _preferences;
  bool _splashSeen = false;
  late auth.FirebaseAuth _instance;
  bool _error = false;

  @override
  void initState() {
    Timer(Duration(milliseconds: 750), () {
      _doSplashCheck();
    });

    super.initState();
  }

  _doSplashCheck() async {
    _preferences = await SharedPreferences.getInstance();
    _splashSeen = (_preferences.getBool('splashSeen') ?? false);
    //TODO: Check if the user has or no neighborhood assigned

    _instance = auth.FirebaseAuth.instance;
    auth.User? user;

    //============================================/
    /// Check if there is already a Firebase
    /// instance user
    //============================================/
    try {
      user = _instance.currentUser;
    } catch (e) {
      user = null;
    }

    //============================================/
    /// There is a Firebase User
    //============================================/
    if (user != null) {
      Dio dio = CustomFunctions.getDio();
      String token = await user.getIdToken();

      // print("The user Token is = $token");
      // print("Token Lenght: ${token.length}");
      // print("PART 1: ${token.substring(0, 600)}");
      // print("PART 2: ${token.substring(600)}");

      print("TV USER: $user");

      dio.options.headers["authorization"] = "$token";
      bool safeCall = true;
      Response response =
          await dio.get('/users?email=${user.email}').catchError((onError) {
        print("TV - CALL DIO ERROR ${onError.toString()}");
        setState(() {
          safeCall = false;
          _error = true;
        });
      });

      if (safeCall) {
        if (response.statusCode != 200) {
          print("TV DIO ERROR - Status Code ${response.statusCode}");
          print("TV DIO ERROR - Status Message ${response.statusMessage}");
          print(
              "TV - SPLASH MAIN: Hubo un error en el servidor regresa e intenta nuevamente");
        } else {
          print("DIO Status Code ${response.statusCode}");
          print("DIO - Response RAW Data");
          print(response);
          print("DIO Trying to convert data");
          //List data = response.data['items'];
          //print("Converted USER:");

          if (response.data['items'].length > 0) {
            InterUser _userInter =
                InterUser.fromJson(response.data["items"][0]);
            //print(_user);
            SharedPreferences preferences =
                await SharedPreferences.getInstance();
            await preferences.setString('userId', _userInter.id);
            await preferences.setString('uuid', user.uid);
            await preferences.setString(
                'neighborhoodId', _userInter.neighborhoodId ?? '');
            print(
                "DIO - Preferences userId ${preferences.getString('userId')}");
            print("DIO - Preferences UUID ${preferences.getString('uuid')}");
            print(
                "DIO - Preferences NigborhoodId ${preferences.getString('NigborhoodId')}");

            return Navigator.pushReplacementNamed(context, '/home');
          } else {
            //==================================================/
            /// There is the firebase user
            /// but close the app before gettin to our servers
            //==================================================/
            /* Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => RegisterSecondPage())); */
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => RegisterZipCode()));
          }
        }
      } else {
        print(
            "TV - SPLASH MAIN: Hubo un error en el servidor, intenta más tarde");
        setState(() {
          _error = true;
        });
        return;
      }
    } else {
      //==================================================/
      /// The Firebase User is not created so
      //==================================================/
      if (_splashSeen == true) {
        //==================================================/
        /// And the user has already seen the splash screen
        //==================================================/
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      }
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => SplashScreen()));
    }

    /* if (user != null && _neighborhoodSet == true) {
      /// There is a user so lets get to the home
      return Navigator.pushReplacementNamed(context, '/home');
    }
    if (user != null && _neighborhoodSet == false) {
      /// you are registered but not finished the proces
      /// So there is your personal info already in the app
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => RegisterSecondPage()));
    }
    if (_splashSeen == true) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LoginPage()));
    } */

    //==================================================/
    /// If something terrible happens up there
    /// directly to the splash again
    //==================================================/
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => SplashScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Center(
        child: _error
            ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.error_outline,
                      size: 28.0,
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Text(
                        "Hubo un error en el servidor, deberás intentar nuevamente",
                        textAlign: TextAlign.center),
                    SizedBox(
                      height: 12.5,
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/', (Route<dynamic> route) => false);
                      },
                      child: Text("¿Reiniciar app?"),
                    ),
                  ],
                ),
              )
            : Image.asset(
                'assets/splash/tuvcino_splash_icono_720.png',
                width: MediaQuery.of(context).size.width / 2,
              ),
      ),
    );
  }
}
