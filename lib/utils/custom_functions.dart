import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:flutter/material.dart';

///
// Util custom functions
// for the app
///

class CustomFunctions {
  //
  //static final String url = "http://80.211.138.216:5005";
  static final String url = "http://162.214.55.6:5005";

  ///
  // Function to get Dio Basic configuration
  ///
  static Dio getDio() {
    Dio dio = new Dio();

    // Set default configs
    // dio.options.baseUrl = "https://www.xx.com/api";
    dio.options.baseUrl = "http://162.214.55.6:5005";
    // dio.options.connectTimeout = 5000; //5s
    // dio.options.receiveTimeout = 3000;

    return dio;
  }

  ///
  // Util function for the check loged user
  // @return true if loged
  // false if not loged
  ///
  checkUserAlreadyLoged() {
    auth.FirebaseAuth _auth = auth.FirebaseAuth.instance;

    var user;

    try {
      user = _auth.currentUser;
    } catch (e) {
      user = null;
    }

    if (user != null) {
      return true;
    }

    return false;
  }

  Future<bool> singOutUser() async {
    auth.FirebaseAuth _auth = auth.FirebaseAuth.instance;
    //wait to sign out
    await _auth.signOut();
    return true;
  }

  bool isFavorite(List list, String id) {
    return list.contains(id);
  }

  static List mexicanStates() {
    List<String> states = <String>[
      "Aguascalientes",
      "Baja California",
      "Baja California Sur",
      "Campeche",
      "Coahuila de Zaragoza",
      "Colima",
      "Chiapas",
      "Chihuahua",
      "Distrito Federal",
      "Durango",
      "Guanajuato",
      "Guerrero",
      "Hidalgo",
      "Jalisco",
      "México",
      "Michoacán de Ocampo",
      "Morelos",
      "Nayarit",
      "Nuevo León",
      "Oaxaca",
      "Puebla",
      "Querétaro",
      "Quintana Roo",
      "San Luis Potosí",
      "Sinaloa",
      "Sonora",
      "Tabasco",
      "Tamaulipas",
      "Tlaxcala",
      "Veracruz",
      "Yucatán",
      "Zacatecas",
    ];
    return states;
  }

  static List unitedStates() {
    return <String>[
      "Alabama",
      "Alaska",
      "Arizona",
      "Arkansas",
      "California",
      "Colorado",
      "Connecticut",
      "Delaware",
      "Florida",
      "Georgia",
      "Idaho",
      "Hawaii",
      "Illinois",
      "Indiana",
      "Iowa",
      "Kansas",
      "Kentucky",
      "Louisiana",
      "Maine",
      "Maryland",
      "Massachusetts",
      "Michigan",
      "Minnesota",
      "Mississippi",
      "Missouri",
      "Montana",
      "Nebraska",
      "Nevada",
      "New Hampshire",
      "New Jersey",
      "New Mexico",
      "New York",
      "North Carolina",
      "North Dakota",
      "Ohio",
      "Oklahoma",
      "Oregon",
      "Pennsylvania",
      "Rhode Island",
      "South Carolina",
      "South Dakota",
      "Tennessee",
      "Texas",
      "Utah",
      "Vermont",
      "Virginia",
      "Washington",
      "West Virginia",
      "Wisconsin",
      "Wyoming",
    ];
  }

  static List canadaStates() {
    return <String>[
      "Nunavut",
      "Quebec",
      "Northwest Territories",
      "Ontario",
      "British Columbia",
      "Alberta",
      "Saskatchewan",
      "Manitoba",
      "Yukon",
      "Newfoundland and Labrador",
      "New Brunswick",
      "Nova Scotia",
      "Prince Edward Island",
    ];
  }

  static showBottomMessage(
      IconData icon, String title, String message, BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(25.0),
          //color: Theme.of(context).accentColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  icon,
                ),
                Text(
                  title,
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
