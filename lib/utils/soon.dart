import 'package:flutter/material.dart';

class SoonPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 40.0, vertical: 15.0),
              child: Image(
                image: ExactAssetImage(
                    'assets/splash/tuvcino_splash_logotipo_1024_M.png'),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Estamos trabajando",
              style: Theme.of(context).textTheme.headline2,
            ),
            Text(
              "vuelve pronto",
              style: Theme.of(context).textTheme.headline3,
            ),
            SizedBox(
              height: 35.0,
            ),
            TextButton(
              onPressed: () {
                return Navigator.pop(context);
              },
              child: Text("Volver".toUpperCase()),
            )
          ],
        ),
      ),
    );
  }
}
