import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart' show rootBundle;
import 'package:path_provider/path_provider.dart';

Future<File> getImageFileFromAssets(String path) async {
  final byteData = await rootBundle.load('assets/$path');

  final file = File('${(await getTemporaryDirectory()).path}/$path');
  await file.writeAsBytes(byteData.buffer
      .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));

  return file;
}

// CALL FUNCTION File f = await getImageFileFromAssets('images/myImage.jpg');

// USE EXAMPLE
/* RaisedButton(
   onPressed: () async => showDialog(
     context: context,
     builder: (_) => Container(child: Image.file(await getImageFileFromAssets('images/myImage.jpg')))),
   child: Text('Show Image')); */
