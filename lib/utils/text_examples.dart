import 'package:flutter/material.dart';

class TextExamples extends StatefulWidget {
  @override
  _TextExamplesState createState() => _TextExamplesState();
}

class _TextExamplesState extends State<TextExamples> {
  int _counter = 0;
  bool _selected = true;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Text Examples"),
        centerTitle: true,
        actions: [
          IconButton(
              icon: Icon(
                Icons.settings_input_antenna,
                //color: Theme.of(context).accentIconTheme.color,
              ),
              onPressed: null)
        ],
      ),
      drawer: Drawer(
        child: ListView(children: [
          UserAccountsDrawerHeader(
            currentAccountPicture: GestureDetector(
              child: CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
              ),
            ),
            accountName: Text("Jane Doe"),
            accountEmail: Text("jane@example.com"),
            otherAccountsPictures: [
              CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          ListTile(
            leading: Icon(Icons.merge_type),
            title: Text("Menu Item 1"),
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text("Menu Item 1"),
          ),
          Divider(),
        ]),
      ),
      body: SingleChildScrollView(
        child: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('First line of text',
                  style: Theme.of(context).textTheme.headline1),
              Text('Second line of exmple text',
                  style: Theme.of(context).textTheme.headline2),
              Text('Third line of exmple text',
                  style: Theme.of(context).textTheme.headline3),
              Text('Four line of exmple text',
                  style: Theme.of(context).textTheme.headline4),
              Text('Fifth line of exmple text',
                  style: Theme.of(context).textTheme.headline5),
              Text('Six line of exmple text',
                  style: Theme.of(context).textTheme.headline6),
              Text('Subtitle line example',
                  style: Theme.of(context).textTheme.subtitle1),
              Text('Subtitle 2 line example',
                  style: Theme.of(context).textTheme.subtitle2),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: Text('Content line of exmple text, lorem ipsum atiem',
                    style: Theme.of(context).textTheme.bodyText1),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: Text(
                  'Default text to read and more, just letters between each others',
                ),
              ),
              Text(
                '$_counter',
                style: Theme.of(context).textTheme.headline3,
                //style: myTheme.textTheme.headline4,
              ),
              /* Wrap(
                spacing: 5.0,
                children: [
                  FlatButton(
                    onPressed: () {
                      //
                    },
                    child: Text("Button Default!"),
                  ),
                  FlatButton(
                    color: Theme.of(context).buttonTheme.colorScheme.primary,
                    onPressed: () {
                      //
                    },
                    child: Text("Button Primary!"),
                  ),
                ],
              ),
              FlatButton(
                color: Theme.of(context).buttonTheme.colorScheme.secondary,
                onPressed: () {
                  //
                },
                child: Text("Button Secondary!"),
              ),
              FlatButton(
                color: Theme.of(context).buttonTheme.colorScheme.secondary,
                onPressed: () {
                  //
                },
                child: Text(
                  "Button Example!",
                  style: Theme.of(context).textTheme.button,
                ),
              ), */
              Chip(
                avatar: CircleAvatar(
                  backgroundColor: Colors.grey.shade800,
                  child: Text('AB'),
                ),
                label: Text('Aaron Burr'),
              ),
              ChoiceChip(
                  selected: _selected,
                  avatar: FlutterLogo(),
                  label: Text('Woolha'),
                  onSelected: (bool selected) {
                    setState(() {
                      _selected = !_selected;
                    });
                  }),
              Text('First line of text',
                  style: Theme.of(context).primaryTextTheme.headline1),
              Text('Second line of text',
                  style: Theme.of(context).primaryTextTheme.headline2),
              Text('Third line of text',
                  style: Theme.of(context).primaryTextTheme.headline3),
              Text('Four line of exmple text',
                  style: Theme.of(context).primaryTextTheme.headline4),
              Text('Fifth line of exmple text',
                  style: Theme.of(context).primaryTextTheme.headline5),
              Text('Six line of exmple text',
                  style: Theme.of(context).primaryTextTheme.headline6),
              Text('Subtitle line example',
                  style: Theme.of(context).primaryTextTheme.subtitle1),
              Text('Subtitle 2 line example',
                  style: Theme.of(context).primaryTextTheme.subtitle2),
              SizedBox(
                height: 50.0,
              ),
              Text("I'm doing like I'm working while I'm not",
                  softWrap: true,
                  overflow: TextOverflow.visible,
                  style: Theme.of(context).primaryTextTheme.headline1)
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.search),
        //elevation: 2.0,
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.book),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.bubble_chart),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.people),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
          ],
        ),
        //notchedShape: CircularNotchedRectangle(),
        color: Colors.white,
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
