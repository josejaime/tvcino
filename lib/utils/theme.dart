import 'package:flutter/material.dart';

final ThemeData myTheme = ThemeData(
    primarySwatch: MaterialColor(4285024200, {
      50: Color(0xffefebf9),
      100: Color(0xffdfd8f3),
      200: Color(0xffbfb0e8),
      300: Color(0xff9e89dc),
      400: Color(0xff7e62d0),
      500: Color(0xff5e3bc4),
      600: Color(0xFF4A2F9D),
      700: Color(0xff382376),
      800: Color(0xff26174f),
      900: Color(0xff130c27)
    }),
    brightness: Brightness.dark,
    primaryColor: Color(0xff6847c8),
    primaryColorBrightness: Brightness.dark,
    primaryColorLight: Color(0xffdfd8f3),
    primaryColorDark: Color(0xff382376),
    accentColor: Color(0xff5e3bc4),
    accentColorBrightness: Brightness.dark,
    canvasColor: Color(0xfffafafa),
    //scaffoldBackgroundColor: Color(0xfffafafa),
    scaffoldBackgroundColor: Color(0xffebebeb),
    bottomAppBarColor: Color(0xffffffff),
    cardColor: Color(0xffffffff),
    dividerColor: Color(0x1f000000),
    highlightColor: Color(0x66bcbcbc),
    splashColor: Color(0x66c8c8c8),
    selectedRowColor: Color(0xfff5f5f5),
    unselectedWidgetColor: Color(0x8a000000),
    disabledColor: Color(0x61000000),
    buttonColor: Color(0xffe0e0e0),
    toggleableActiveColor: Color(0xff4b2f9d),
    secondaryHeaderColor: Color(0xffefebf9),
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: Color(0xff4285f4),
      selectionHandleColor: Color(0xff9e89dc),
      selectionColor: Color(0xffbfb0e8),
    ),
    backgroundColor: Color(0xffbfb0e8),
    dialogBackgroundColor: Color(0xffffffff),
    indicatorColor: Color(0xff5e3bc4),
    hintColor: Color(0x8a000000),
    errorColor: Color(0xffd32f2f),
    buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary,
      minWidth: 105,
      //height: 36,
      //padding: EdgeInsets.only(top: 5, bottom: 5, left: 16, right: 16),
      padding: EdgeInsets.only(top: 12.5, bottom: 12.5, left: 25, right: 25),
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: Color(0x86000000),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.all(Radius.circular(18.0)),
      ),
      alignedDropdown: false,
      //buttonColor: Color(0xffe0e0e0),
      buttonColor: Color(0xff6847c8),
      disabledColor: Color(0x61000000),
      highlightColor: Color(0x29000000),
      splashColor: Color(0x1f000000),
      focusColor: Color(0x1f000000),
      hoverColor: Color(0x0a000000),
      colorScheme: ColorScheme(
        primary: Color(0xff6847c8),
        primaryVariant: Color(0xff382376),
        //secondary: Color(0xff5e3bc4),
        //secondary: Color(0xff1b365d),
        secondary: Color(0xff1b365d),
        secondaryVariant: Color(0xff382376),
        surface: Color(0xffffffff),
        background: Color(0xffbfb0e8),
        error: Color(0xffd32f2f),
        onPrimary: Color(0xffffffff),
        onSecondary: Color(0xffffffff),
        onSurface: Color(0xff000000),
        onBackground: Color(0xffffffff),
        onError: Color(0xffffffff),
        brightness: Brightness.light,
      ),
    ),
    //=======================================
    // TEXT THEME FOR THE
    // DARK BLUE HEADLINES AND SUBTITLES
    // MITR FONT FAMILY
    //=======================================
    textTheme: TextTheme(
      headline1: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 43.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w600,
        fontStyle: FontStyle.normal,
      ),
      headline2: TextStyle(
        color: Color(0xff1b365d),
        fontSize: 27.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline3: TextStyle(
        color: Color(0xff1b365d),
        fontSize: 25.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      ),
      headline4: TextStyle(
        color: Color(0xff1b365d),
        fontSize: 21.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      ),
      headline5: TextStyle(
        color: Color(0xff1b365d),
        fontSize: 19.5,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline6: TextStyle(
        color: Color(0xff1b365d),
        fontSize: 16.5,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      subtitle1: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 18.0,
        fontFamily: "Sans",
        fontWeight: FontWeight.w700,
        fontStyle: FontStyle.normal,
      ),
      bodyText1: TextStyle(
        color: Color(0xdd000000),
        fontSize: 21.0,
        fontFamily: "Sans",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      //=============================
      // Body text default
      //=============================
      bodyText2: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontFamily: "Sans",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      caption: TextStyle(
        color: Color(0x8a000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      button: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      subtitle2: TextStyle(
        color: Color(0xff000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      overline: TextStyle(
        color: Color(0xff000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
    ),
    //=======================================
    // PRIMARY TEXT THEME FOR THE
    // PURPLE HEADLINES AND SUBTITLES
    // MITR FONT FAMILY
    //=======================================
    primaryTextTheme: TextTheme(
      //===========================
      // Large Title
      //===========================
      headline1: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 40.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w600,
        fontStyle: FontStyle.normal,
      ),
      //===========================
      // Title
      //===========================
      headline2: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 32.0,
        height: 1.25,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      //===========================
      // Small Title
      //===========================
      headline3: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 26.5,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      ),
      headline4: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 21.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
      ),
      headline5: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 19.5,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline6: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 16.5,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      subtitle1: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 18.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      subtitle2: TextStyle(
        color: Color(0xff6847C8),
        fontSize: 14.0,
        fontFamily: "Mitr",
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      bodyText1: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      bodyText2: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      caption: TextStyle(
        color: Color(0xb3ffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      button: TextStyle(
        color: Color(0xff346f33),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      overline: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
    ),
    accentTextTheme: TextTheme(
      headline1: TextStyle(
        color: Color(0xb3ffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline2: TextStyle(
        color: Color(0xb3ffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline3: TextStyle(
        color: Color(0xb3ffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline4: TextStyle(
        color: Color(0xb3ffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline5: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      headline6: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      subtitle1: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      bodyText1: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      bodyText2: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      caption: TextStyle(
        color: Color(0xb3ffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      button: TextStyle(
        color: Color(0xff346f33),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      subtitle2: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      overline: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      helperStyle: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      hintStyle: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      errorStyle: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      errorMaxLines: null,
      floatingLabelBehavior: FloatingLabelBehavior.auto,
      isDense: false,
      contentPadding: EdgeInsets.only(top: 12, bottom: 12, left: 0, right: 0),
      isCollapsed: false,
      prefixStyle: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      suffixStyle: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      counterStyle: TextStyle(
        color: Color(0xdd000000),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      filled: false,
      fillColor: Color(0x00000000),
      errorBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xff000000),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xff000000),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      focusedErrorBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xff000000),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      disabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xff000000),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xff000000),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Color(0xff000000),
          width: 1,
          style: BorderStyle.solid,
        ),
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
    ),
    iconTheme: IconThemeData(
      color: Color(0xdd000000),
      opacity: 1,
      size: 24,
    ),
    primaryIconTheme: IconThemeData(
      color: Color(0xffffffff),
      opacity: 1,
      size: 24,
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Color(0xff6847c8),
    ),
    // accentIconTheme: IconThemeData(
    //   color: Color(0xffffffff),
    //   opacity: 1,
    //   size: 24,
    // ),
    sliderTheme: SliderThemeData(
      activeTrackColor: null,
      inactiveTrackColor: null,
      disabledActiveTrackColor: null,
      disabledInactiveTrackColor: null,
      activeTickMarkColor: null,
      inactiveTickMarkColor: null,
      disabledActiveTickMarkColor: null,
      disabledInactiveTickMarkColor: null,
      thumbColor: null,
      disabledThumbColor: null,
      thumbShape: RoundSliderThumbShape(),
      overlayColor: null,
      valueIndicatorColor: null,
      valueIndicatorShape: PaddleSliderValueIndicatorShape(),
      showValueIndicator: null,
      valueIndicatorTextStyle: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
    ),
    tabBarTheme: TabBarTheme(
      indicatorSize: TabBarIndicatorSize.tab,
      labelColor: Color(0xffffffff),
      unselectedLabelColor: Color(0xb2ffffff),
    ),
    chipTheme: ChipThemeData(
      //backgroundColor: Color(0x1f000000),
      backgroundColor: Color(0xff1b365d),
      brightness: Brightness.light,
      deleteIconColor: Color(0xde000000),
      disabledColor: Color(0x0c000000),
      labelPadding: EdgeInsets.only(top: 0, bottom: 0, left: 8, right: 8),
      labelStyle: TextStyle(
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      padding: EdgeInsets.only(top: 4, bottom: 4, left: 4, right: 4),
      secondaryLabelStyle: TextStyle(
        //color: Color(0x3d000000),
        color: Color(0xffffffff),
        fontSize: null,
        fontWeight: FontWeight.w400,
        fontStyle: FontStyle.normal,
      ),
      //secondarySelectedColor: Color(0x3d6847c8),
      secondarySelectedColor: Color(0xd76847C8),
      selectedColor: Color(0x3dffffff),
      shape: StadiumBorder(
          side: BorderSide(
        color: Color(0xff000000),
        width: 0,
        style: BorderStyle.none,
      )),
    ),
    dialogTheme: DialogTheme(
        shape: RoundedRectangleBorder(
      side: BorderSide(
        color: Color(0xff000000),
        width: 0,
        style: BorderStyle.none,
      ),
      borderRadius: BorderRadius.all(Radius.circular(0.0)),
    )),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Color(0xff6847c8)),
        foregroundColor: MaterialStateProperty.all(Colors.white),
        elevation: MaterialStateProperty.all(2.0),
        padding: MaterialStateProperty.all(
            EdgeInsets.symmetric(horizontal: 25, vertical: 10.0)),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
        ),
        textStyle: MaterialStateProperty.all(
          TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    ));
